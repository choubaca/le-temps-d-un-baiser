$(document).ready(function() {
    
        // fonction qui permet de flouter l'arriere plan à l'ouverture du menu
        function flouterMenu(){

                // Au clic sur le bouton du menu on execute la fonction
                $("#bouton").click(function(e) {

                    // on cible l'id carte de visite et on lui enleve ou rajoute la class flou
                    $( "#carte_de_visite" ).toggleClass( 'flou' );
                    
                     // on cible l'id de la page contact et on lui enleve ou rajoute la class flou

                    $( "#contact" ).toggleClass( 'flou' );

                });
            }
            
            
    flouterMenu();
            
 });
function ajoutgalerie ()
{
    let newDiv;
    newDiv = document.createElement('div');
    newDiv.classList.add('mariage');
    document.getElementById('galerie').appendChild(newDiv);
}
    
function afficher ()
    
{
    let champs_requis = document.getElementsByClassName('requis');
    document.getElementById("temoin2").className = 'block';
    document.getElementById("bouton").className = 'd-none';
    document.getElementById("boutoncacher").className = 'btn btn-info';
    for(x in champs_requis){
        champs_requis[x].required = true;
    }
}


function cacher ()
{
    let champs_non_requis = document.getElementsByClassName('requis');
    let non_check = document.getElementsByClassName('sexeTemoin2');
    document.getElementById("temoin2").className = 'd-none';
    document.getElementById("bouton").className = 'btn btn-info';
    document.getElementById("boutoncacher").className = 'd-none';
    for (x in non_check){
        non_check[x].checked = false;
    }
    for(x in champs_non_requis){
        champs_non_requis[x].required = false;
        champs_non_requis[x].value = "";
    }
}
<?php

include("../../PHP/connexion/connexion.php");

$data = [
    'num' => $_GET['numPhotographe']
];

$req="SELECT * FROM photographes

            LEFT JOIN deplacements ON deplacements.photographes_id = photographes.id
            LEFT JOIN photographes_photobooths ON photographes_photobooths.photographes_id = photographes.id
            LEFT JOIN photographes_styles ON photographes_styles.photographes_id = photographes.id
            LEFT JOIN photographes_supports ON photographes_supports.photographes_id = photographes.id
            LEFT JOIN photographes_tarifs ON photographes_tarifs.photographes_id = photographes.id
                WHERE photographes.id = :num ";

$requete = $connexion->prepare($req);
$requete->execute($data);
$photographe = $requete-> fetch();

?>
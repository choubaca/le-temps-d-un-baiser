<?php

include("../../PHP/connexion/connexion.php");

$data = [
    'num' => $_GET['numMarie']
];

$req="SELECT * FROM organisations

            LEFT JOIN maries_1 ON maries_1_id = maries_1.id
            LEFT JOIN maries_2 ON maries_2.maries_1_id = maries_2.id
            LEFT JOIN organisations_animations_musicales ON organisations_animations_musicales.organisation_id = organisations.id
            LEFT JOIN organisations_baby_sitters ON organisations_baby_sitters.organisation_id = organisations.id
            LEFT JOIN organisations_convives ON organisations_convives.organisation_id = organisations.id
            LEFT JOIN organisations_criteres ON organisations_criteres.organisation_id = organisations.id
            LEFT JOIN organisations_deroules ON organisations_deroules.organisation_id = organisations.id
            LEFT JOIN organisations_desserts ON organisations_desserts.organisation_id = organisations.id
            LEFT JOIN organisations_diners ON organisations_diners.organisation_id = organisations.id
            LEFT JOIN organisations_hebergements ON organisations_hebergements.organisation_id = organisations.id
            LEFT JOIN organisations_photographes ON organisations_photographes.organisation_id = organisations.id
            LEFT JOIN organisations_soirees ON organisations_soirees.organisation_id = organisations.id
            LEFT JOIN organisations_videos ON organisations_videos.organisation_id = organisations.id
            LEFT JOIN organisations_vins_dhonneur ON organisations_vins_dhonneur.organisation_id = organisations.id
            LEFT JOIN organisations_autres_prestataires ON organisations_autres_prestataires.organisation_id = organisations.id

                WHERE organisations.id = :num ";

$requete = $connexion->prepare($req);
$requete->execute($data);
$organisations = $requete-> fetch();


?>
<?php

include("../../PHP/connexion/connexion.php");

$data = [
    'num' => $_GET['numLieu']
];

$req="SELECT * FROM lieux_contacts

            LEFT JOIN lieux ON lieux.lieux_contact_id = lieux_contacts.id
            LEFT JOIN lieux_annexes ON lieux_annexes.lieux_id = lieux.id
            LEFT JOIN lieux_buffets ON lieux_buffets.lieux_id = lieux.id
            LEFT JOIN lieux_chaises_exterieur ON lieux_chaises_exterieur.lieux_id = lieux.id
            LEFT JOIN lieux_chaises_interieur ON lieux_chaises_interieur.lieux_id = lieux.id
            LEFT JOIN lieux_equipements ON lieux_equipements.lieux_id = lieux.id
            LEFT JOIN lieux_hebergements ON lieux_hebergements.lieux_id = lieux.id
            LEFT JOIN lieux_salles_principales ON lieux_salles_principales.lieux_id = lieux.id
            LEFT JOIN lieux_secondes_salles ON lieux_secondes_salles.lieux_id = lieux.id
            LEFT JOIN lieux_tables_rectanculaires ON lieux_tables_rectanculaires.lieux_id = lieux.id
            LEFT JOIN lieux_tables_rondes ON lieux_tables_rondes.lieux_id = lieux.id
            LEFT JOIN lieux_tarifs ON lieux_tarifs.lieux_id = lieux.id
            LEFT JOIN lieux_troisiemes_salles ON lieux_troisiemes_salles.lieux_id = lieux.id

                WHERE lieux_contacts.id = :num ";

$requete = $connexion->prepare($req);
$requete->execute($data);
$Lieu = $requete-> fetch();

?>
<?php

include("../../PHP/connexion/connexion.php");

$data = [
    'num' => $_GET['numTraiteur']
];

$req="SELECT * FROM traiteurs

            LEFT JOIN traiteurs_bouteilles ON traiteurs_bouteilles.traiteur_id = traiteurs.id
            LEFT JOIN traiteurs_cocktails ON traiteurs_cocktails.traiteur_id = traiteurs.id
            LEFT JOIN traiteurs_cuisines ON traiteurs_cuisines.traiteur_id = traiteurs.id
            LEFT JOIN traiteurs_diners ON traiteurs_diners.traiteur_id = traiteurs.id
            LEFT JOIN traiteurs_produits ON traiteurs_produits.traiteur_id = traiteurs.id
            LEFT JOIN traiteurs_soirees ON traiteurs_soirees.traiteur_id = traiteurs.id
            LEFT JOIN traiteurs_types_prestations ON traiteurs_types_prestations.traiteur_id = traiteurs.id
            LEFT JOIN traiteurs_vaisselles ON traiteurs_vaisselles.traiteur_id = traiteurs.id
            LEFT JOIN traiteur_degustations ON traiteur_degustations.traiteur_id = traiteurs.id
            LEFT JOIN traiteur_desserts ON traiteur_desserts.traiteur_id = traiteurs.id

                WHERE traiteurs.id = :num ";

$requete = $connexion->prepare($req);
$requete->execute($data);
$Traiteur = $requete-> fetch();


?>
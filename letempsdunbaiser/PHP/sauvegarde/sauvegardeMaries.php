<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");

if (isset($_POST['submit'])){


    $data = [
        'marie1_nom' => htmlspecialchars ($_POST['nom']),
        'marie1_prenom' => htmlspecialchars ($_POST['prenom']),
        'marie1_telephone' => htmlspecialchars ($_POST['tel']),
        'marie1_adresse' => htmlspecialchars ($_POST['adresse']),
        'marie1_email' => htmlspecialchars ($_POST['email']),
        'marie1_profession' => htmlspecialchars ($_POST['profession']),
        'marie1_date_naissance' => htmlspecialchars ($_POST['naissance']),
        'marie1_etat_civil' => htmlspecialchars ($_POST['sexe'])
        ];

        $marie1="";
        $req = "INSERT INTO maries_1 (marie1_nom, marie1_prenom, marie1_telephone, marie1_adresse, marie1_email, marie1_profession, marie1_date_naissance, marie1_etat_civil) 
                        VALUES (:marie1_nom, :marie1_prenom, :marie1_telephone, :marie1_adresse, :marie1_email, :marie1_profession, :marie1_date_naissance, :marie1_etat_civil)";
        $requete = $connexion->prepare($req);
        $requete->execute($data);
        $marie1 = $connexion->lastInsertId();

    $data2 = [
        'marie1_id' => $marie1,
        'temoin1_nom' => htmlspecialchars ($_POST['nomTemoin1']),
        'temoin1_prenom' => htmlspecialchars ($_POST['prenomTemoin1']),
        'temoin1_telephone' => htmlspecialchars ($_POST['telTemoin1']),
        'temoin1_etat_civil' => htmlspecialchars ($_POST['sexeTemoin1'])
        ];


        $req = "INSERT INTO temoins_1 (marie1_id, temoin1_nom, temoin1_prenom, temoin1_telephone, temoin1_etat_civil) 
                        VALUES (:marie1_id, :temoin1_nom, :temoin1_prenom, :temoin1_telephone, :temoin1_etat_civil)";
        $requete = $connexion->prepare($req);
        $requete->execute($data2);


    if (!empty($_POST['sexeTemoin2'])){

    $data3 = [
        'marie1_id' => $marie1,
        'temoin2_nom' => htmlspecialchars ($_POST['nomTemoin2']),
        'temoin2_prenom' => htmlspecialchars ($_POST['prenomTemoin2']),
        'temoin2_telephone' => htmlspecialchars ($_POST['telTemoin2']),
        'temoin2_etat_civil' => htmlspecialchars ($_POST['sexeTemoin2'])
        ];

        $req = "INSERT INTO temoins_2 (marie1_id, temoin2_nom, temoin2_prenom, temoin2_telephone, temoin2_etat_civil) 
                        VALUES (:marie1_id, :temoin2_nom, :temoin2_prenom, :temoin2_telephone, :temoin2_etat_civil)";
        $requete = $connexion->prepare($req);
        $requete->execute($data3);

        }else {
            $temoin2 = 1; // on attribut la valeur '1' par defaut si les champ $_POST ne sont pas remplie ce qui correspond à la valeur 'aucun' dans la bdd
        };


    // on redirige vers le second formulaire

    header("location:../../HTML/ajout/ajoutMaries2.php?numMarie=".$marie1."");
}

?>
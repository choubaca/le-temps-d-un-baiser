<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");



if (isset($_POST['submit'])){
    
        $data6 = [
                'contact_nom' => htmlspecialchars ($_POST['nomContact']),
                'contact_telephone' => htmlspecialchars ($_POST['telContact']),
                'contact_mail' => htmlspecialchars ($_POST['mailContact'])
                ];
        
                $contact="";
                $req = "INSERT INTO lieux_contacts (contact_nom, contact_telephone, contact_mail) 
                                VALUES (:contact_nom, :contact_telephone, :contact_mail)";
                $requete = $connexion->prepare($req);                
                $requete->execute($data6);
                $contact = $connexion->lastInsertId();

        $data = [
                'lieux_contact_id' => $contact,
                'lieu_nom' => htmlspecialchars ($_POST['nomLieu']),
                'lieu_type' => htmlspecialchars ($_POST['typeLieu']),
                'lieu_ville' => htmlspecialchars ($_POST['ville']),
                'lieu_distance' => htmlspecialchars ($_POST['distance']),
                'lieu_plan_B' => htmlspecialchars ($_POST['planB']),
                'lieu_temps_trajet' => htmlspecialchars ($_POST['tempsTrajet']),
                'lieu_salle_occupation_enfant' => htmlspecialchars ($_POST['salleOccupEnfant']),
                'lieu_salle_repos_enfant' => htmlspecialchars ($_POST['SalleDormirEnfant']),
                'lieu_mariage_unique' => htmlspecialchars ($_POST['mariageSolo']),
                'lieu_accompte' => htmlspecialchars ($_POST['accompte']),
                'lieu_travailWP' => htmlspecialchars ($_POST['travailWP']),
                'lieu_remarque' => htmlspecialchars ($_POST['remarque'])
                ];
        
                $lieu = "";
                $req = "INSERT INTO lieux (lieux_contact_id, lieu_nom, lieu_type, lieu_ville, lieu_distance, lieu_plan_B, lieu_temps_trajet, lieu_salle_occupation_enfant, lieu_salle_repos_enfant, 
                                        lieu_mariage_unique, lieu_accompte, lieu_travailWP, lieu_remarque) 
                                VALUES (:lieux_contact_id, :lieu_nom, :lieu_type, :lieu_ville, :lieu_distance, :lieu_plan_B, :lieu_temps_trajet, :lieu_salle_occupation_enfant, :lieu_salle_repos_enfant, 
                                        :lieu_mariage_unique, :lieu_accompte, :lieu_travailWP, :lieu_remarque)";
                $requete = $connexion->prepare($req);
                $requete->execute($data);
                $lieu = $connexion->lastInsertId();


        $data2 = [
                'lieux_id' => $lieu,
                'terrasse' => htmlspecialchars ($_POST['terrasse']),
                'surface_terrasse' => htmlspecialchars ($_POST['terrasseSurf']),
                'jardin' => htmlspecialchars ($_POST['jardin']),
                'surface_jardin' => htmlspecialchars ($_POST['jardinSurf']),
                'parc' => htmlspecialchars ($_POST['parc']),
                'surface_parc' => htmlspecialchars ($_POST['parcSurf']),
                'parking' => htmlspecialchars ($_POST['parking']),
                'place_parking' => htmlspecialchars ($_POST['placeParking']),
                'vestiaire' => htmlspecialchars ($_POST['vestiaire']),
                'toilette_homme' => htmlspecialchars ($_POST['WcHomme']),
                'toilette_femme' => htmlspecialchars ($_POST['WcFemme']),
                'PMR' => htmlspecialchars ($_POST['PMR'])
                ];
        
                $req = "INSERT INTO lieux_annexes (lieux_id, terrasse, surface_terrasse, jardin, surface_jardin, parc, surface_parc, parking, place_parking, vestiaire, toilette_homme, 
                                        toilette_femme, PMR) 
                                VALUES (:lieux_id, :terrasse, :surface_terrasse, :jardin, :surface_jardin, :parc, :surface_parc, :parking, :place_parking, :vestiaire, :toilette_homme, 
                                        :toilette_femme, :PMR)";
                $requete = $connexion->prepare($req);
                $requete->execute($data2);


        $data3 = [
                'lieux_id' => $lieu,
                'buffet_nombre' => htmlspecialchars ($_POST['dimBuffet']),
                'buffet_dimension' => htmlspecialchars ($_POST['matiereBuffet']),
                'buffet_nombre_de_personnes' => htmlspecialchars ($_POST['nbrPersBuffet']),
                'buffet_matiere' => htmlspecialchars ($_POST['nbrBuffet'])
                ];
        
                $req = "INSERT INTO lieux_buffets (lieux_id, buffet_nombre, buffet_dimension, buffet_nombre_de_personnes, buffet_matiere) 
                                VALUES (:lieux_id, :buffet_nombre, :buffet_dimension, :buffet_nombre_de_personnes, :buffet_matiere)";
                $requete = $connexion->prepare($req);
                $requete->execute($data3);


        $data4 = [
                'lieux_id' => $lieu,
                'chaise_exterieur_type' => htmlspecialchars ($_POST['TypeChaiseExt']),
                'chaise_exterieur_matiere' => htmlspecialchars ($_POST['matiereChaiseExt']),
                'chaise_exterieur_nombre' => htmlspecialchars ($_POST['qtsChaiseExt'])
        ];
        
                $req = "INSERT INTO lieux_chaises_exterieur (lieux_id, chaise_exterieur_type, chaise_exterieur_matiere, chaise_exterieur_nombre) 
                                VALUES (:lieux_id, :chaise_exterieur_type, :chaise_exterieur_matiere, :chaise_exterieur_nombre)";
                $requete = $connexion->prepare($req);
                $requete->execute($data4);


        $data5 = [
                'lieux_id' => $lieu,
                'chaise_interieur_type' => htmlspecialchars ($_POST['TypeChaiseInt']),
                'chaise_interieur_matiere' => htmlspecialchars ($_POST['matiereChaiseInt']),
                'chaise_interieur_nombre' => htmlspecialchars ($_POST['qtsChaiseInt']),
                'autre_chaise' => htmlspecialchars ($_POST['autreChaise'])
        ];
        
                $req = "INSERT INTO lieux_chaises_interieur (lieux_id, chaise_interieur_type, chaise_interieur_matiere, chaise_interieur_nombre, autre_chaise) 
                                VALUES (:lieux_id, :chaise_interieur_type, :chaise_interieur_matiere, :chaise_interieur_nombre, :autre_chaise)";
                $requete = $connexion->prepare($req);
                $requete->execute($data5);


        $data7 = [
                'lieux_id' => $lieu,
                'retroprojecteur' => htmlspecialchars ($_POST['lieuRetropro']),
                'ecran' => htmlspecialchars ($_POST['lieuEcran']),
                'ampli' => htmlspecialchars ($_POST['lieuAmpli']),
                'enceinte' => htmlspecialchars ($_POST['lieuEnceinte']),
                'prix_mobilier_compris' => htmlspecialchars ($_POST['mobilierCompris']),
                'autre_mobilier' => htmlspecialchars ($_POST['autreMobilier'])
                ];
        
                $req = "INSERT INTO lieux_equipements (lieux_id, retroprojecteur, ecran, ampli, enceinte, prix_mobilier_compris, autre_mobilier) 
                                VALUES (:lieux_id, :retroprojecteur, :ecran, :ampli, :enceinte, :prix_mobilier_compris, :autre_mobilier)";
                $requete = $connexion->prepare($req);
                $requete->execute($data7);


        $data8 = [
                'lieux_id' => $lieu,
                'hebergement_sur_place' => htmlspecialchars ($_POST['hebergSurPlace']),
                'nombre_de_chambre' => htmlspecialchars ($_POST['combienChambre']),
                'capacite_total' => htmlspecialchars ($_POST['capaTotal']),
                'prix_chambre1' => htmlspecialchars ($_POST['prixChambre1']),
                'prix_chambre2' => htmlspecialchars ($_POST['prixChambre2']),
                'forfait_location' => htmlspecialchars ($_POST['forfaitLocation']),
                'forfait_location_et_hebergement' => htmlspecialchars ($_POST['forfaitLocHerb'])
                ];
        
                $req = "INSERT INTO lieux_hebergements (lieux_id, hebergement_sur_place, nombre_de_chambre, capacite_total, prix_chambre1, prix_chambre2, forfait_location, forfait_location_et_hebergement) 
                                VALUES (:lieux_id, :hebergement_sur_place, :nombre_de_chambre, :capacite_total, :prix_chambre1, :prix_chambre2, :forfait_location, :forfait_location_et_hebergement)";
                $requete = $connexion->prepare($req);
                $requete->execute($data8);


        $data9 = [
                'lieux_id' => $lieu,
                'salle1_longueur' => htmlspecialchars ($_POST['longueurSalleP']),
                'salle1_largeur' => htmlspecialchars ($_POST['largeurSalleP']),
                'salle1_nombre_max_personne' => htmlspecialchars ($_POST['nbMaxSalleP']),
                'salle1_separation' => htmlspecialchars ($_POST['sepSalleP']),
                'salle1_chauffage' => htmlspecialchars ($_POST['sallePChauffe']),
                'salle1_climatisation' => htmlspecialchars ($_POST['sallePClimati']),
                'salle1_vin_en_exterieur' => htmlspecialchars ($_POST['vinExter'])
                ];
        
                $req = "INSERT INTO lieux_salles_principales (lieux_id, salle1_longueur, salle1_largeur, salle1_nombre_max_personne, salle1_separation, salle1_chauffage, salle1_climatisation, salle1_vin_en_exterieur) 
                                VALUES (:lieux_id, :salle1_longueur, :salle1_largeur, :salle1_nombre_max_personne, :salle1_separation, :salle1_chauffage, :salle1_climatisation, :salle1_vin_en_exterieur)";                
                $requete = $connexion->prepare($req);
                $requete->execute($data9);


        $data10 = [
                'lieux_id' => $lieu,
                'salle2_longueur' => htmlspecialchars ($_POST['longueurSalle2']),
                'salle2_largeur' => htmlspecialchars ($_POST['largeurSalle2']),
                'salle2_nombre_max_personne' => htmlspecialchars ($_POST['nbMaxSalle2']),
                'salle2_utilisation' => htmlspecialchars ($_POST['utilSalle2']),
                'salle2_separation' => htmlspecialchars ($_POST['sepSalle2']),
                'salle2_chauffage' => htmlspecialchars ($_POST['salle2Chauffe']),
                'salle2_climatisation' => htmlspecialchars ($_POST['salle2Climati'])
                ];

                $req = "INSERT INTO lieux_secondes_salles (lieux_id, salle2_longueur, salle2_largeur, salle2_nombre_max_personne, salle2_utilisation, salle2_separation, salle2_chauffage, salle2_climatisation) 
                                VALUES (:lieux_id, :salle2_longueur, :salle2_largeur, :salle2_nombre_max_personne, :salle2_utilisation, :salle2_separation, :salle2_chauffage, :salle2_climatisation)";                
                $requete = $connexion->prepare($req);
                $requete->execute($data10);


        $data11 = [
                'lieux_id' => $lieu,
                'salle3_longueur' => htmlspecialchars ($_POST['longueurSalle3']),
                'salle3_largeur' => htmlspecialchars ($_POST['largeurSalle3']),
                'salle3_nombre_max_personne' => htmlspecialchars ($_POST['nbMaxSalle3']),
                'salle3_utilisation' => htmlspecialchars ($_POST['utilSalle3']),
                'salle3_separation' => htmlspecialchars ($_POST['sepSalle3']),
                'salle3_chauffage' => htmlspecialchars ($_POST['salle3Chauffe']),
                'salle3_climatisation' => htmlspecialchars ($_POST['salle3Climati'])
                ];
        
                $req = "INSERT INTO lieux_troisiemes_salles (lieux_id, salle3_longueur, salle3_largeur, salle3_nombre_max_personne, salle3_utilisation, salle3_separation, salle3_chauffage, salle3_climatisation) 
                                VALUES (:lieux_id, :salle3_longueur, :salle3_largeur, :salle3_nombre_max_personne, :salle3_utilisation, :salle3_separation, :salle3_chauffage, :salle3_climatisation)";                
                $requete = $connexion->prepare($req);
                $requete->execute($data11);


        $data12 = [
                'lieux_id' => $lieu,
                'table_rectanculaire_nombre' => htmlspecialchars ($_POST['dimTableRect']),
                'table_rectanculaire_dimension' => htmlspecialchars ($_POST['matiereTableRect']),
                'table_rectanculaire_nombre_de_personnes' => htmlspecialchars ($_POST['nbrPersTableRect']),
                'table_rectanculaire_matiere' => htmlspecialchars ($_POST['nbrTableRect'])
                ];
        
                $req = "INSERT INTO lieux_tables_rectanculaires (lieux_id, table_rectanculaire_nombre, table_rectanculaire_dimension, table_rectanculaire_nombre_de_personnes, table_rectanculaire_matiere) 
                                VALUES (:lieux_id, :table_rectanculaire_nombre, :table_rectanculaire_dimension, :table_rectanculaire_nombre_de_personnes, :table_rectanculaire_matiere)";
                $requete = $connexion->prepare($req);
                $requete->execute($data12);


        $data13 = [
                'lieux_id' => $lieu,
                'table_ronde_nombre' => htmlspecialchars ($_POST['nbrTableRonde']),
                'table_ronde_dimension' => htmlspecialchars ($_POST['dimTableRonde']),
                'table_ronde_nombre_de_personnes' => htmlspecialchars ($_POST['nbrPersTableRonde']),
                'table_ronde_matiere' => htmlspecialchars ($_POST['matiereTableRonde']),
                'autre_table' => htmlspecialchars ($_POST['autreTable'])
                ];

                $req = "INSERT INTO lieux_tables_rondes (lieux_id, table_ronde_nombre, table_ronde_dimension, table_ronde_nombre_de_personnes, table_ronde_matiere, autre_table) 
                                VALUES (:lieux_id, :table_ronde_nombre, :table_ronde_dimension, :table_ronde_nombre_de_personnes, :table_ronde_matiere ,:autre_table)";
                $requete = $connexion->prepare($req);
                $requete->execute($data13);


        $data14 = [
                'lieux_id' => $lieu,
                'date1' => htmlspecialchars ($_POST['date1']),
                'date2' => htmlspecialchars ($_POST['date2']),
                'date3' => htmlspecialchars ($_POST['date3']),
                'date4' => htmlspecialchars ($_POST['date4']),
                'date5' => htmlspecialchars ($_POST['date5']),
                'date6' => htmlspecialchars ($_POST['date6']),
                'heure1' => htmlspecialchars ($_POST['heure1']),
                'heure2' => htmlspecialchars ($_POST['heure2']),
                'heure3' => htmlspecialchars ($_POST['heure3']),
                'heure4' => htmlspecialchars ($_POST['heure4']),
                'heure5' => htmlspecialchars ($_POST['heure5']),
                'heure6' => htmlspecialchars ($_POST['heure6']),
                'tarif_saison_haute1' => htmlspecialchars ($_POST['tarifHaute1']),
                'tarif_moyenne_saison1' => htmlspecialchars ($_POST['tarifMoy1']),
                'tarif_basse_saison1' => htmlspecialchars ($_POST['tarifBas1']),
                'tarif_saison_haute2' => htmlspecialchars ($_POST['tarifHaute2']),
                'tarif_moyenne_saison2' => htmlspecialchars ($_POST['tarifMoy2']),
                'tarif_basse_saison2' => htmlspecialchars ($_POST['tarifBas2']),
                'tarif_saison_haute3' => htmlspecialchars ($_POST['tarifHaute3']),
                'tarif_moyenne_saison3' => htmlspecialchars ($_POST['tarifMoy3']),
                'tarif_basse_saison3' => htmlspecialchars ($_POST['tarifBas3']),
                'nettoyage_inclue' => htmlspecialchars ($_POST['nettoyageInclue']),
                'tarif_nettoyage' => htmlspecialchars ($_POST['tarifNettoie']),
                'restauration_impose' => htmlspecialchars ($_POST['restoImposer']),
                'tarif_restauration' => htmlspecialchars ($_POST['tarifResto'])
                ];
        
                $req = "INSERT INTO lieux_tarifs (lieux_id, date1, date2, date3, date4, date5, date6, heure1, heure2, heure3, heure4, heure5, heure6, tarif_saison_haute1, tarif_moyenne_saison1, 
                                        tarif_basse_saison1, tarif_saison_haute2, tarif_moyenne_saison2, tarif_basse_saison2, tarif_saison_haute3, tarif_moyenne_saison3, 
                                        tarif_basse_saison3, nettoyage_inclue, tarif_nettoyage, restauration_impose, tarif_restauration) 
                                VALUES (:lieux_id, :date1, :date2, :date3, :date4, :date5, :date6, :heure1, :heure2, :heure3, :heure4, :heure5, :heure6, :tarif_saison_haute1, :tarif_moyenne_saison1, 
                                        :tarif_basse_saison1, :tarif_saison_haute2, :tarif_moyenne_saison2, :tarif_basse_saison2, :tarif_saison_haute3, :tarif_moyenne_saison3, 
                                        :tarif_basse_saison3, :nettoyage_inclue, :tarif_nettoyage, :restauration_impose, :tarif_restauration)";
                $requete = $connexion->prepare($req);
                $requete->execute($data14);


header('location:../../HTML/affichage/listeLieux.php');


}

?>
<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");


if (isset($_POST['submit'])){


        $data = [
                'photographe_nom' => htmlspecialchars ($_POST['nomPhoto']),
                'photographe_prenom' => htmlspecialchars ($_POST['prenomPhoto']),
                'photographe_telephone' => htmlspecialchars ($_POST['telPhoto']),
                'photographe_mail' => htmlspecialchars ($_POST['mailPhoto']),
                'delai_livraison' => htmlspecialchars ($_POST['delaiLivrai']),
                'retouche_photo' => htmlspecialchars ($_POST['retouchePhoto']),
                'photographe_remarque' => htmlspecialchars ($_POST['remarquePhoto'])
                ];

                $photographe = "";
                $req="INSERT INTO photographes (photographe_nom, photographe_prenom, photographe_telephone, photographe_mail, delai_livraison, retouche_photo, photographe_remarque) 
                                VALUE (:photographe_nom, :photographe_prenom, :photographe_telephone, :photographe_mail, :delai_livraison, :retouche_photo,  :photographe_remarque)";

                $requete = $connexion->prepare($req);
                $requete->execute($data);
                $photographe = $connexion->lastInsertId();



        $data2 = [
                'photographes_id' => $photographe,
                'deplacement_validation' => htmlspecialchars ($_POST['deplacementPhoto']),
                'deplacement_supplement' => htmlspecialchars ($_POST['supplementPhoto']),
                'prix_au_km' => htmlspecialchars ($_POST['prixKMPhoto']),
                'deplacement_zone' => htmlspecialchars ($_POST['zone'])
                ];
            
                $req="INSERT INTO deplacements (photographes_id, deplacement_validation, deplacement_supplement, prix_au_km, deplacement_zone) 
                                VALUE (:photographes_id, :deplacement_validation, :deplacement_supplement, :prix_au_km, :deplacement_zone)";
                $requete = $connexion->prepare($req);
                $requete->execute($data2);


        $data3 = [
                'photographes_id' => $photographe,
                'validation_photobooth' => htmlspecialchars ($_POST['photoBooth']),
                'option_photobooth' => htmlspecialchars ($_POST['optionBooth'])
                ];
            
                $req="INSERT INTO photographes_photobooths (photographes_id, validation_photobooth, option_photobooth) 
                                VALUE (:photographes_id, :validation_photobooth, :option_photobooth)";
                $requete = $connexion->prepare($req);
                $requete->execute($data3);


        $data4 = [
                'photographes_id' => $photographe,
                'numerique' => htmlspecialchars ($_POST['numeriquePhoto']),
                'argentine' => htmlspecialchars ($_POST['argentPhoto']),
                'noir_et_blanc' => htmlspecialchars ($_POST['noirEtBlanc']),
                'autre_style' => htmlspecialchars ($_POST['autrePhoto'])
                ];
        
                $req="INSERT INTO photographes_styles (photographes_id, numerique, argentine, noir_et_blanc, autre_style) 
                                VALUE (:photographes_id, :numerique, :argentine, :noir_et_blanc, :autre_style)";
                $requete = $connexion->prepare($req);
                $requete->execute($data4);


        $data5 = [
                'photographes_id' => $photographe,
                'photographes_dvd' => htmlspecialchars ($_POST['dvd']),
                'photographes_coffret' => htmlspecialchars ($_POST['coffret']),
                'photographes_tirages' => htmlspecialchars ($_POST['tirages']),
                'photographes_usb' => htmlspecialchars ($_POST['usb']),
                'photographes_Autre' => htmlspecialchars ($_POST['autreSupport'])
                ];
        
                $req="INSERT INTO photographes_supports (photographes_id, photographes_dvd, photographes_coffret, photographes_tirages, photographes_usb, photographes_Autre) 
                        VALUE (:photographes_id, :photographes_dvd, :photographes_coffret, :photographes_tirages, :photographes_usb, :photographes_Autre)";
                $requete = $connexion->prepare($req);
                $requete->execute($data5);


        $data6 = [
                'photographes_id' => $photographe,
                'photographe_formule5' => htmlspecialchars ($_POST['formule5']),
                'photographe_formule6' => htmlspecialchars ($_POST['formule6']),
                'photographe_formule10' => htmlspecialchars ($_POST['formule10']),
                'photographe_formule11' => htmlspecialchars ($_POST['formule11']),
                'photographe_tarif1' => htmlspecialchars ($_POST['tarif1']),
                'photographe_tarif2' => htmlspecialchars ($_POST['tarif2']),
                'photographe_tarif3' => htmlspecialchars ($_POST['tarif3']),
                'photographe_tarif4' => htmlspecialchars ($_POST['tarif4']),
                'photographe_tarif5' => htmlspecialchars ($_POST['tarif5']),
                'photographe_tarif6' => htmlspecialchars ($_POST['tarif6']),
                'photographe_tarif7' => htmlspecialchars ($_POST['tarif7']),
                'photographe_tarif8' => htmlspecialchars ($_POST['tarif8']),
                'photographe_tarif9' => htmlspecialchars ($_POST['tarif9']),
                'photographe_tarif10' => htmlspecialchars ($_POST['tarif10']),
                'photographe_tarif11' => htmlspecialchars ($_POST['tarif11']),
                'photographe_formule_horaire' => htmlspecialchars ($_POST['autreFormuleHoraire']),
                'photographe_formule_moment' => htmlspecialchars ($_POST['autreFormuleMoment'])
                ];
                
                $req="INSERT INTO photographes_tarifs (photographes_id, photographe_formule5, photographe_formule6, photographe_formule10, photographe_formule11, photographe_tarif1, 
                                                        photographe_tarif2, photographe_tarif3, photographe_tarif4, photographe_tarif5, photographe_tarif6, photographe_tarif7, 
                                                        photographe_tarif8, photographe_tarif9, photographe_tarif10, photographe_tarif11, photographe_formule_horaire, 
                                                        photographe_formule_moment) 
                                VALUE (:photographes_id, :photographe_formule5, :photographe_formule6, :photographe_formule10, :photographe_formule11, :photographe_tarif1, 
                                        :photographe_tarif2, :photographe_tarif3, :photographe_tarif4, :photographe_tarif5, :photographe_tarif6, :photographe_tarif7, :photographe_tarif8, 
                                        :photographe_tarif9, :photographe_tarif10, :photographe_tarif11, :photographe_formule_horaire, :photographe_formule_moment)";
                $requete = $connexion->prepare($req);
                $requete->execute($data6);




header('location:../../HTML/affichage/listePhotographes.php');

}

?>
<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");


$marie = $_GET['numMarie'];

if (isset($_POST['submit'])){


    $data = [
        'maries_1_id' => $marie,
        'marie2_nom' => htmlspecialchars ($_POST['nom']),
        'marie2_prenom' => htmlspecialchars ($_POST['prenom']),
        'marie2_telephone' => htmlspecialchars ($_POST['tel']),
        'marie2_adresse' => htmlspecialchars ($_POST['adresse']),
        'marie2_email' => htmlspecialchars ($_POST['email']),
        'marie2_profession' => htmlspecialchars ($_POST['profession']),
        'marie2_date_naissance' => htmlspecialchars ($_POST['naissance']),
        'marie2_etat_civil' => htmlspecialchars ($_POST['sexe'])
        ];

        $marie2 = "";
        $req = "INSERT INTO maries_2 (maries_1_id, marie2_nom, marie2_prenom, marie2_telephone, marie2_adresse, marie2_email, marie2_profession, marie2_date_naissance, marie2_etat_civil) 
                        VALUES (:maries_1_id, :marie2_nom, :marie2_prenom, :marie2_telephone, :marie2_adresse, :marie2_email, :marie2_profession, :marie2_date_naissance, :marie2_etat_civil)";
        $requete = $connexion->prepare($req);
        $requete->execute($data);
        $marie2 = $connexion->lastInsertID();

    $data = [
        'marie2_id' => $marie2,
        'temoin3_nom' => htmlspecialchars ($_POST['nomTemoin3']),
        'temoin3_prenom' => htmlspecialchars ($_POST['prenomTemoin3']),
        'temoin3_telephone' => htmlspecialchars ($_POST['telTemoin3']),
        'temoin3_etat_civil' => htmlspecialchars ($_POST['sexeTemoin3'])
        ];


        $req = "INSERT INTO temoins_3 (marie2_id, temoin3_nom, temoin3_prenom, temoin3_telephone, temoin3_etat_civil) 
                        VALUES (:marie2_id, :temoin3_nom, :temoin3_prenom, :temoin3_telephone, :temoin3_etat_civil)";
        $requete = $connexion->prepare($req);
        $requete->execute($data);

    if (!empty($_POST['sexeTemoin4'])){

    $data2 = [
        'marie2_id' => $marie2,
        'temoin4_nom' => htmlspecialchars ($_POST['nomTemoin4']),
        'temoin4_prenom' => htmlspecialchars ($_POST['prenomTemoin4']),
        'temoin4_telephone' => htmlspecialchars ($_POST['telTemoin4']),
        'temoin4_etat_civil' => htmlspecialchars ($_POST['sexeTemoin4'])
        ];

        $req = "INSERT INTO temoins_4 (marie2_id, temoin4_nom, temoin4_prenom, temoin4_telephone, temoin4_etat_civil) 
                        VALUES (:marie2_id, :temoin4_nom, :temoin4_prenom, :temoin4_telephone, :temoin4_etat_civil)";
        $requete = $connexion->prepare($req);
        $requete->execute($data2);


        }else {
            $temoin4 = 1; // on attribut la valeur '1' par defaut si les champ $_POST ne sont pas remplie ce qui correspond à la valeur 'aucun' dans la bdd
        };


// on redirige vers le second formulaire

header("location:../../HTML/ajout/ajoutOrganisations.php?numMarie=".$marie."");
}

?>
<?php

include("../connexion/connexion.php");


if (isset($_POST['submit'])){


    $date = [
        'fleuriste_nom' => htmlspecialchars ($_POST['nomFleur']),
        'fleuriste_prenom' => htmlspecialchars ($_POST['prenomFleur']),
        'fleuriste_telephone' => htmlspecialchars ($_POST['telFleur']),
        'fleuriste_mail' => htmlspecialchars ($_POST['mailFleur']),
        'fleuriste_adresse' => htmlspecialchars ($_POST['adresseFleur']),
        'fleuriste_saison' => htmlspecialchars ($_POST['fleurSaison']),
        'fleuriste_choix' => htmlspecialchars ($_POST['choixFleur']),
        'fleuriste_test_floral' => htmlspecialchars ($_POST['testFloral']),
        'fleuriste_location' => htmlspecialchars ($_POST['locationFleur']),
        'fleuriste_commande' => htmlspecialchars ($_POST['commande']),
        'fleuriste_rdv' => htmlspecialchars ($_POST['rdvFleur']),
        'fleuriste_autre_elements_decoration' => htmlspecialchars ($_POST['autreLocationFleur'])
        ];

        $fleuriste = "";
        $req="INSERT INTO fleuristes (fleuriste_nom, fleuriste_prenom, fleuriste_telephone, fleuriste_mail, fleuriste_adresse, fleuriste_saison, fleuriste_choix, fleuriste_test_floral, 
                        fleuriste_location, fleuriste_commande, fleuriste_rdv, fleuriste_autre_elements_decoration) 
                    VALUE (:fleuriste_nom, :fleuriste_prenom, :fleuriste_telephone, :fleuriste_mail, :fleuriste_adresse, :fleuriste_saison, :fleuriste_choix, :fleuriste_test_floral, 
                        :fleuriste_location, :fleuriste_commande, :fleuriste_rdv, :fleuriste_autre_elements_decoration)";
        $requete = $connexion->prepare($req);
        $requete->execute($date);
        $fleuriste = $connexion->lastInsertId();


    $data2 = [
        'fleuriste_id' => $fleuriste,
        'livraison_validation' => htmlspecialchars ($_POST['livraison']),
        'livraison_supplement' => htmlspecialchars ($_POST['supplementLivrai']),
        'prix_au_km' => htmlspecialchars ($_POST['PrixAuxKM']),
        'livraison_zone' => htmlspecialchars ($_POST['zoneLivrai'])
        ];

        $req="INSERT INTO fleuristes_livraisons (fleuriste_id, livraison_validation, livraison_supplement, prix_au_km, livraison_zone) 
                        VALUE (:fleuriste_id, :livraison_validation, :livraison_supplement, :prix_au_km, :livraison_zone)";
        $requete = $connexion->prepare($req);
        $requete->execute($data2);


    $date3 = [
        'fleuriste_id' => $fleuriste,
        'decoration_plafond' => htmlspecialchars ($_POST['decoPlafondFleur']),
        'decoration_voiture' => htmlspecialchars ($_POST['decoVoitureFleur']),
        'carterie' => htmlspecialchars ($_POST['carterieFleur']),
        'nappage' => htmlspecialchars ($_POST['nappageFleur']),
        'chemin' => htmlspecialchars ($_POST['cheminFleur']),
        'bougies' => htmlspecialchars ($_POST['bougiesFleur'])
        ];

        $req="INSERT INTO fleuristes_decorations (fleuriste_id, decoration_plafond, decoration_voiture, carterie, nappage, chemin, bougies) 
                        VALUE (:fleuriste_id, :decoration_plafond, :decoration_voiture, :carterie, :nappage, :chemin, :bougies)";
        $requete = $connexion->prepare($req);
        $requete->execute($date3);


    $date4 = [
        'fleuriste_id' => $fleuriste,
        'desinstallation_validation' => htmlspecialchars ($_POST['desinstal']),
        'desinstallation_supplement' => htmlspecialchars ($_POST['prixDesinstal']),
        ];

        $req="INSERT INTO fleuristes_desinstallations (fleuriste_id, desinstallation_validation, desinstallation_supplement) 
                        VALUE (:fleuriste_id, :desinstallation_validation, :desinstallation_supplement)";
        $requete = $connexion->prepare($req);
        $requete->execute($date4);


    $date5 = [
        'fleuriste_id' => $fleuriste,
        'installation_validation' => htmlspecialchars ($_POST['instal']),
        'installation_supplement' => htmlspecialchars ($_POST['prixInstal']),
        ];

        $req="INSERT INTO fleuristes_installations (fleuriste_id, installation_validation, installation_supplement) 
                        VALUE (:fleuriste_id, :installation_validation, :installation_supplement)";
        $requete = $connexion->prepare($req);
        $requete->execute($date5);



// on redirige vers la liste des fleuristes

header('location:../../HTML/affichage/listeFleuristes.php');

}

?>
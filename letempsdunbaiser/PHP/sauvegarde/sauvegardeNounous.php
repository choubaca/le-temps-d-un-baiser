<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");


if (isset($_POST['submit'])){


    $data = [
        'nounou_nom' => htmlspecialchars ($_POST['nomNounou']),
        'nounou_prenom' => htmlspecialchars ($_POST['prenomNounou']),
        'nounou_telephone' => htmlspecialchars ($_POST['telNounou']),
        'nounou_mail' => htmlspecialchars ($_POST['mailNounou']),
        'nounou_nombre_animateur' => htmlspecialchars ($_POST['nbAnimateurs']),
        'nounou_gere_enfant' => htmlspecialchars ($_POST['gereEnfantDiner']),
        'nounou_heure_limite' => htmlspecialchars ($_POST['heureLimiteNounou']),
        'nounou_laquelle' => htmlspecialchars ($_POST['quelHeure']),
        'nounou_remarque' => htmlspecialchars ($_POST['remarques']),
        ];
    
        $nounou = "";
        $req="INSERT INTO nounous (nounou_nom, nounou_prenom, nounou_telephone, nounou_mail, nounou_nombre_animateur, nounou_gere_enfant, nounou_heure_limite, nounou_laquelle, 
                            nounou_remarque) 
                        VALUE (:nounou_nom, :nounou_prenom, :nounou_telephone, :nounou_mail,  :nounou_nombre_animateur, :nounou_gere_enfant, :nounou_heure_limite, :nounou_laquelle, 
                            :nounou_remarque)";
        
        
        $requete = $connexion->prepare($req);
        $requete->execute($data);
        $nounou = $connexion->lastInsertId();


    $data2 = [
        'nounous_id' => $nounou,
        'tables' => htmlspecialchars ($_POST['tables']),
        'chaise' => htmlspecialchars ($_POST['chaise']),
        'sono' => htmlspecialchars ($_POST['sono']),
        'autre_besoin' => htmlspecialchars ($_POST['autres'])
        ];

        $req="INSERT INTO nounous_besoins (nounous_id, tables, chaise, sono, autre_besoin) 
                        VALUE (:nounous_id, :tables, :chaise, :sono, :autre_besoin)";
        $requete = $connexion->prepare($req);
        $requete->execute($data2);


    $data3 = [
        'nounous_id' => $nounou,
        'animations_divers' => htmlspecialchars ($_POST['animationNounou']),
        'quelle_animations' => htmlspecialchars ($_POST['lesquelles'])
        ];

        $req="INSERT INTO animations (nounous_id, animations_divers, quelle_animations) 
                        VALUE (:nounous_id, :animations_divers, :quelle_animations)";
        $requete = $connexion->prepare($req);
        $requete->execute($data3);


    $data4 = [
        'nounous_id' => $nounou,
        'deplacement_validation' => htmlspecialchars ($_POST['deplacementNounou']),
        'deplacement_supplement' => htmlspecialchars ($_POST['supplementNounou']),
        'prix_au_km' => htmlspecialchars ($_POST['prixKMNounou']),
        'deplacement_zone' => htmlspecialchars ($_POST['zone'])
        ];

        $req="INSERT INTO deplacements (nounous_id, deplacement_validation, deplacement_supplement, prix_au_km, deplacement_zone) 
                        VALUE (:nounous_id, :deplacement_validation, :deplacement_supplement, :prix_au_km, :deplacement_zone)";
        $requete = $connexion->prepare($req);
        $requete->execute($data4);


    $data5 = [
        'nounous_id' => $nounou,
        'age_minimum' => htmlspecialchars ($_POST['ageMinEnfant']),
        'age_maximum' => htmlspecialchars ($_POST['ageMaxEnfant']),
        'nombre_minimum' => htmlspecialchars ($_POST['nbrEnfantsMin']),
        'nombre_maximum' => htmlspecialchars ($_POST['nbrEnfantstMax'])
        ];

        $req="INSERT INTO nounous_enfants (nounous_id, age_minimum, age_maximum, nombre_minimum, nombre_maximum) 
                        VALUE (:nounous_id, :age_minimum, :age_maximum, :nombre_minimum, :nombre_maximum)";
        $requete = $connexion->prepare($req);
        $requete->execute($data5);


    $data6 = [
        'nounous_id' => $nounou,
        'validation_qualification' => htmlspecialchars ($_POST['qualifAnim']),
        'quelles_qualification' => htmlspecialchars ($_POST['quellesQualif'])
        ];

        $req="INSERT INTO nounous_qualifications_animateurs (nounous_id, validation_qualification, quelles_qualification) 
                        VALUE (:nounous_id, :validation_qualification, :quelles_qualification)";
        $requete = $connexion->prepare($req);
        $requete->execute($data6);


    $data7 = [
        'nounous_id' => $nounou,
        'nounou_formule1' => htmlspecialchars ($_POST['formule1']),
        'nounou_formule2' => htmlspecialchars ($_POST['formule2']),
        'nounou_formule3' => htmlspecialchars ($_POST['formule3']),
        'nounou_formule4' => htmlspecialchars ($_POST['formule4']),
        'nounou_tarif1' => htmlspecialchars ($_POST['tarif1']),
        'nounou_tarif2' => htmlspecialchars ($_POST['tarif2']),
        'nounou_tarif3' => htmlspecialchars ($_POST['tarif3']),
        'nounou_tarif4' => htmlspecialchars ($_POST['tarif4'])
        ];

        $req="INSERT INTO nounous_tarifs (nounous_id, nounou_formule1, nounou_formule2, nounou_formule3, nounou_formule4, nounou_tarif1, nounou_tarif2, nounou_tarif3, nounou_tarif4) 
                        VALUE (:nounous_id, :nounou_formule1, :nounou_formule2, :nounou_formule3, :nounou_formule4, :nounou_tarif1, :nounou_tarif2, :nounou_tarif3, :nounou_tarif4)";
        $requete = $connexion->prepare($req);
        $requete->execute($data7);



header('location:../../HTML/affichage/listeNounous.php');

}

?>
<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");


if (isset($_POST['submit'])){


    $data = [
        'dj_nom' => htmlspecialchars ($_POST['nomDJ']),
        'dj_prenom' => htmlspecialchars ($_POST['prenomDJ']),
        'dj_telephone' => htmlspecialchars ($_POST['telDJ']),
        'dj_mail' => htmlspecialchars ($_POST['mailDJ']),
        'dj_heure_limite' => htmlspecialchars ($_POST['heureDJ']),
        'dj_laquelle' => htmlspecialchars ($_POST['heureLimiteDJ']),
        'dj_rechange' => htmlspecialchars ($_POST['rechangeDJ']),
        'dj_style_musical' => htmlspecialchars ($_POST['styleMusical']),
        'dj_travail_seul' => htmlspecialchars ($_POST['seulDJ']),
        'dj_musique_demande' => htmlspecialchars ($_POST['musiqueDemande']),
        'dj_habit' => htmlspecialchars ($_POST['habitDJ'])
        ];
        
        $dj="";
        $req="INSERT INTO djs (dj_nom, dj_prenom, dj_telephone, dj_mail, dj_heure_limite, dj_laquelle, dj_rechange, dj_style_musical, dj_travail_seul, dj_musique_demande, dj_habit) 
                    VALUE (:dj_nom, :dj_prenom, :dj_telephone, :dj_mail, :dj_heure_limite, :dj_laquelle, :dj_rechange, :dj_style_musical, :dj_travail_seul, :dj_musique_demande, :dj_habit)";
        $requete = $connexion->prepare($req);
        $requete->execute($data);
        $dj = $connexion->lastInsertId();


    $data2 = [
            'djs_id' => $dj,
            'animations_divers' => htmlspecialchars ($_POST['animationDJ']),
            'quelle_animations' => htmlspecialchars ($_POST['lesquels'])
        ];

        $req="INSERT INTO animations (djs_id, animations_divers, quelle_animations) 
                        VALUE (:djs_id, :animations_divers, :quelle_animations)";
        $requete = $connexion->prepare($req);
        $requete->execute($data2);


    $data3 = [
        'djs_id' => $dj,
        'deplacement_validation' => htmlspecialchars ($_POST['deplacementDJ']),
        'deplacement_supplement' => htmlspecialchars ($_POST['supplementDJ']),
        'prix_au_km' => htmlspecialchars ($_POST['prixKMDJ']),
        'deplacement_zone' => htmlspecialchars ($_POST['zone'])
        ];

        $req="INSERT INTO deplacements (djs_id, deplacement_validation, deplacement_supplement, prix_au_km, deplacement_zone) 
                        VALUE (:djs_id, :deplacement_validation, :deplacement_supplement, :prix_au_km, :deplacement_zone)";
        $requete = $connexion->prepare($req);
        $requete->execute($data3);


    $data4 = [
        'djs_id' => $dj,
        'dj_sono' => htmlspecialchars ($_POST['sonoDJ']),
        'dj_videoprojecteur' => htmlspecialchars ($_POST['videoprojecteurDJ']),
        'dj_eclairage' => htmlspecialchars ($_POST['eclairageDJ']),
        'dj_machine_a_fumee' => htmlspecialchars ($_POST['machineAFumeeDJ']),
        'dj_micro' => htmlspecialchars ($_POST['microDJ'])
        ];

        $req="INSERT INTO djs_materiels (djs_id, dj_sono, dj_videoprojecteur, dj_eclairage, dj_machine_a_fumee, dj_micro) 
                        VALUE (:djs_id, :dj_sono, :dj_videoprojecteur, :dj_eclairage, :dj_machine_a_fumee, :dj_micro)";
        $requete = $connexion->prepare($req);
        $requete->execute($data4);


    $data5 = [
        'djs_id' => $dj,
        'dj_tarif1' => htmlspecialchars ($_POST['tarif1']),
        'dj_tarif2' => htmlspecialchars ($_POST['tarif2']),
        'dj_tarif3' => htmlspecialchars ($_POST['tarif3']),
        'dj_tarif4' => htmlspecialchars ($_POST['tarif4']),
        'dj_tarif5' => htmlspecialchars ($_POST['tarif5']),
        'dj_option1' => htmlspecialchars ($_POST['option1']),
        'dj_option2' => htmlspecialchars ($_POST['option2'])
        ];

        $req="INSERT INTO djs_tarifs (djs_id, dj_tarif1, dj_tarif2, dj_tarif3, dj_tarif4, dj_tarif5, dj_option1, dj_option2) 
                        VALUE (:djs_id, :dj_tarif1, :dj_tarif2, :dj_tarif3, :dj_tarif4, :dj_tarif5, :dj_option1, :dj_option2)";
        $requete = $connexion->prepare($req);
        $requete->execute($data5);


header('location:../../HTML/affichage/listeDJ.php');

}

?>
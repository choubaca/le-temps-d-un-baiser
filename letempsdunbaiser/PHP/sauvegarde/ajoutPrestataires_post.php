<?php

include("../../PHP/connexion/connexion.php");


$pdoDjs = "SELECT id, dj_nom FROM djs";

$requete = $connexion->prepare($pdoDjs);
$requete->execute();

$djs = $requete->fetchAll();


$pdoFleuristes = "SELECT id, fleuriste_nom FROM fleuristes";

$requete = $connexion->prepare($pdoFleuristes);
$requete->execute();

$fleuristes = $requete->fetchAll();


$pdoLieux = "SELECT id, contact_nom FROM lieux_contacts";

$requete = $connexion->prepare($pdoLieux);
$requete->execute();

$lieux = $requete->fetchAll();


$pdoNounous = "SELECT id, nounou_nom FROM nounous";

$requete = $connexion->prepare($pdoNounous);
$requete->execute();

$nounous = $requete->fetchAll();


$pdoPhotographes = "SELECT id, photographe_nom FROM photographes";

$requete = $connexion->prepare($pdoPhotographes);
$requete->execute();

$photographes = $requete->fetchAll();


$pdoTraiteurs = "SELECT id, traiteur_nom FROM traiteurs";

$requete = $connexion->prepare($pdoTraiteurs);
$requete->execute();

$traiteurs = $requete->fetchAll();

$pdoVideastes = "SELECT id, videaste_nom FROM videastes";

$requete = $connexion->prepare($pdoVideastes);
$requete->execute();

$videastes = $requete->fetchAll();

?>
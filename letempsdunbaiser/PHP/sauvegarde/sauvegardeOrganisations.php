<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");


$marie = $_GET['numMarie'];

if (isset($_POST['submit'])){


        $data = [
                'maries_1_id' => $marie,
                'date_mariage' => htmlspecialchars ($_POST['dateDuMariage']),
                'nombre_adulte' => htmlspecialchars ($_POST['nbrAdulte']),
                'nombre_enfant' => htmlspecialchars ($_POST['nbrEnfant']),
                'lieu_ceremonie' => htmlspecialchars ($_POST['lieuCeremonie']),
                'lieu_mairie' => htmlspecialchars ($_POST['mairie']),
                'lieu_ceremonie_religieuse' => htmlspecialchars ($_POST['Ceremoniereligieuse']),
                'lieu_ceremonie_laique' => htmlspecialchars ($_POST['Ceremonielaïque']),
                'budget_reception' => htmlspecialchars ($_POST['budgetReception']),
                'event_ceremonie' => htmlspecialchars ($_POST['eventCeremonie']),
                'event_reception' => htmlspecialchars ($_POST['eventReception']),
                'type_de_lieu' => htmlspecialchars ($_POST['typeDeLieu']),
                'autre_type_de_lieu' => htmlspecialchars ($_POST['autreTypeDeLieu']),
                'localisation_geographique' => htmlspecialchars ($_POST['localisationGeo']),
                'autre' => htmlspecialchars ($_POST['autre'])
                ];
        
                $organisation = "";
                $req = "INSERT INTO organisations (maries_1_id, date_mariage, nombre_adulte, nombre_enfant, lieu_ceremonie, lieu_mairie, lieu_ceremonie_religieuse, lieu_ceremonie_laique, 
                                                budget_reception, event_ceremonie, event_reception, type_de_lieu, autre_type_de_lieu, localisation_geographique, autre) 
                                VALUES (:maries_1_id, :date_mariage, :nombre_adulte, :nombre_enfant, :lieu_ceremonie, :lieu_mairie, :lieu_ceremonie_religieuse, :lieu_ceremonie_laique, 
                                        :budget_reception, :event_ceremonie, :event_reception, :type_de_lieu, :autre_type_de_lieu, :localisation_geographique, :autre)";
                $requete = $connexion->prepare($req);
                $requete->execute($data);
                $organisation = $connexion->lastInsertId();


        $data2 = [
                'organisation_id' => $organisation,
                'espace_exterieur' => htmlspecialchars ($_POST['espaceExt']),
                'piscine' => htmlspecialchars ($_POST['piscine']),
                'chambre_maries' => htmlspecialchars ($_POST['chambreMaries']),
                ];
        
                $req = "INSERT INTO organisations_criteres (organisation_id, espace_exterieur, piscine, chambre_maries) 
                                VALUES (:organisation_id, :espace_exterieur, :piscine, :chambre_maries)";
                $requete = $connexion->prepare($req);
                $requete->execute($data2);


        $data3 = [
                'organisation_id' => $organisation,
                'hebergement_sur_place' => htmlspecialchars ($_POST['hebergementSurPlace']),
                'nombre_hebergement' => htmlspecialchars ($_POST['combienHebergement']),
                'autre_hebergement' => htmlspecialchars ($_POST['autreHebergement'])
                ];
        
                $req = "INSERT INTO organisations_hebergements (organisation_id, hebergement_sur_place, nombre_hebergement, autre_hebergement) 
                                VALUES (:organisation_id, :hebergement_sur_place, :nombre_hebergement, :autre_hebergement)";
                $requete = $connexion->prepare($req);
                $requete->execute($data3);


        $data4 = [
                'organisation_id' => $organisation,
                'vin_dhonneur' => htmlspecialchars ($_POST['convivesVinDhonneur']),
                'brunch' => htmlspecialchars ($_POST['convivesAuDiner']),
                'diner' => htmlspecialchars ($_POST['convivesAuBrunch']),
                ];
        
                $req = "INSERT INTO organisations_convives (organisation_id, vin_dhonneur, brunch, diner) 
                                VALUES (:organisation_id, :vin_dhonneur, :brunch, :diner)";
                $requete = $connexion->prepare($req);
                $requete->execute($data4);


        $data5 = [
                'organisation_id' => $organisation,
                'mise_en_bouche' => htmlspecialchars ($_POST['miseEnBouche']),
                'viande' => htmlspecialchars ($_POST['viande']),
                'entree' => htmlspecialchars ($_POST['entree']),
                'fromage' => htmlspecialchars ($_POST['fromage']),
                'poisson' => htmlspecialchars ($_POST['poisson']),
                'dessert' => htmlspecialchars ($_POST['dessert']),
                'nombre_de_piece' => htmlspecialchars ($_POST['nbrPieceCocktail']),
                'brunch_lendemain' => htmlspecialchars ($_POST['brunchLendemain']),
                'type_de_diner' => htmlspecialchars ($_POST['TypeDeDiner']),
                'animation_culinaire' => htmlspecialchars ($_POST['animationCulinaire'])
                ];
        
                $req = "INSERT INTO organisations_deroules (organisation_id, mise_en_bouche, viande, entree, fromage, poisson, dessert, nombre_de_piece, brunch_lendemain, 
                                        type_de_diner, animation_culinaire) 
                                VALUES (:organisation_id, :mise_en_bouche, :viande, :entree, :fromage, :poisson, :dessert, :nombre_de_piece, :brunch_lendemain, :type_de_diner, 
                                        :animation_culinaire)";
                $requete = $connexion->prepare($req);
                $requete->execute($data5);
        

        $data6 = [
                'organisation_id' => $organisation,
                'organisation_dessert_piece_montee_choux' => htmlspecialchars ($_POST['pieceMonteeChoux']),
                'organisation_dessert_piece_montee_macarons' => htmlspecialchars ($_POST['pieceMonteeMacaron']),
                'organisation_dessert_wedding_cake' => htmlspecialchars ($_POST['weddingCake']),
                'organisation_dessert_autre_dessert' => htmlspecialchars ($_POST['autreDessert'])
                ];
        
                $req = "INSERT INTO organisations_desserts (organisation_id, organisation_dessert_piece_montee_choux, organisation_dessert_piece_montee_macarons, 
                                        organisation_dessert_wedding_cake, organisation_dessert_autre_dessert) 
                                VALUES (:organisation_id, :organisation_dessert_piece_montee_choux, :organisation_dessert_piece_montee_macarons, :organisation_dessert_wedding_cake, 
                                        :organisation_dessert_autre_dessert)";
                $requete = $connexion->prepare($req);
                $requete->execute($data6);


        $data7 = [
                'organisation_id' => $organisation,
                'organisation_vin_dhonneur_eau' => htmlspecialchars ($_POST['vinDhonneurEau']),
                'organisation_vin_dhonneur_soft' => htmlspecialchars ($_POST['vinDhonneurSofts']),
                'organisation_vin_dhonneur_cocktail' => htmlspecialchars ($_POST['vinDhonneurCocktail']),
                'organisation_vin_dhonneur_alcool' => htmlspecialchars ($_POST['vinDhonneurAlcools']),
                'organisation_vin_dhonneur_champagne' => htmlspecialchars ($_POST['vinDhonneurChampagne'])
                ];
        
                $req = "INSERT INTO organisations_vins_dhonneur (organisation_id, organisation_vin_dhonneur_eau, organisation_vin_dhonneur_soft, organisation_vin_dhonneur_cocktail, 
                                        organisation_vin_dhonneur_alcool, organisation_vin_dhonneur_champagne) 
                                VALUES (:organisation_id, :organisation_vin_dhonneur_eau, :organisation_vin_dhonneur_soft, :organisation_vin_dhonneur_cocktail, 
                                        :organisation_vin_dhonneur_alcool, :organisation_vin_dhonneur_champagne)";
                $requete = $connexion->prepare($req);
                $requete->execute($data7);


        $data8 = [
                'organisation_id' => $organisation,
                'organisation_diner_eau' => htmlspecialchars ($_POST['dinerEau']),
                'organisation_diner_softs' => htmlspecialchars ($_POST['dinerSofts']),
                'organisation_diner_vin' => htmlspecialchars ($_POST['dinerVin']),
                'organisation_diner_alcools' => htmlspecialchars ($_POST['dinerAlcools'])
                ];
        
                $req = "INSERT INTO organisations_diners (organisation_id, organisation_diner_eau, organisation_diner_softs, organisation_diner_vin, organisation_diner_alcools) 
                                VALUES (:organisation_id, :organisation_diner_eau, :organisation_diner_softs, :organisation_diner_vin, :organisation_diner_alcools)";
                $requete = $connexion->prepare($req);
                $requete->execute($data8);


        $data9 = [
                'organisation_id' => $organisation,
                'organisation_soiree_eau' => htmlspecialchars ($_POST['soireeEau']),
                'organisation_soiree_softs' => htmlspecialchars ($_POST['soireeSofts']),
                'organisation_soiree_alcool' => htmlspecialchars ($_POST['soireeAlcools']),
                'organisation_soiree_champagne' => htmlspecialchars ($_POST['soireeChampagne'])
                ];
        
                $req = "INSERT INTO organisations_soirees (organisation_id, organisation_soiree_eau, organisation_soiree_softs, organisation_soiree_alcool, organisation_soiree_champagne) 
                                VALUES (:organisation_id, :organisation_soiree_eau, :organisation_soiree_softs, :organisation_soiree_alcool, :organisation_soiree_champagne)";
                $requete = $connexion->prepare($req);
                $requete->execute($data9);


        $data10 = [
                'organisation_id' => $organisation,
                'dj' => htmlspecialchars ($_POST['DJ']),
                'gospel' => htmlspecialchars ($_POST['Gospel']),
                'orchestre' => htmlspecialchars ($_POST['Orchestre']),
                'chant' => htmlspecialchars ($_POST['Chant']),
                'style' => htmlspecialchars ($_POST['styleAnimationMusical'])
                ];
        
                $req = "INSERT INTO organisations_animations_musicales (organisation_id, dj, gospel, orchestre, chant, style) 
                                VALUES (:organisation_id, :dj, :gospel, :orchestre, :chant, :style)";
                $requete = $connexion->prepare($req);
                $requete->execute($data10);


        $data11 = [
                'organisation_id' => $organisation,
                'organisation_photographe_formule' => htmlspecialchars ($_POST['formulePhotographe']),
                'organisation_photographe_style' => htmlspecialchars ($_POST['stylePhotographe']),
                'organisation_photographe_support' => htmlspecialchars ($_POST['supportPhotographe']),
                ];
                
                $req = "INSERT INTO organisations_photographes (organisation_id, organisation_photographe_formule, organisation_photographe_style, organisation_photographe_support) 
                                VALUES (:organisation_id, :organisation_photographe_formule, :organisation_photographe_style, :organisation_photographe_support)";
                $requete = $connexion->prepare($req);
                $requete->execute($data11);


        $data12 = [
                'organisation_id' => $organisation,
                'organisation_video_formule' => htmlspecialchars ($_POST['formuleVideo']),
                'organisation_video_style' => htmlspecialchars ($_POST['styleVideo']),
                'organisation_video_format' => htmlspecialchars ($_POST['formatVideo']),
                'organisation_video_support' => htmlspecialchars ($_POST['supportVideo'])
                ];
        
                $req = "INSERT INTO organisations_videos (organisation_id, organisation_video_formule, organisation_video_style, organisation_video_format, organisation_video_support) 
                                VALUES (:organisation_id, :organisation_video_formule, :organisation_video_style, :organisation_video_format, :organisation_video_support)";
                $requete = $connexion->prepare($req);
                $requete->execute($data12);


        $data13 = [
                'organisation_id' => $organisation,
                'nombre_d_enfant' => htmlspecialchars ($_POST['nbrEnfantsBS']),
                'horaire' => htmlspecialchars ($_POST['horaireBS'])
                ];
        
                $req = "INSERT INTO organisations_baby_sitters (organisation_id, nombre_d_enfant, horaire) 
                                VALUES (:organisation_id, :nombre_d_enfant, :horaire)";
                $requete = $connexion->prepare($req);
                $requete->execute($data13);


        $data14 = [
                'organisation_id' => $organisation,
                'coiffure' => htmlspecialchars ($_POST['coiffure']),
                'maquillage' => htmlspecialchars ($_POST['maquillage']),
                'officiant' => htmlspecialchars ($_POST['officiant']),
                'location_voiture' => htmlspecialchars ($_POST['locationVoiture'])
                ];
        
                $req = "INSERT INTO organisations_autres_prestataires (organisation_id, coiffure, maquillage, officiant, location_voiture) 
                                VALUES (:organisation_id, :coiffure, :maquillage, :officiant, :location_voiture)";
                $requete = $connexion->prepare($req);
                $requete->execute($data14);

                
header("location:../../HTML/ajout/ajoutPrestataires.php?numMarie=".$marie."");

}

?>
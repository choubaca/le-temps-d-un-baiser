<?php

$validation = false;

if(isset($_POST['submit'])){
    
    include("../../PHP/connexion/connexion.php");
    
    $galerie = $_POST["numGalerie"];
    
    $destination = '../../IMG/';
    
    $extention = array('jpg','png','jpeg','gif');
    
    $valeurInserer = $message = $erreurExtention = $erreurUpload = "";
    
    $nomFichier = array_filter($_FILES['photogalerie']['name']);
    
    if(!empty($nomFichier)){
        
        foreach($_FILES['photogalerie']['name'] as $key=>$val){
            
            $nomFichier = strtolower(basename($_FILES['photogalerie']['name'][$key]));
            
            $destinationFichier = $destination . $nomFichier;
            
            $typeDeFichier = pathinfo($destinationFichier, PATHINFO_EXTENSION);
            
            if(in_array($typeDeFichier, $extention)){
                if(move_uploaded_file($_FILES['photogalerie']['tmp_name'][$key], $destinationFichier)){
                    $valeurInserer .= $nomFichier. '|';
                }else{
                    $erreurUpload .= $_FILES['photogalerie']['name'][$key].' | ';
                }
            }else{
                $erreurExtention .= $_FILES['photogalerie']['name'][$key].' | ';
            }
        }
        
        if(!empty($valeurInserer)){
                
            
            
            $valeurInserer = trim($valeurInserer, ',');
            
            $inserer = $connexion->query('INSERT INTO galeries_photos (galerie_id, nom_photo) VALUES ("'.$galerie.'", "'.$valeurInserer.'")');
            
            $message = "Les fichiers ont bien été téléchargés !";
            
            $validation = true;
            
            }
            
        if(!empty($erreurUpload)){
            
             $erreurUpload = "Les fichiers n'ont pas été téléchargés !";
             
             header("refresh: 2 ; url=../../HTML/ajout/ajoutPhotos.php?numGalerie=".$galerie."");
             
            }
        if(!empty($erreurExtention)){
            
            $erreurExtention = "L'extention des fichiers est mauvaise !";
            
            header("refresh: 2 ; url=../../HTML/ajout/ajoutPhotos.php?numGalerie=".$galerie."");
        
            }
    }
    
}

?>
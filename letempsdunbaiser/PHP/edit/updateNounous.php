<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");

$num = $_POST['numNounou'];

if (isset($_POST['submit'])){


    $data = [
        'nounou_nom' => htmlspecialchars ($_POST['nomNounou']),
        'nounou_prenom' => htmlspecialchars ($_POST['prenomNounou']),
        'nounou_telephone' => htmlspecialchars ($_POST['telNounou']),
        'nounou_mail' => htmlspecialchars ($_POST['mailNounou']),
        'nounou_nombre_animateur' => htmlspecialchars ($_POST['nbAnimateurs']),
        'nounou_gere_enfant' => htmlspecialchars ($_POST['gereEnfantDiner']),
        'nounou_heure_limite' => htmlspecialchars ($_POST['heureLimiteNounou']),
        'nounou_laquelle' => htmlspecialchars ($_POST['quelHeure']),
        'nounou_remarque' => htmlspecialchars ($_POST['remarques']),
        ];
    
        $req="UPDATE nounous
                        SET nounou_nom=:nounou_nom, nounou_prenom=:nounou_prenom, nounou_telephone=:nounou_telephone, nounou_mail=:nounou_mail,  nounou_nombre_animateur=:nounou_nombre_animateur, 
                        nounou_gere_enfant=:nounou_gere_enfant, nounou_heure_limite=:nounou_heure_limite, nounou_laquelle=:nounou_laquelle, nounou_remarque=:nounou_remarque
                            WHERE nounous.id = '$num'";
        
        $requete = $connexion->prepare($req);
        $requete->execute($data);

    $data2 = [
        'tables' => htmlspecialchars ($_POST['tables']),
        'chaise' => htmlspecialchars ($_POST['chaise']),
        'sono' => htmlspecialchars ($_POST['sono']),
        'autre_besoin' => htmlspecialchars ($_POST['autres'])
        ];

        $req="UPDATE nounous_besoins 
                        SET tables=:tables, chaise=:chaise, sono=:sono, autre_besoin=:autre_besoin
                            WHERE nounous_besoins.nounous_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($data2);


    $data3 = [
        'animations_divers' => htmlspecialchars ($_POST['animationNounou']),
        'quelle_animations' => htmlspecialchars ($_POST['lesquelles'])
        ];

        $req="UPDATE animations 
                        SET animations_divers=:animations_divers, quelle_animations=:quelle_animations
                            WHERE animations.nounous_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($data3);


    $data4 = [
        'deplacement_validation' => htmlspecialchars ($_POST['deplacementNounou']),
        'deplacement_supplement' => htmlspecialchars ($_POST['supplementNounou']),
        'prix_au_km' => htmlspecialchars ($_POST['prixKMNounou']),
        'deplacement_zone' => htmlspecialchars ($_POST['zone'])
        ];

        $req="UPDATE deplacements 
                        SET deplacement_validation=:deplacement_validation, deplacement_supplement=:deplacement_supplement, prix_au_km=:prix_au_km, deplacement_zone=:deplacement_zone
                            WHERE deplacements.nounous_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($data4);


    $data5 = [
        'age_minimum' => htmlspecialchars ($_POST['ageMinEnfant']),
        'age_maximum' => htmlspecialchars ($_POST['ageMaxEnfant']),
        'nombre_minimum' => htmlspecialchars ($_POST['nbrEnfantsMin']),
        'nombre_maximum' => htmlspecialchars ($_POST['nbrEnfantstMax'])
        ];

        $req="UPDATE nounous_enfants 
                        SET age_minimum=:age_minimum, age_maximum=:age_maximum, nombre_minimum=:nombre_minimum, nombre_maximum=:nombre_maximum
                            WHERE nounous_enfants.nounous_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($data5);


    $data6 = [
        'validation_qualification' => htmlspecialchars ($_POST['qualifAnim']),
        'quelles_qualification' => htmlspecialchars ($_POST['quellesQualif'])
        ];

        $req="UPDATE nounous_qualifications_animateurs 
                        SET validation_qualification=:validation_qualification, quelles_qualification=:quelles_qualification
                            WHERE nounous_qualifications_animateurs.nounous_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($data6);


    $data7 = [
        'nounou_formule1' => htmlspecialchars ($_POST['formule1']),
        'nounou_formule2' => htmlspecialchars ($_POST['formule2']),
        'nounou_formule3' => htmlspecialchars ($_POST['formule3']),
        'nounou_formule4' => htmlspecialchars ($_POST['formule4']),
        'nounou_tarif1' => htmlspecialchars ($_POST['tarif1']),
        'nounou_tarif2' => htmlspecialchars ($_POST['tarif2']),
        'nounou_tarif3' => htmlspecialchars ($_POST['tarif3']),
        'nounou_tarif4' => htmlspecialchars ($_POST['tarif4'])
        ];

        $req="UPDATE nounous_tarifs 
                        SET nounou_formule1=:nounou_formule1, nounou_formule2=:nounou_formule2, nounou_formule3=:nounou_formule3, nounou_formule4=:nounou_formule4, 
                            nounou_tarif1=:nounou_tarif1, nounou_tarif2=:nounou_tarif2, nounou_tarif3=:nounou_tarif3, nounou_tarif4=:nounou_tarif4
                                WHERE nounous_tarifs.nounous_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($data7);


header("location:../../HTML/fiche/ficheNounous.php?numNounou=$num");

}

?>
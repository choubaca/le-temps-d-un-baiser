<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");

$num = $_POST['numFleuriste'];

if (isset($_POST['submit'])){

    $date = [
        'fleuriste_nom' => htmlspecialchars ($_POST['nomFleur']),
        'fleuriste_prenom' => htmlspecialchars ($_POST['prenomFleur']),
        'fleuriste_telephone' => htmlspecialchars ($_POST['telFleur']),
        'fleuriste_mail' => htmlspecialchars ($_POST['mailFleur']),
        'fleuriste_adresse' => htmlspecialchars ($_POST['adresseFleur']),
        'fleuriste_saison' => htmlspecialchars ($_POST['fleurSaison']),
        'fleuriste_choix' => htmlspecialchars ($_POST['choixFleur']),
        'fleuriste_test_floral' => htmlspecialchars ($_POST['testFloral']),
        'fleuriste_location' => htmlspecialchars ($_POST['locationFleur']),
        'fleuriste_commande' => htmlspecialchars ($_POST['commande']),
        'fleuriste_rdv' => htmlspecialchars ($_POST['rdvFleur']),
        'fleuriste_autre_elements_decoration' => htmlspecialchars ($_POST['autreLocationFleur'])
        ];

        $req="UPDATE fleuristes 
                    SET fleuriste_nom=:fleuriste_nom, fleuriste_prenom=:fleuriste_prenom, fleuriste_telephone=:fleuriste_telephone, fleuriste_mail=:fleuriste_mail, 
                        fleuriste_adresse=:fleuriste_adresse, fleuriste_saison=:fleuriste_saison, fleuriste_choix=:fleuriste_choix,fleuriste_test_floral =:fleuriste_test_floral, 
                        fleuriste_location=:fleuriste_location, fleuriste_commande=:fleuriste_commande, fleuriste_rdv=:fleuriste_rdv, fleuriste_autre_elements_decoration=:fleuriste_autre_elements_decoration
                         WHERE fleuristes.id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($date);


    $data2 = [
        'livraison_validation' => htmlspecialchars ($_POST['livraison']),
        'livraison_supplement' => htmlspecialchars ($_POST['supplementLivrai']),
        'prix_au_km' => htmlspecialchars ($_POST['PrixAuxKM']),
        'livraison_zone' => htmlspecialchars ($_POST['zoneLivrai'])
        ];

        $req="UPDATE fleuristes_livraisons 
                        SET livraison_validation=:livraison_validation, livraison_supplement=:livraison_supplement, prix_au_km=:prix_au_km, livraison_zone=:livraison_zone
                         WHERE fleuristes_livraisons.fleuriste_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($data2);


    $date3 = [
        'decoration_plafond' => htmlspecialchars ($_POST['decoPlafondFleur']),
        'decoration_voiture' => htmlspecialchars ($_POST['decoVoitureFleur']),
        'carterie' => htmlspecialchars ($_POST['carterieFleur']),
        'nappage' => htmlspecialchars ($_POST['nappageFleur']),
        'chemin' => htmlspecialchars ($_POST['cheminFleur']),
        'bougies' => htmlspecialchars ($_POST['bougiesFleur'])
        ];

        $req="UPDATE fleuristes_decorations 
                        SET decoration_plafond=:decoration_plafond, decoration_voiture=:decoration_voiture, carterie=:carterie, nappage=:nappage, chemin=:chemin, bougies=:bougies
                         WHERE fleuristes_decorations.fleuriste_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($date3);


    $date4 = [
        'desinstallation_validation' => htmlspecialchars ($_POST['desinstal']),
        'desinstallation_supplement' => htmlspecialchars ($_POST['prixDesinstal'])
        ];

        $req="UPDATE fleuristes_desinstallations 
                        SET desinstallation_validation=:desinstallation_validation, desinstallation_supplement=:desinstallation_supplement
                         WHERE fleuristes_desinstallations.fleuriste_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($date4);


    $date5 = [
        'installation_validation' => htmlspecialchars ($_POST['instal']),
        'installation_supplement' => htmlspecialchars ($_POST['prixInstal']),
        ];

        $req="UPDATE fleuristes_installations 
                        SET installation_validation=:installation_validation, installation_supplement=:installation_supplement
                         WHERE fleuristes_installations.fleuriste_id = '$num'";
        $requete = $connexion->prepare($req);
        $requete->execute($date5);



// on redirige vers la liste des fleuristes

header("location:../../HTML/fiche/ficheFleuristes.php?numFleuriste=$num");

}

?>
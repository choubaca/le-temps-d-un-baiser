<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");

$num = $_POST['numVideaste'];

if (isset($_POST['submit'])){

        $data = [
                'videaste_nom' => htmlspecialchars ($_POST['nomVideo']),
                'videaste_prenom' => htmlspecialchars ($_POST['prenomVideo']),
                'videaste_telephone' => htmlspecialchars ($_POST['telVideo']),
                'videaste_mail' => htmlspecialchars ($_POST['mailVideo']),
                'nombre_film' => htmlspecialchars ($_POST['combienFilm']),
                'duree_film' => htmlspecialchars ($_POST['dureeFilm']),
                'livraison_film' => htmlspecialchars ($_POST['livraisonFilm']),
                'heure_fin_video' => htmlspecialchars ($_POST['heureFilm']),
                'nombre_cameramen' => htmlspecialchars ($_POST['cameramen']),
                'visionnage_film' => htmlspecialchars ($_POST['visioFilm']),
                'effet_montage' => htmlspecialchars ($_POST['effetMontage']),
                'videaste_remarque' => htmlspecialchars ($_POST['remarque'])
                ];

                $req="UPDATE videastes 
                                SET videaste_nom=:videaste_nom, videaste_prenom=:videaste_prenom, videaste_telephone=:videaste_telephone, videaste_mail=:videaste_mail, 
                                nombre_film=:nombre_film, duree_film=:duree_film, livraison_film=:livraison_film, heure_fin_video=:heure_fin_video, nombre_cameramen=:nombre_cameramen, 
                                visionnage_film=:visionnage_film, effet_montage=:effet_montage, videaste_remarque=:videaste_remarque
                                        WHERE videastes.id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data);


        $data2 = [
                'deplacement_validation' => htmlspecialchars ($_POST['deplacementVideo']),
                'deplacement_supplement' => htmlspecialchars ($_POST['supplementVideo']),
                'prix_au_km' => htmlspecialchars ($_POST['prixKMVideo']),
                'deplacement_zone' => htmlspecialchars ($_POST['zone'])
                ];
            
                $req="UPDATE deplacements 
                                SET deplacement_validation=:deplacement_validation, deplacement_supplement=:deplacement_supplement, prix_au_km=:prix_au_km, deplacement_zone=:deplacement_zone
                                        WHERE deplacements.videaste_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data2);



        $data3 = [
                'videaste_dvd' => htmlspecialchars ($_POST['supportDVD']),
                'videaste_usb' => htmlspecialchars ($_POST['supportUSB'])
                ];
            
                $req="UPDATE videastes_supports 
                                SET videaste_dvd=:videaste_dvd, videaste_usb=:videaste_usb
                                        WHERE videastes_supports.videaste_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data3);


        $data4 = [
                'betisier' => htmlspecialchars ($_POST['betisier']),
                'insertion_photo' => htmlspecialchars ($_POST['inserePhoto']),
                'generique' => htmlspecialchars ($_POST['generique']),
                'interview' => htmlspecialchars ($_POST['interview'])
                ];
        
                $req="UPDATE videastes_options 
                                SET betisier=:betisier, insertion_photo=:insertion_photo, generique=:generique, interview=:interview
                                        WHERE videastes_options.videaste_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data4);


        $data5 = [
                'videaste_tarif1' => htmlspecialchars ($_POST['tarif1']),
                'videaste_tarif2' => htmlspecialchars ($_POST['tarif2']),
                'videaste_tarif3' => htmlspecialchars ($_POST['tarif3']),
                'videaste_tarif4' => htmlspecialchars ($_POST['tarif4']),
                'videaste_tarif5' => htmlspecialchars ($_POST['tarif5']),
                'videaste_tarif6' => htmlspecialchars ($_POST['tarif6']),
                'videaste_tarif7' => htmlspecialchars ($_POST['tarif7']),
                'videaste_formule_horaire' => htmlspecialchars ($_POST['autreFormuleHoraire']),
                'videaste_formule_moment' => htmlspecialchars ($_POST['autreFormuleMoment']),
                ];
                
                $req="UPDATE videastes_tarifs 
                                SET videaste_tarif1=:videaste_tarif1, videaste_tarif2=:videaste_tarif2, videaste_tarif3=:videaste_tarif3, videaste_tarif4=:videaste_tarif4, 
                                videaste_tarif5=:videaste_tarif5, videaste_tarif6=:videaste_tarif6, videaste_tarif7=:videaste_tarif7, videaste_formule_horaire=:videaste_formule_horaire, 
                                videaste_formule_moment=:videaste_formule_moment
                                        WHERE videastes_tarifs.videaste_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data5);

header("location:../../HTML/fiche/ficheVideastes.php?numVideaste=$num");

}

?>
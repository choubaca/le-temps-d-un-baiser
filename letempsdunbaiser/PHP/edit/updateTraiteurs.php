<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");

$num = $_POST['numTraiteur'];

if (isset($_POST['submit'])){

        $data = [
                'traiteur_nom' => htmlspecialchars ($_POST['nomTraiteur']),
                'traiteur_prenom' => htmlspecialchars ($_POST['prenomTraiteur']),
                'traiteur_telephone' => htmlspecialchars ($_POST['telTraiteur']),
                'traiteur_mail' => htmlspecialchars ($_POST['mailTraiteur']),
                'traiteur_adresse' => htmlspecialchars ($_POST['adresseTraiteur']),
                'traiteur_style_cuisine' => htmlspecialchars ($_POST['styleCuisine']),
                'service' => htmlspecialchars ($_POST['service']),
                'nombre_minimum_invite' => htmlspecialchars ($_POST['nbrMinInvite']),
                'nombre_maximum_invite' => htmlspecialchars ($_POST['nbrMaxInvite']),
                'nombre_serveur' => htmlspecialchars ($_POST['nbrServeurs']),
                'apporte_materiel' => htmlspecialchars ($_POST['apportMateriel']),
                'visite_technique' => htmlspecialchars ($_POST['visiteTech']),
                'commande_avant_mariage' => htmlspecialchars ($_POST['tempsAvantMariage']),
                'arriver_avant_vin_dhonneur' => htmlspecialchars ($_POST['delaiVinDhonnuer']),
                'delai_entre_plat' => htmlspecialchars ($_POST['delaiPlat']),
                'heure_limite_traiteur' => htmlspecialchars ($_POST['heureLimite']),
                'tarif_heure_supplementaire' => htmlspecialchars ($_POST['tarifHeureSup']),
                'deduction' => htmlspecialchars ($_POST['deduction'])
                ];
        
                $req = "UPDATE traiteurs 
                                SET traiteur_nom=:traiteur_nom, traiteur_prenom=:traiteur_prenom, traiteur_telephone=:traiteur_telephone, traiteur_mail=:traiteur_mail, 
                                        traiteur_adresse=:traiteur_adresse, traiteur_style_cuisine=:traiteur_style_cuisine, service=:service, nombre_minimum_invite=:nombre_minimum_invite, 
                                        nombre_maximum_invite=:nombre_maximum_invite, nombre_serveur=:nombre_serveur, apporte_materiel=:apporte_materiel, visite_technique=:visite_technique, 
                                        commande_avant_mariage=:commande_avant_mariage, arriver_avant_vin_dhonneur=:arriver_avant_vin_dhonneur, delai_entre_plat=:delai_entre_plat, 
                                        heure_limite_traiteur=:heure_limite_traiteur, tarif_heure_supplementaire=:tarif_heure_supplementaire, deduction=:deduction
                                                WHERE traiteurs.id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data);


        $data2 = [
                'bouteille_matiere' => htmlspecialchars ($_POST['matiereBouteille']),
                'bouteille_supplement' => htmlspecialchars ($_POST['supplementBouteille']),
                'tarif_supplement' => htmlspecialchars ($_POST['combienSup'])
                ];
        
                $req = "UPDATE traiteurs_bouteilles  
                                SET bouteille_matiere=:bouteille_matiere, bouteille_supplement=:bouteille_supplement, tarif_supplement=:tarif_supplement
                                        WHERE traiteurs_bouteilles.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data2);


        $data3 = [
                'traiteur_cocktail_eaux' => htmlspecialchars ($_POST['eauxCocktail']),
                'traiteur_cocktail_softs' => htmlspecialchars ($_POST['softsCocktail']),
                'traiteur_cocktail_alcools' => htmlspecialchars ($_POST['alcoolsCocktail']),
                'traiteur_cocktail_champagne' => htmlspecialchars ($_POST['champagneCocktail']),
                'traiteur_cocktail' => htmlspecialchars ($_POST['cocktails'])
                ];
        
                $req = "UPDATE traiteurs_cocktails 
                                SET traiteur_cocktail_eaux=:traiteur_cocktail_eaux, traiteur_cocktail_softs=:traiteur_cocktail_softs, traiteur_cocktail_alcools=:traiteur_cocktail_alcools, 
                                        traiteur_cocktail_champagne=:traiteur_cocktail_champagne, traiteur_cocktail=:traiteur_cocktail
                                                WHERE traiteurs_cocktails.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data3);


        $data4 = [
                'halal' => htmlspecialchars ($_POST['halal']),
                'cacher' => htmlspecialchars ($_POST['cacher']),
                'vegetarien' => htmlspecialchars ($_POST['vegetarien']),
                'sansgluten' => htmlspecialchars ($_POST['sansgluten']),
                'vegan' => htmlspecialchars ($_POST['vegan']),
                'sanslactose' => htmlspecialchars ($_POST['sanslactose'])
                ];
        
                $req = "UPDATE traiteurs_cuisines 
                                SET halal=:halal, cacher=:cacher, vegetarien=:vegetarien, sansgluten=:sansgluten, vegan=:vegan, sanslactose=:sanslactose
                                        WHERE traiteurs_cuisines.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data4);


        $data5 = [
                'traiteur_diner_eaux' => htmlspecialchars ($_POST['eauxDiner']),
                'traiteur_diner_softs' => htmlspecialchars ($_POST['softsDiner']),
                'traiteur_diner_vins' => htmlspecialchars ($_POST['vinDiner']),
                'traiteur_diner_champagne' => htmlspecialchars ($_POST['ChampagneDiner'])
                ];
        
                $req = "UPDATE traiteurs_diners 
                                SET traiteur_diner_eaux=:traiteur_diner_eaux, traiteur_diner_softs=:traiteur_diner_softs, traiteur_diner_vins=:traiteur_diner_vins,
                                        traiteur_diner_champagne=:traiteur_diner_champagne
                                                WHERE traiteurs_diners.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data5);
        

        $data6 = [
                'fait_maison' => htmlspecialchars ($_POST['faitMaison']),
                'autre_fait_maison' => htmlspecialchars ($_POST['autreFaitMaison']),
                'produit_frais' => htmlspecialchars ($_POST['produitFrais']),
                'produit_de_saison' => htmlspecialchars ($_POST['produitSaison']),
                'fabrication_dessert' => htmlspecialchars ($_POST['dessertFait'])
                ];
        
                $req = "UPDATE traiteurs_produits 
                                SET fait_maison=:fait_maison, autre_fait_maison=:autre_fait_maison, produit_frais=:produit_frais, produit_de_saison=:produit_de_saison, 
                                        fabrication_dessert=:fabrication_dessert
                                                WHERE traiteurs_produits.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data6);


        $data7 = [
                'traiteur_soiree_eaux' => htmlspecialchars ($_POST['eauxSoiree']),
                'traiteur_soiree_softs' => htmlspecialchars ($_POST['softsSoiree']),
                'traiteur_soiree_alcools' => htmlspecialchars ($_POST['alcoolSoiree'])
                ];
        
                $req = "UPDATE traiteurs_soirees 
                                SET traiteur_soiree_eaux=:traiteur_soiree_eaux, traiteur_soiree_softs=:traiteur_soiree_softs, traiteur_soiree_alcools=:traiteur_soiree_alcools
                                        WHERE traiteurs_soirees.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data7);


        $data8 = [
                'buffet' => htmlspecialchars ($_POST['buffet']),
                'animation_culinaire' => htmlspecialchars ($_POST['animCuli']),
                'service_a_table' => htmlspecialchars ($_POST['serviceTable']),
                'brunch' => htmlspecialchars ($_POST['brunch'])
                ];
        
                $req = "UPDATE traiteurs_types_prestations 
                                SET buffet=:buffet, animation_culinaire=:animation_culinaire, service_a_table=:service_a_table, brunch=:brunch
                                        WHERE traiteurs_types_prestations.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data8);


        $data9 = [
                'decoration_buffet' => htmlspecialchars ($_POST['decoBuffet']),
                'couleur_chemin_table' => htmlspecialchars ($_POST['couleurChemin']),
                'nappage' => htmlspecialchars ($_POST['nappage']),
                'recuperation_nappage' => htmlspecialchars ($_POST['recupnappe']),
                'vaisselle' => htmlspecialchars ($_POST['vaisselle']),
                'vaisselle_laissee' => htmlspecialchars ($_POST['vaisselleSurPlace']),
                'vaisselle_retour' => htmlspecialchars ($_POST['retourVaisselle'])
                ];
        
                $req = "UPDATE traiteurs_vaisselles 
                                SET decoration_buffet=:decoration_buffet, couleur_chemin_table=:couleur_chemin_table, nappage=:nappage, recuperation_nappage=:recuperation_nappage, 
                                        vaisselle=:vaisselle, vaisselle_laissee=:vaisselle_laissee, vaisselle_retour=:vaisselle_retour
                                                WHERE traiteurs_vaisselles.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data9);


        $data10 = [
                'validation_degustation' => htmlspecialchars ($_POST['faireDegustation']),
                'lieu_degustation' => htmlspecialchars ($_POST['degustation']),
                'nombre_personne_max' => htmlspecialchars ($_POST['persMaxTest']),
                'prix_par_personne' => htmlspecialchars ($_POST['prixParPers']),
                'test_cocktail' => htmlspecialchars ($_POST['cocktailTest']),
                'combien_cocktail_test' => htmlspecialchars ($_POST['combienCocktailTest']),
                'test_entree' => htmlspecialchars ($_POST['entreeTest']),
                'combien_entree_test' => htmlspecialchars ($_POST['combienEntreeTest']),
                'test_plat' => htmlspecialchars ($_POST['platTest']),
                'combien_plat_test' => htmlspecialchars ($_POST['combienPlatTest']),
                'test_dessert' => htmlspecialchars ($_POST['dessertTest']),
                'combien_dessert_test' => htmlspecialchars ($_POST['combienDessertTest'])
                ];
        
                $req = "UPDATE traiteur_degustations 
                                SET validation_degustation=:validation_degustation, lieu_degustation=:lieu_degustation, nombre_personne_max=:nombre_personne_max, 
                                        prix_par_personne=:prix_par_personne, test_cocktail=:test_cocktail, combien_cocktail_test=:combien_cocktail_test, test_entree=:test_entree, 
                                        combien_entree_test=:combien_entree_test, test_plat=:test_plat, combien_plat_test=:combien_plat_test, test_dessert=:test_dessert, 
                                        combien_dessert_test=:combien_dessert_test
                                                WHERE traiteur_degustations.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data10);


        $data11 = [
                'traiteur_piece_montee_choux' => htmlspecialchars ($_POST['pieceMonteeChoux']),
                'traiteur_piece_montee_macarons' => htmlspecialchars ($_POST['pieceMonteeMacarons']),
                'traiteur_wedding_cake' => htmlspecialchars ($_POST['weddingCake']),
                'traiteur_autre_dessert' => htmlspecialchars ($_POST['autreDessert'])
                ];
        
                $req = "UPDATE traiteur_desserts 
                                SET traiteur_piece_montee_choux=:traiteur_piece_montee_choux, traiteur_piece_montee_macarons=:traiteur_piece_montee_macarons, 
                                        traiteur_wedding_cake=:traiteur_wedding_cake, traiteur_autre_dessert=:traiteur_autre_dessert
                                                WHERE traiteur_desserts.traiteur_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data11);


        
header("location:../../HTML/fiche/ficheTraiteurs.php?numTraiteur=$num");

}

?>
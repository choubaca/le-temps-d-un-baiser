<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");


$marie = $_POST['numMarie'];

if (isset($_POST['submit'])){


        $data = [
                'date_mariage' => htmlspecialchars ($_POST['dateDuMariage']),
                'nombre_adulte' => htmlspecialchars ($_POST['nbrAdulte']),
                'nombre_enfant' => htmlspecialchars ($_POST['nbrEnfant']),
                'lieu_ceremonie' => htmlspecialchars ($_POST['lieuCeremonie']),
                'lieu_mairie' => htmlspecialchars ($_POST['mairie']),
                'lieu_ceremonie_religieuse' => htmlspecialchars ($_POST['Ceremoniereligieuse']),
                'lieu_ceremonie_laique' => htmlspecialchars ($_POST['Ceremonielaïque']),
                'budget_reception' => htmlspecialchars ($_POST['budgetReception']),
                'event_ceremonie' => htmlspecialchars ($_POST['eventCeremonie']),
                'event_reception' => htmlspecialchars ($_POST['eventReception']),
                'type_de_lieu' => htmlspecialchars ($_POST['typeDeLieu']),
                'autre_type_de_lieu' => htmlspecialchars ($_POST['autreTypeDeLieu']),
                'localisation_geographique' => htmlspecialchars ($_POST['localisationGeo']),
                'autre' => htmlspecialchars ($_POST['autre'])
                ];
        
                $req = "UPDATE organisations 
                            SET date_mariage=:date_mariage, nombre_adulte=:nombre_adulte, nombre_enfant=:nombre_enfant, lieu_ceremonie=:lieu_ceremonie, lieu_mairie=:lieu_mairie, 
                                lieu_ceremonie_religieuse=:lieu_ceremonie_religieuse, lieu_ceremonie_laique=:lieu_ceremonie_laique, budget_reception=:budget_reception, 
                                event_ceremonie=:event_ceremonie, event_reception=:event_reception, type_de_lieu=:type_de_lieu, autre_type_de_lieu=:autre_type_de_lieu, 
                                localisation_geographique=:localisation_geographique, autre=:autre";
                $requete = $connexion->prepare($req);
                $requete->execute($data);


        $data2 = [
                'espace_exterieur' => htmlspecialchars ($_POST['espaceExt']),
                'piscine' => htmlspecialchars ($_POST['piscine']),
                'chambre_maries' => htmlspecialchars ($_POST['chambreMaries'])
                ];
        
                $req = "UPDATE organisations_criteres 
                            SET espace_exterieur=:espace_exterieur, piscine=:piscine, chambre_maries=:chambre_maries";
                $requete = $connexion->prepare($req);
                $requete->execute($data2);


        $data3 = [
                'hebergement_sur_place' => htmlspecialchars ($_POST['hebergementSurPlace']),
                'nombre_hebergement' => htmlspecialchars ($_POST['combienHebergement']),
                'autre_hebergement' => htmlspecialchars ($_POST['autreHebergement'])
                ];
        
                $req = "UPDATE organisations_hebergements 
                            SET hebergement_sur_place=:hebergement_sur_place, nombre_hebergement=:nombre_hebergement, autre_hebergement=:autre_hebergement";
                $requete = $connexion->prepare($req);
                $requete->execute($data3);


        $data4 = [
                'vin_dhonneur' => htmlspecialchars ($_POST['convivesVinDhonneur']),
                'brunch' => htmlspecialchars ($_POST['convivesAuDiner']),
                'diner' => htmlspecialchars ($_POST['convivesAuBrunch'])
                ];
        
                $req = "UPDATE organisations_convives 
                            SET vin_dhonneur=:vin_dhonneur, brunch=:brunch, diner=:diner";
                $requete = $connexion->prepare($req);
                $requete->execute($data4);


        $data5 = [
                'mise_en_bouche' => htmlspecialchars ($_POST['miseEnBouche']),
                'viande' => htmlspecialchars ($_POST['viande']),
                'entree' => htmlspecialchars ($_POST['entree']),
                'fromage' => htmlspecialchars ($_POST['fromage']),
                'poisson' => htmlspecialchars ($_POST['poisson']),
                'dessert' => htmlspecialchars ($_POST['dessert']),
                'nombre_de_piece' => htmlspecialchars ($_POST['nbrPieceCocktail']),
                'brunch_lendemain' => htmlspecialchars ($_POST['brunchLendemain']),
                'type_de_diner' => htmlspecialchars ($_POST['TypeDeDiner']),
                'animation_culinaire' => htmlspecialchars ($_POST['animationCulinaire'])
                ];
        
                $req = "UPDATE organisations_deroules
                            SET mise_en_bouche=:mise_en_bouche, viande=:viande, entree=:entree, fromage=:fromage, poisson=:poisson, dessert=:dessert, nombre_de_piece=:nombre_de_piece, 
                                brunch_lendemain=:brunch_lendemain, type_de_diner=:type_de_diner, animation_culinaire=:animation_culinaire";
                $requete = $connexion->prepare($req);
                $requete->execute($data5);
        

        $data6 = [
                'organisation_dessert_piece_montee_choux' => htmlspecialchars ($_POST['pieceMonteeChoux']),
                'organisation_dessert_piece_montee_macarons' => htmlspecialchars ($_POST['pieceMonteeMacaron']),
                'organisation_dessert_wedding_cake' => htmlspecialchars ($_POST['weddingCake']),
                'organisation_dessert_autre_dessert' => htmlspecialchars ($_POST['autreDessert'])
                ];
        
                $req = "UPDATE organisations_desserts 
                            SET organisation_dessert_piece_montee_choux=:organisation_dessert_piece_montee_choux, organisation_dessert_piece_montee_macarons=:organisation_dessert_piece_montee_macarons, 
                                organisation_dessert_wedding_cake=:organisation_dessert_wedding_cake, organisation_dessert_autre_dessert=:organisation_dessert_autre_dessert";
                $requete = $connexion->prepare($req);
                $requete->execute($data6);


        $data7 = [
                'organisation_vin_dhonneur_eau' => htmlspecialchars ($_POST['vinDhonneurEau']),
                'organisation_vin_dhonneur_soft' => htmlspecialchars ($_POST['vinDhonneurSofts']),
                'organisation_vin_dhonneur_cocktail' => htmlspecialchars ($_POST['vinDhonneurCocktail']),
                'organisation_vin_dhonneur_alcool' => htmlspecialchars ($_POST['vinDhonneurAlcools']),
                'organisation_vin_dhonneur_champagne' => htmlspecialchars ($_POST['vinDhonneurChampagne'])
                ];
        
                $req = "UPDATE organisations_vins_dhonneur 
                            SET organisation_vin_dhonneur_eau=:organisation_vin_dhonneur_eau, organisation_vin_dhonneur_soft=:organisation_vin_dhonneur_soft, 
                                organisation_vin_dhonneur_cocktail=:organisation_vin_dhonneur_cocktail, organisation_vin_dhonneur_alcool=:organisation_vin_dhonneur_alcool, 
                                organisation_vin_dhonneur_champagne=:organisation_vin_dhonneur_champagne";
                $requete = $connexion->prepare($req);
                $requete->execute($data7);


        $data8 = [
                'organisation_diner_eau' => htmlspecialchars ($_POST['dinerEau']),
                'organisation_diner_softs' => htmlspecialchars ($_POST['dinerSofts']),
                'organisation_diner_vin' => htmlspecialchars ($_POST['dinerVin']),
                'organisation_diner_alcools' => htmlspecialchars ($_POST['dinerAlcools'])
                ];
        
                $req = "UPDATE organisations_diners 
                            SET organisation_diner_eau=:organisation_diner_eau, organisation_diner_softs=:organisation_diner_softs, organisation_diner_vin=:organisation_diner_vin, 
                                organisation_diner_alcools=:organisation_diner_alcools";
                $requete = $connexion->prepare($req);
                $requete->execute($data8);


        $data9 = [
                'organisation_soiree_eau' => htmlspecialchars ($_POST['soireeEau']),
                'organisation_soiree_softs' => htmlspecialchars ($_POST['soireeSofts']),
                'organisation_soiree_alcool' => htmlspecialchars ($_POST['soireeAlcools']),
                'organisation_soiree_champagne' => htmlspecialchars ($_POST['soireeChampagne'])
                ];
        
                $req = "UPDATE organisations_soirees 
                            SET organisation_soiree_eau=:organisation_soiree_eau, organisation_soiree_softs=:organisation_soiree_softs, organisation_soiree_alcool=:organisation_soiree_alcool, 
                                organisation_soiree_champagne=:organisation_soiree_champagne";
                $requete = $connexion->prepare($req);
                $requete->execute($data9);


        $data10 = [
                'dj' => htmlspecialchars ($_POST['DJ']),
                'gospel' => htmlspecialchars ($_POST['Gospel']),
                'orchestre' => htmlspecialchars ($_POST['Orchestre']),
                'chant' => htmlspecialchars ($_POST['Chant']),
                'style' => htmlspecialchars ($_POST['styleAnimationMusical'])
                ];
        
                $req = "UPDATE organisations_animations_musicales 
                            SET dj=:dj, gospel=:gospel, orchestre=:orchestre, chant=:chant, style=:style";
                $requete = $connexion->prepare($req);
                $requete->execute($data10);


        $data11 = [
                'organisation_photographe_formule' => htmlspecialchars ($_POST['formulePhotographe']),
                'organisation_photographe_style' => htmlspecialchars ($_POST['stylePhotographe']),
                'organisation_photographe_support' => htmlspecialchars ($_POST['supportPhotographe']),
                ];
                
                $req = "UPDATE organisations_photographes 
                            SET organisation_photographe_formule=:organisation_photographe_formule, organisation_photographe_style=:organisation_photographe_style, 
                                organisation_photographe_support=:organisation_photographe_support";
                $requete = $connexion->prepare($req);
                $requete->execute($data11);


        $data12 = [
                'organisation_video_formule' => htmlspecialchars ($_POST['formuleVideo']),
                'organisation_video_style' => htmlspecialchars ($_POST['styleVideo']),
                'organisation_video_format' => htmlspecialchars ($_POST['formatVideo']),
                'organisation_video_support' => htmlspecialchars ($_POST['supportVideo'])
                ];
        
                $req = "UPDATE organisations_videos 
                            SET organisation_video_formule=:organisation_video_formule, organisation_video_style=:organisation_video_style, 
                                organisation_video_format=:organisation_video_format, organisation_video_support=:organisation_video_support";
                $requete = $connexion->prepare($req);
                $requete->execute($data12);


        $data13 = [
                'nombre_d_enfant' => htmlspecialchars ($_POST['nbrEnfantsBS']),
                'horaire' => htmlspecialchars ($_POST['horaireBS'])
                ];
        
                $req = "UPDATE organisations_baby_sitters 
                                SET nombre_d_enfant=:nombre_d_enfant, horaire=:horaire";
                $requete = $connexion->prepare($req);
                $requete->execute($data13);


        $data14 = [
                'coiffure' => htmlspecialchars ($_POST['coiffure']),
                'maquillage' => htmlspecialchars ($_POST['maquillage']),
                'officiant' => htmlspecialchars ($_POST['officiant']),
                'location_voiture' => htmlspecialchars ($_POST['locationVoiture'])
                ];
        
                $req = "UPDATE organisations_autres_prestataires 
                                SET coiffure=:coiffure, maquillage=:maquillage, officiant=:officiant, location_voiture=:location_voiture";
                $requete = $connexion->prepare($req);
                $requete->execute($data14);


                
header("location:../../HTML/edit/editPrestataires.php?numMarie=".$marie."");

}

?>
<?php

// on fait la connexion avec la bdd

include("../connexion/connexion.php");

$num = $_POST['numLieu'];

if (isset($_POST['submit'])){

        $data = [
                'lieu_nom' => htmlspecialchars ($_POST['nomLieu']),
                'lieu_type' => htmlspecialchars ($_POST['typeLieu']),
                'lieu_ville' => htmlspecialchars ($_POST['ville']),
                'lieu_distance' => htmlspecialchars ($_POST['distance']),
                'lieu_plan_B' => htmlspecialchars ($_POST['planB']),
                'lieu_temps_trajet' => htmlspecialchars ($_POST['tempsTrajet']),
                'lieu_salle_occupation_enfant' => htmlspecialchars ($_POST['salleOccupEnfant']),
                'lieu_salle_repos_enfant' => htmlspecialchars ($_POST['SalleDormirEnfant']),
                'lieu_mariage_unique' => htmlspecialchars ($_POST['mariageSolo']),
                'lieu_accompte' => htmlspecialchars ($_POST['accompte']),
                'lieu_travailWP' => htmlspecialchars ($_POST['travailWP']),
                'lieu_remarque' => htmlspecialchars ($_POST['remarque'])
                ];
        
                $req = "UPDATE lieux 
                                SET lieu_nom=:lieu_nom, lieu_type=:lieu_type, lieu_ville=:lieu_ville, lieu_distance=:lieu_distance, lieu_plan_B=:lieu_plan_B, 
                                lieu_temps_trajet=:lieu_temps_trajet, lieu_salle_occupation_enfant=:lieu_salle_occupation_enfant, lieu_salle_repos_enfant=:lieu_salle_repos_enfant, 
                                lieu_mariage_unique=:lieu_mariage_unique, lieu_accompte=:lieu_accompte, lieu_travailWP=:lieu_travailWP, lieu_remarque=:lieu_remarque
                                        WHERE lieux.id = '$num'";
                
                $requete = $connexion->prepare($req);
                $requete->execute($data);


        $data2 = [
                'terrasse' => htmlspecialchars ($_POST['terrasse']),
                'surface_terrasse' => htmlspecialchars ($_POST['terrasseSurf']),
                'jardin' => htmlspecialchars ($_POST['jardin']),
                'surface_jardin' => htmlspecialchars ($_POST['jardinSurf']),
                'parc' => htmlspecialchars ($_POST['parc']),
                'surface_parc' => htmlspecialchars ($_POST['parcSurf']),
                'parking' => htmlspecialchars ($_POST['parking']),
                'place_parking' => htmlspecialchars ($_POST['placeParking']),
                'vestiaire' => htmlspecialchars ($_POST['vestiaire']),
                'toilette_homme' => htmlspecialchars ($_POST['WcHomme']),
                'toilette_femme' => htmlspecialchars ($_POST['WcFemme']),
                'PMR' => htmlspecialchars ($_POST['PMR'])
                ];
        
                $req = "UPDATE lieux_annexes 
                                SET terrasse=:terrasse, surface_terrasse=:surface_terrasse, jardin=:jardin, surface_jardin=:surface_jardin, parc=:parc, surface_parc=:surface_parc, 
                                parking=:parking, place_parking=:place_parking, vestiaire=:vestiaire, toilette_homme=:toilette_homme, toilette_femme=:toilette_femme, PMR=:PMR
                                        WHERE lieux_annexes.lieux_id = '$num'";
                
                $requete = $connexion->prepare($req);
                $requete->execute($data2);


        $data3 = [
                'buffet_nombre' => htmlspecialchars ($_POST['nbrBuffet']),
                'buffet_dimension' => htmlspecialchars ($_POST['dimBuffet']),
                'buffet_nombre_de_personnes' => htmlspecialchars ($_POST['nbrPersBuffet']),
                'buffet_matiere' => htmlspecialchars ($_POST['matiereBuffet'])
                ];
        
                $req = "UPDATE lieux_buffets 
                                SET buffet_nombre=:buffet_nombre, buffet_dimension=:buffet_dimension, buffet_nombre_de_personnes=:buffet_nombre_de_personnes, buffet_matiere=:buffet_matiere
                                        WHERE lieux_buffets.lieux_id = '$num'";
                
                $requete = $connexion->prepare($req);
                $requete->execute($data3);


        $data4 = [
                'chaise_exterieur_type' => htmlspecialchars ($_POST['TypeChaiseExt']),
                'chaise_exterieur_matiere' => htmlspecialchars ($_POST['matiereChaiseExt']),
                'chaise_exterieur_nombre' => htmlspecialchars ($_POST['qtsChaiseExt'])
                ];
        
                $req = "UPDATE lieux_chaises_exterieur 
                                SET chaise_exterieur_type=:chaise_exterieur_type, chaise_exterieur_matiere=:chaise_exterieur_matiere, chaise_exterieur_nombre=:chaise_exterieur_nombre
                                        WHERE lieux_chaises_exterieur.lieux_id = '$num'";
                
                $requete = $connexion->prepare($req);
                $requete->execute($data4);


        $data5 = [
                'chaise_interieur_type' => htmlspecialchars ($_POST['TypeChaiseInt']),
                'chaise_interieur_matiere' => htmlspecialchars ($_POST['matiereChaiseInt']),
                'chaise_interieur_nombre' => htmlspecialchars ($_POST['qtsChaiseInt']),
                'autre_chaise' => htmlspecialchars ($_POST['autreChaise'])
                ];
        
                $req = "UPDATE lieux_chaises_interieur
                                SET chaise_interieur_type=:chaise_interieur_type, chaise_interieur_matiere=:chaise_interieur_matiere, chaise_interieur_nombre=:chaise_interieur_nombre, 
                                autre_chaise=:autre_chaise
                                        WHERE lieux_chaises_interieur.lieux_id = '$num'";
                
                $requete = $connexion->prepare($req);
                $requete->execute($data5);
        

        $data6 = [
                'contact_nom' => htmlspecialchars ($_POST['nomContact']),
                'contact_telephone' => htmlspecialchars ($_POST['telContact']),
                'contact_mail' => htmlspecialchars ($_POST['mailContact'])
                ];
        
                $req = "UPDATE lieux_contacts 
                                SET contact_nom=:contact_nom, contact_telephone=:contact_telephone, contact_mail=:contact_mail
                                        WHERE lieux_contacts.id = '$num'";
                
                $requete = $connexion->prepare($req);                
                $requete->execute($data6);


        $data7 = [
                'retroprojecteur' => htmlspecialchars ($_POST['lieuRetropro']),
                'ecran' => htmlspecialchars ($_POST['lieuEcran']),
                'ampli' => htmlspecialchars ($_POST['lieuAmpli']),
                'enceinte' => htmlspecialchars ($_POST['lieuEnceinte']),
                'prix_mobilier_compris' => htmlspecialchars ($_POST['mobilierCompris']),
                'autre_mobilier' => htmlspecialchars ($_POST['autreMobilier'])
                ];
        
                $req = "UPDATE lieux_equipements
                                SET retroprojecteur=:retroprojecteur, ecran=:ecran, ampli=:ampli, enceinte=:enceinte, prix_mobilier_compris=:prix_mobilier_compris, autre_mobilier=:autre_mobilier
                                        WHERE lieux_equipements.lieux_id = '$num'";
               
                $requete = $connexion->prepare($req);
                $requete->execute($data7);


        $data8 = [
                'hebergement_sur_place' => htmlspecialchars ($_POST['hebergSurPlace']),
                'nombre_de_chambre' => htmlspecialchars ($_POST['combienChambre']),
                'capacite_total' => htmlspecialchars ($_POST['capaTotal']),
                'prix_chambre1' => htmlspecialchars ($_POST['prixChambre1']),
                'prix_chambre2' => htmlspecialchars ($_POST['prixChambre2']),
                'forfait_location' => htmlspecialchars ($_POST['forfaitLocation']),
                'forfait_location_et_hebergement' => htmlspecialchars ($_POST['forfaitLocHerb'])
                ];
        
                $req = "UPDATE lieux_hebergements 
                                SET hebergement_sur_place=:hebergement_sur_place, nombre_de_chambre=:nombre_de_chambre, capacite_total=:capacite_total, 
                                prix_chambre1=:prix_chambre1, prix_chambre2=:prix_chambre2, forfait_location=:forfait_location, forfait_location_et_hebergement=:forfait_location_et_hebergement
                                        WHERE lieux_hebergements.lieux_id = '$num'";
                
                $requete = $connexion->prepare($req);
                $requete->execute($data8);


        $data9 = [
                'salle1_longueur' => htmlspecialchars ($_POST['longueurSalleP']),
                'salle1_largeur' => htmlspecialchars ($_POST['largeurSalleP']),
                'salle1_nombre_max_personne' => htmlspecialchars ($_POST['nbMaxSalleP']),
                'salle1_separation' => htmlspecialchars ($_POST['sepSalleP']),
                'salle1_chauffage' => htmlspecialchars ($_POST['sallePChauffe']),
                'salle1_climatisation' => htmlspecialchars ($_POST['sallePClimati']),
                'salle1_vin_en_exterieur' => htmlspecialchars ($_POST['vinExter'])
                ];
        
                $req = "UPDATE lieux_salles_principales
                                SET salle1_longueur=:salle1_longueur, salle1_largeur=:salle1_largeur, salle1_nombre_max_personne=:salle1_nombre_max_personne, 
                                salle1_separation=:salle1_separation, salle1_chauffage=:salle1_chauffage, salle1_climatisation=:salle1_climatisation, 
                                salle1_vin_en_exterieur=:salle1_vin_en_exterieur
                                        WHERE lieux_salles_principales.lieux_id = '$num'";                
                
                $requete = $connexion->prepare($req);
                $requete->execute($data9);


        $data10 = [
                'salle2_longueur' => htmlspecialchars ($_POST['longueurSalle2']),
                'salle2_largeur' => htmlspecialchars ($_POST['largeurSalle2']),
                'salle2_nombre_max_personne' => htmlspecialchars ($_POST['nbMaxSalle2']),
                'salle2_utilisation' => htmlspecialchars ($_POST['utilSalle2']),
                'salle2_separation' => htmlspecialchars ($_POST['sepSalle2']),
                'salle2_chauffage' => htmlspecialchars ($_POST['salle2Chauffe']),
                'salle2_climatisation' => htmlspecialchars ($_POST['salle2Climati'])
                ];

                $req = "UPDATE lieux_secondes_salles  
                                SET salle2_longueur=:salle2_longueur, salle2_largeur=:salle2_largeur, salle2_nombre_max_personne=:salle2_nombre_max_personne, 
                                salle2_utilisation=:salle2_utilisation, salle2_separation=:salle2_separation, salle2_chauffage=:salle2_chauffage, salle2_climatisation=:salle2_climatisation
                                        WHERE lieux_secondes_salles.lieux_id = '$num'";                
                
                $requete = $connexion->prepare($req);
                $requete->execute($data10);


        $data11 = [
                'salle3_longueur' => htmlspecialchars ($_POST['longueurSalle3']),
                'salle3_largeur' => htmlspecialchars ($_POST['largeurSalle3']),
                'salle3_nombre_max_personne' => htmlspecialchars ($_POST['nbMaxSalle3']),
                'salle3_utilisation' => htmlspecialchars ($_POST['utilSalle3']),
                'salle3_separation' => htmlspecialchars ($_POST['sepSalle3']),
                'salle3_chauffage' => htmlspecialchars ($_POST['salle3Chauffe']),
                'salle3_climatisation' => htmlspecialchars ($_POST['salle3Climati'])
                ];
        
                $req = "UPDATE lieux_troisiemes_salles 
                                SET salle3_longueur=:salle3_longueur, salle3_largeur=:salle3_largeur, salle3_nombre_max_personne=:salle3_nombre_max_personne, 
                                salle3_utilisation=:salle3_utilisation, salle3_separation=:salle3_separation, salle3_chauffage=:salle3_chauffage, salle3_climatisation=:salle3_climatisation
                                        WHERE lieux_troisiemes_salles.lieux_id = '$num'";                
                
                $requete = $connexion->prepare($req);
                $requete->execute($data11);


        $data12 = [
                'table_rectanculaire_nombre' => htmlspecialchars ($_POST['nbrTableRect']),
                'table_rectanculaire_dimension' => htmlspecialchars ($_POST['dimTableRect']),
                'table_rectanculaire_nombre_de_personnes' => htmlspecialchars ($_POST['nbrPersTableRect']),
                'table_rectanculaire_matiere' => htmlspecialchars ($_POST['matiereTableRect'])
                ];
        
                $req = "UPDATE lieux_tables_rectanculaires 
                                SET table_rectanculaire_nombre=:table_rectanculaire_nombre, table_rectanculaire_dimension=:table_rectanculaire_dimension, 
                                table_rectanculaire_nombre_de_personnes=:table_rectanculaire_nombre_de_personnes, table_rectanculaire_matiere=:table_rectanculaire_matiere
                                        WHERE lieux_tables_rectanculaires.lieux_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data12);


        $data13 = [
                'table_ronde_nombre' => htmlspecialchars ($_POST['nbrTableRonde']),
                'table_ronde_dimension' => htmlspecialchars ($_POST['dimTableRonde']),
                'table_ronde_nombre_de_personnes' => htmlspecialchars ($_POST['nbrPersTableRonde']),
                'table_ronde_matiere' => htmlspecialchars ($_POST['matiereTableRonde']),
                'autre_table' => htmlspecialchars ($_POST['autreTable'])
                ];

                $req = "UPDATE lieux_tables_rondes 
                                SET table_ronde_nombre=:table_ronde_nombre, table_ronde_dimension=:table_ronde_dimension, table_ronde_nombre_de_personnes=:table_ronde_nombre_de_personnes, 
                                table_ronde_matiere=:table_ronde_matiere , autre_table=:autre_table
                                        WHERE lieux_tables_rondes.lieux_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data13);


        $data14 = [
                'date1' => htmlspecialchars ($_POST['date1']),
                'date2' => htmlspecialchars ($_POST['date2']),
                'date3' => htmlspecialchars ($_POST['date3']),
                'date4' => htmlspecialchars ($_POST['date4']),
                'date5' => htmlspecialchars ($_POST['date5']),
                'date6' => htmlspecialchars ($_POST['date6']),
                'heure1' => htmlspecialchars ($_POST['heure1']),
                'heure2' => htmlspecialchars ($_POST['heure2']),
                'heure3' => htmlspecialchars ($_POST['heure3']),
                'heure4' => htmlspecialchars ($_POST['heure4']),
                'heure5' => htmlspecialchars ($_POST['heure5']),
                'heure6' => htmlspecialchars ($_POST['heure6']),
                'tarif_saison_haute1' => htmlspecialchars ($_POST['tarifHaute1']),
                'tarif_moyenne_saison1' => htmlspecialchars ($_POST['tarifMoy1']),
                'tarif_basse_saison1' => htmlspecialchars ($_POST['tarifBas1']),
                'tarif_saison_haute2' => htmlspecialchars ($_POST['tarifHaute2']),
                'tarif_moyenne_saison2' => htmlspecialchars ($_POST['tarifMoy2']),
                'tarif_basse_saison2' => htmlspecialchars ($_POST['tarifBas2']),
                'tarif_saison_haute3' => htmlspecialchars ($_POST['tarifHaute3']),
                'tarif_moyenne_saison3' => htmlspecialchars ($_POST['tarifMoy3']),
                'tarif_basse_saison3' => htmlspecialchars ($_POST['tarifBas3']),
                'nettoyage_inclue' => htmlspecialchars ($_POST['nettoyageInclue']),
                'tarif_nettoyage' => htmlspecialchars ($_POST['tarifNettoie']),
                'restauration_impose' => htmlspecialchars ($_POST['restoImposer']),
                'tarif_restauration' => htmlspecialchars ($_POST['tarifResto'])
                ];
        
                $req = "UPDATE lieux_tarifs 
                                SET date1=:date1, date2=:date2, date3=:date3, date4=:date4, date5=:date5, date6=:date6, heure1=:heure1, heure2=:heure2, heure3=:heure3, 
                                heure4=:heure4, heure5=:heure5, heure6=:heure6, tarif_saison_haute1=:tarif_saison_haute1, tarif_moyenne_saison1=:tarif_moyenne_saison1, 
                                tarif_basse_saison1=:tarif_basse_saison1, tarif_saison_haute2=:tarif_saison_haute2, tarif_moyenne_saison2=:tarif_moyenne_saison2, 
                                tarif_basse_saison2=:tarif_basse_saison2, tarif_saison_haute3=:tarif_saison_haute3, tarif_moyenne_saison3=:tarif_moyenne_saison3, 
                                tarif_basse_saison3=:tarif_basse_saison3, nettoyage_inclue=:nettoyage_inclue, tarif_nettoyage=:tarif_nettoyage, 
                                restauration_impose=:restauration_impose, tarif_restauration=:tarif_restauration
                                        WHERE lieux_tarifs.lieux_id = '$num'";

                $requete = $connexion->prepare($req);
                $requete->execute($data14);


header("location:../../HTML/fiche/ficheLieux.php?numLieu=$num");

}

?>
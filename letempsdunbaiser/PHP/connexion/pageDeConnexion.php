<?php

session_start();
require ('connexionPrincipale.php');
if ($validation == true) header("refresh: 1 ; url=../../HTML/affichage/listeMaries.php");

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../../CSS/screen.css">
        <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
        <title>Connexion</title>
    </head>
    <body>

        <main>
            <form id="monFormulaire" action="pageDeConnexion.php" method="post">

            <legend>Vos Identifiants</legend>

                <fieldset class="identification">
                    
                    <label for="login">Votre Nom
                    <input type="text" name="login" id="login" value="<?php if (isset($_POST['login'])){echo $_POST['login'];} ?>"/></label>

                    <label for="passWord">Votre Mot de Passe
                    <input type="password" name="passWord" id="passWord" value="<?php if (isset($_POST['passWord'])){echo $_POST['passWord'];} ?>"/></label>

                </fieldset>

                <div>
                    <?php 

                        if (!empty($erreur_nomUser) || !empty($erreur_mdp)) echo "$erreur_mdp";
                        
                        if (!empty($valid_log)) echo "$valid_log";
                    ?>
                </div>

                <div id="boutons">
                    <input type="submit" name="submit" class="btn btn-primary" value="Entrée"/>
                </div>
            </form> 
        
        </main>    
    </body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/ficheVideaste_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche du vidéaste</title>
</head>

<body>

    <h1 class="text-center m-5"> <?=$videaste['videaste_nom'];?> <?=$videaste['videaste_prenom'];?> </h1>
    
    <div class="d-flex justify-content-around">

        <table class="tableau_fiche_organisation m-3">

            <tbody>

            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Coordonnées et déplacement </h4>
            </th>
                <tr>
                    <th> Téléphone : </th>
                    <td> <?=$videaste['videaste_telephone'];?> </td>
                </tr>
                <tr>
                    <th> E-mail : </th>
                    <td> <?=$videaste['videaste_mail'];?> </td>
                </tr>
                <tr>
                    <th> Déplacement : </th>
                    <td> <?=$videaste['deplacement_validation'];?> </td>
                </tr>
                <tr>
                    <th> Supplement : </th>
                    <td> <?=$videaste['deplacement_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Prix au KM : </th>
                    <td> <?=$videaste['prix_au_km'];?> </td>
                </tr>
                <tr>
                    <th> Zone de deplacement : </th>
                    <td> <?=$videaste['deplacement_zone'];?> </td>
                </tr>
            </tbody>

        </table>

        <table class="tableau_fiche_organisation m-3">

            <tbody>

            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Vidéos </h4>
            </th>
                <tr>
                    <th class="align-text-top"> Formats remis : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li> <?php if ($videaste['videaste_usb']=='USB') { echo 'USB';}
                                else{ echo '';} ?> </li>
                            <li> <?php if ($videaste['videaste_dvd']=='DVD') { echo 'DVD';}
                                else{ echo '';} ?> </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Combien de films sont remis ? </th>
                    <td> <?=$videaste['nombre_film'];?> </td>
                </tr>
                <tr>
                    <th> Quelle sera leur durée ? </th>
                    <td> <?=$videaste['duree_film'];?> </td>
                </tr>
                <tr>
                    <th> Délai de livraison moyen : </th>
                    <td> <?=$videaste['livraison_film'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_fiche_organisation m-3">

            <tbody>

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Options </h4>
                </th>
                <tr>
                    <th> Un best of / bêtisier ? </th>
                    <td> <?=$videaste['betisier'];?> </td>
                </tr>
                <tr>
                    <th> Possibilité d’insérer des photos ? </th>
                    <td> <?=$videaste['insertion_photo'];?> </td>
                </tr>
                <tr>
                    <th> Générique de fin avec le nom des invités ? </th>
                    <td> <?=$videaste['generique'];?> </td>
                </tr>
                <tr>
                    <th> Interview des invités ? </th>
                    <td> <?=$videaste['interview'];?> </td>
                </tr>

            </tbody>

        </table>

    </div>
    
    <div class="d-flex justify-content-around m-5">

        <table class="tableau_fiche_organisation m-3">

            <tbody>

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Info divers </h4>
                </th>
                <tr>
                    <th> Jusqu’à quelle heure filmez-vous la réception ? </th>
                    <td> <?=$videaste['heure_fin_video'];?> </td>
                </tr>
                <tr>
                    <th> Combien y a-t-il de cameramen ? </th>
                    <td> <?=$videaste['nombre_cameramen'];?> </td>
                </tr>
                <tr>
                    <th> Possibilité de visionner le film avant <br> la fin du montage pour valider ? </th>
                    <td> <?=$videaste['visionnage_film'];?> </td>
                </tr>
                <tr>
                    <th> Effets sur le montage ? </th>
                    <td> <?=$videaste['effet_montage'];?> </td>
                </tr>

            </tbody>

        </table>
        
        <div class="d-flex flex-column">
        
            <h4 class="bg-info text-white text-center">Tarifs</h4>
        
                <div class="d-flex">
    
                    <fieldset class="mr-5">
    
                        <table class="tableau_fiche_organisation m-3">
    
                            <tr>
                                <td class="text-center"> Formules par moment </td>
                                <td class="text-center">Prix</td>
                            </tr>
                            <tr>
                                <td>De la mairie au vin d’honneur</td>
                                <td> <?=$videaste['videaste_tarif1'];?> </td>
                            </tr>
                            <tr>
                                <td>De la mairie à la soirée</td>
                                <td> <?=$videaste['videaste_tarif2'];?> </td>
                            </tr>
                            <tr>
                                <td>Des préparatifs au vin d’honneur</td>
                                <td> <?=$videaste['videaste_tarif3'];?></td>
                            </tr>
                            <tr>
                                <td>Des préparatifs à la soirée </td>
                                <td> <?=$videaste['videaste_tarif4'];?> </td>
                            </tr>
                            <tr>
                                <td> autre formule </td>
                                <td> <?=$videaste['videaste_formule_moment'];?> </td>
                            </tr>
    
                        </table>
    
                    </fieldset>
    
                    <fieldset>
    
                        <table class="tableau_fiche_organisation m-3">
    
                            <tr>
                                <td class="text-center">Formules par horaires</td>
                                <td class="text-center">Prix</td>
                            </tr>
                            <tr>
                                <td>6h de présence</td>
                                <td> <?=$videaste['videaste_tarif5'];?> </td>
                            </tr>
                            <tr>
                                <td>8h de présence</td>
                                <td> <?=$videaste['videaste_tarif6'];?> </td>
                            </tr>
                            <tr>
                                <td>12h de présence </td>
                                <td> <?=$videaste['videaste_tarif7'];?> </td>
                            </tr>
                            <tr>
                                <td> Autre formule </td>
                                <td> <?=$videaste['videaste_formule_horaire'];?> </td>
                            </tr>
    
                        </table>
    
                    </fieldset>
                
                </div>
                
            </div>

        <table class="tableau_fiche_organisation m-3">

            <tbody>

                <tr>
                    <th> Remarques : </th>
                    <td> <?=$videaste['videaste_remarque'];?> </td>
                </tr>

            </tbody>

        </table>
        
    </div>

        <div class="d-flex justify-content-center mt-4">
            <a href="../edit/editVideastes.php?numVideaste=<?=$_GET['numVideaste']?>"><button class="btn btn-primary"> Modification </button></a>
            <a class="ml-3" href="../../PHP/suppression/fichierSupVideastes.php?numVideaste=<?=$_GET['numVideaste']?>"><button class="btn btn-primary"> Suppression </button></a>
        </div>

</body>
</html>
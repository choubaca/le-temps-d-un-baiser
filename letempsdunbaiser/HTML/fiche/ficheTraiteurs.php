<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/ficheTraiteurs_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche du traiteur</title>
</head>

<body>

    <h1 class="text-center m-5"> <?=$Traiteur['traiteur_nom'];?> <?=$Traiteur['traiteur_prenom'];?> </h1>

    <div class="d-flex justify-content-around">

        <table class="tableau_fiche_client">

            <tbody>
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Coordonnées </h4>
            </th>
                <tr>
                    <th> Téléphone : </th>
                    <td> <?=$Traiteur['traiteur_telephone'];?> </td>
                </tr>
                <tr>
                    <th> E-mail : </th>
                    <td> <?=$Traiteur['traiteur_mail'];?> </td>
                </tr>
                <tr>
                    <th> Adresse : </th>
                    <td> <?=$Traiteur['traiteur_adresse'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_fiche_client">

            <tbody>
            
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Logistique </h4>
            </th>
                <tr>
                    <th> Nombre d'invités min : </th>
                    <td> <?=$Traiteur['nombre_minimum_invite'];?> </td>
                </tr>
                <tr>
                    <th> Nombre d'invités max : </th>
                    <td> <?=$Traiteur['nombre_maximum_invite'];?> </td>
                </tr>
                <tr>
                    <th> Nombre de serveurs : </th>
                    <td> <?=$Traiteur['nombre_serveur'];?> </td>
                </tr>
                <tr>
                    <th> À t-il une heure limite : </th>
                    <td> <?=$Traiteur['heure_limite_traiteur'];?> </td>
                </tr>
                <tr>
                    <th> Tarif des heures supplémentaires : </th>
                    <td> <?=$Traiteur['tarif_heure_supplementaire'];?> </td>
                </tr>
            </tbody>

        </table>

        <table class="tableau_fiche_client">

            <tbody>
            
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Info divers </h4>
            </th>
                <tr>
                    <th> Déduction une fois le contrat signé : </th>
                    <td> <?=$Traiteur['deduction'];?> </td>
                </tr>
                <tr>
                    <th> Combien de temps avant le mariage faut-il <br> fixer le choix du menu et le nombre d’invités :
                    </th>
                    <td> <?=$Traiteur['commande_avant_mariage'];?> </td>
                </tr>
                <tr>
                    <th> Combien de temps arrivez-vous <br> avant le vin d'honneur : </th>
                    <td> <?=$Traiteur['arriver_avant_vin_dhonneur'];?> </td>
                </tr>
                <tr>
                    <th> Combien de temps entre les plats : </th>
                    <td> <?=$Traiteur['delai_entre_plat'];?> </td>
                </tr>
            </tbody>

        </table>

    </div>
    
    <div class="d-flex justify-content-around m-5">

        <table class="tableau_fiche_client">

            <tbody>
            
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Produits </h4>
            </th>
                <tr>
                    <th> Comment sont-ils : </th>
                    <td> <?php if ($Traiteur['produit_frais']=='frais') { echo 'Produits frais';}
                                else{ echo 'Produits surgelés';} ?> </td>
                </tr>
                <tr>
                    <th> Sont-ils de saison : </th>
                    <td> <?=$Traiteur['produit_de_saison'];?> </td>
                </tr>
                <tr>
                    <th> Tout est fait maison : </th>
                    <td> <?=$Traiteur['fait_maison'];?> </td>
                </tr>
                <tr>
                    <th> Autre : </th>
                    <td> <?=$Traiteur['autre_fait_maison'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_fiche_client">

            <tbody>
            
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Cuisine </h4>
            </th>
                <tr>
                    <th> Le style de cuisine : </th>
                    <td> <?=$Traiteur['traiteur_style_cuisine'];?> </td>
                </tr>
                <tr>
                    <th class="align-text-top"> Ses contraintes alimentaires : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li> <?php if ($Traiteur['halal']=='halal') { echo 'Halal';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['cacher']=='cacher') { echo 'Cacher';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['vegetarien']=='vegetarien') { echo 'Végétarien';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['sansgluten']=='sansgluten') { echo 'Sans gluten';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['vegan']=='vegan') { echo 'Végan';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['sanslactose']=='sanslactose') { echo 'Sans lactose';}
                            else{ echo '';} ?> </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th class="align-text-top"> Ses prestations : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li> <?php if ($Traiteur['buffet']=='buffet') { echo 'Buffet';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['animation_culinaire']=='animCuli') { echo 'Animations culinaires';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['service_a_table']=='service') { echo 'Service à table';}
                            else{ echo '';} ?> </li>
                            <li> <?php if ($Traiteur['brunch']=='brunch') { echo 'Brunch';}
                            else{ echo '';} ?> </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Comment est fait le service ? </th>
                    <td><?php if ($Traiteur['service']=='verre') { echo 'Aux verres';}
                                 else {echo 'Bouteilles à table '; }?> </td>
                </tr>
            </tbody>

        </table>

        <table class="tableau_fiche_client">

            <tbody>
            
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Desserts </h4>
            </th>
                <tr>
                    <th> Type de desserts : </th>
                    <td><?php if ($Traiteur['traiteur_piece_montee_choux']=='pieceChoux') { echo 'Pièce montée de choux';}
                               elseif ($Traiteur['traiteur_piece_montee_macarons']=='pieceMacaron') { echo 'Pièce montée de macarons';}
                                 else {echo 'wedding cake'; }?> </td>
                </tr>
                <tr>
                    <th> Autre type de desserts : </th>
                    <td> <?=$Traiteur['traiteur_autre_dessert'];?> </td>
                </tr>
                <tr>
                    <th> Les desserts sont fait : </th>
                    <td><?php if ($Traiteur['fabrication_dessert']=='vous') { echo 'Par le traiteur';}
                                else{ echo 'Par un patissier';} ?> </td>
                </tr>
            </tbody>

        </table>

    </div>
    
    <div class="d-flex justify-content-center">

        <table class="tableau_fiche_organisation table-bordered">
            <tbody>
                <th colspan="13" class="text-center"> <h4 class="bg-info text-white text-center"> Quelle sont les boissons qu'il peux fournir</h4>
                </th>
                <tr>
                    <td colspan="5" class="text-center"> Pour les cocktails </td>
                    <td colspan="4" class="text-center"> Pour les dîners </td>
                    <td colspan="3" class="text-center"> Pour les soirées </td>
                </tr>
        
                <tr>
                    <td class="p-2">Eaux <br> (plate/gazeuse)</td>
                    <td class="p-2">Softs<br>(jus,sodas...)</td>
                    <td class="p-2">Alcools</td>
                    <td class="p-2">Cocktails</td>
                    <td class="p-2">Champagne</td>
                    <td class="p-2">Eaux<br>(plate/gazeuse)</td>
                    <td class="p-2">Softs<br>(jus,sodas...)</td>
                    <td class="p-2">Champagne</td>
                    <td class="p-2">Vins</td>
                    <td class="p-2">Eaux<br>(plate/gazeuse)</td>
                    <td class="p-2">Softs<br>(jus,sodas...)</td>
                    <td class="p-2">Alcools</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="eauxCocktail" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_cocktail_eaux']=='eauxCocktail') { echo 'checked="checked"';}?>></td>
                                             
                    <td><input type="checkbox" name="softsCocktail" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_cocktail_softs']=='softsCocktail') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="alcoolsCocktail" class="form-control"disabled="disabled"
                        <?php if($Traiteur['traiteur_cocktail_alcools']=='alcoolsCocktail') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="cocktails" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_cocktail']=='cocktails') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="champagneCocktail" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_cocktail_champagne']=='champagneCocktail') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="eauxDiner" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_diner_eaux']=='eauxDiner') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="softsDiner" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_diner_softs']=='softsDiner') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="ChampagneDiner" class="form-control"disabled="disabled"
                        <?php if($Traiteur['traiteur_diner_champagne']=='ChampagneDiner') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="vinDiner" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_diner_vins']=='vinDiner') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="eauxSoiree" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_soiree_eaux']=='eauxSoiree') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="softsSoiree" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_soiree_softs']=='softsSoiree') { echo 'checked="checked"';}?>></td>
        
                    <td><input type="checkbox" name="alcoolSoiree" class="form-control" disabled="disabled"
                        <?php if($Traiteur['traiteur_soiree_alcools']=='alcoolSoiree') { echo 'checked="checked"';}?>></td>    
                </tr>
            </tbody>
        </table>
    </div>


    <div class="d-flex justify-content-around m-5">

        <table class="tableau_fiche_client">

            <tbody>

            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Vaisselles </h4>
            </th>
                <tr>
                    <th> Matière des bouteilles : </th>
                    <td> <?=$Traiteur['bouteille_matiere'];?> </td>
                </tr>
                <tr>
                    <th> Y a-t-il un supplément pour avoir <br> des bouteilles en verre : </th>
                    <td> <?=$Traiteur['bouteille_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Quel est le prix du supplément : </th>
                    <td> <?=$Traiteur['tarif_supplement'];?> </td>
                </tr>
                <tr>
                    <th> La vaisselle est : </th>
                    <td> <?php if ($Traiteur['vaisselle']=='vous') { echo 'À vous';}
                                 else {echo 'Louée'; }?></td>
                </tr>
                <tr>
                    <th> La vaisselle est-elle laissée apres le depart : </th>
                    <td> <?=$Traiteur['vaisselle_laissee'];?> </td>
                </tr>
                <tr>
                    <th>Modalités de retour de la vaisselle : </th>
                    <td> <?=$Traiteur['vaisselle_retour'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_fiche_client">

            <tbody>

            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Décorations </h4>
            </th>
                <tr>
                    <th> Décoration du buffet incluse : </th>
                    <td> <?=$Traiteur['decoration_buffet'];?> </td>
                </tr>
                <tr>
                    <th> Choix de couleurs du chemin <br> de table du buffet : </th>
                    <td> <?=$Traiteur['couleur_chemin_table'];?> </td>
                </tr>
                <tr>
                    <th> Le nappage et les serviettes sont : </th>
                    <td> <?php if ($Traiteur['nappage']=='vous') { echo 'À vous';}
                                 else {echo 'Louée'; }?> </td>
                </tr>
                <tr>
                    <th> Y a-t-il possibilité de récupérer les nappages <br> et les serviettes à l’avance : </th>
                    <td> <?=$Traiteur['recuperation_nappage'];?> </td>
                </tr>
                <tr>
                    <th> Apportez-vous le matériel nécessaire : </th>
                    <td> <?=$Traiteur['apporte_materiel'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_fiche_client">

            <tbody>

            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Dégustation </h4>
            </th>
                <tr>
                    <th> Faites-vous une visite technique <br> avant le jour J : </th>
                    <td> <?=$Traiteur['visite_technique'];?> </td>
                </tr>
                <tr>
                    <th> Faites-vous des dégustations : </th>
                    <td> <?=$Traiteur['validation_degustation'];?> </td>
                </tr>
                <tr>
                    <th> La dégustation est : </th>
                    <td> <?=$Traiteur['lieu_degustation'];?> </td>
                </tr>
                <tr>
                    <th> Nombre de personnes max au repas test : </th>
                    <td> <?=$Traiteur['nombre_personne_max'];?> </td>
                </tr>
                <tr>
                    <th> Prix par personne : </th>
                    <td> <?=$Traiteur['prix_par_personne'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_fiche_client">

            <tbody>

            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Tests </h4>
            </th>
                <tr>
                    <th> Composition du repas test </th>
                </tr>
                <tr>
                    <td> Cocktails </td>
                    <td><input type="checkbox" name="cocktailTest" class="form-control" disabled="disabled"
                            <?php if($Traiteur['test_cocktail']=='cocktail') { echo 'checked="checked"' ; } ?>></td>
                    <td> <?=$Traiteur['combien_cocktail_test'];?> </td>
                </tr>
                <tr>
                    <td>Entrees</td>
                    <td><input type="checkbox" name="entreeTest" class="form-control" disabled="disabled"
                            <?php if($Traiteur['test_entree']=='entree') { echo 'checked="checked"' ; } ?>></td>
                    <td> <?=$Traiteur['combien_entree_test'];?> </td>
                </tr>
                <tr>
                    <td>Plats</td>
                    <td><input type="checkbox" name="platTest" class="form-control" disabled="disabled"
                            <?php if($Traiteur['test_plat']=='plat') { echo 'checked="checked"' ; } ?>></td>
                    <td> <?=$Traiteur['combien_plat_test'];?> </td>
                </tr>
                <tr>
                    <td>Desserts</td>
                    <td><input type="checkbox" name="dessertTest" class="form-control" disabled="disabled"
                            <?php if($Traiteur['test_dessert']=='dessert') { echo 'checked="checked"' ; } ?>></td>
                    <td> <?=$Traiteur['combien_dessert_test'];?> </td>
                </tr>

            </tbody>

        </table>

    </div>

        <div class="d-flex justify-content-center m-5">
            <a href="../edit/editTraiteurs.php?numTraiteur=<?=$_GET['numTraiteur']?>"><button class="btn btn-primary"> Modification </button></a>
            <a class="ml-3" href="../../PHP/suppression/fichierSupTraiteurs.php?numTraiteur=<?=$_GET['numTraiteur']?>"><button class="btn btn-primary"> Suppression </button></a>
        </div>

</body>
</html>
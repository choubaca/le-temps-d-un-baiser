<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/fichePhotographes_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche du photographe</title>
</head>

<body>

    <h1 class="text-center m-5"> <?=$photographe['photographe_nom'];?> <?=$photographe['photographe_prenom'];?> </h1>


    <div class="d-flex justify-content-around">

        <table>

            <tbody class="tableau_fiche_client">
            
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Coordonnées et déplacement</h4>
                </th>
                <tr>
                    <th> Téléphone : </th>
                    <td> <?=$photographe['photographe_telephone'];?> </td>
                </tr>
                <tr>
                    <th> E-mail : </th>
                    <td> <?=$photographe['photographe_mail'];?> </td>
                </tr>
                <tr>
                    <th> Déplacement : </th>
                    <td> <?=$photographe['deplacement_validation'];?> </td>
                </tr>
                <tr>
                    <th> Supplément : </th>
                    <td> <?=$photographe['deplacement_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Prix au KM : </th>
                    <td> <?=$photographe['prix_au_km'];?> </td>
                </tr>
                <tr>
                    <th> Zone de deplacement : </th>
                    <td> <?=$photographe['deplacement_zone'];?> </td>
                </tr>
            </tbody>
        </table>
        
        <table>
            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Options</h4>
                </th>
                <tr>
                    <th> Délais de livraison : </th>
                    <td> <?=$photographe['delai_livraison'];?> </td>
                </tr>
                <tr>
                    <th> Retouche des photographes : </th>
                    <td> <?=$photographe['retouche_photo'];?> </td>
                </tr>
                <tr>
                    <th> Y a-t-il un booth : </th>
                    <td> <?=$photographe['validation_photobooth'];?> </td>
                </tr>
                <tr>
                    <th> Quelles sont ses options : </th>
                    <td> <?=$photographe['option_photobooth'];?> </td>
                </tr>
                <tr>
                    <th> Remarques divers : </th>
                    <td> <?=$photographe['photographe_remarque'];?> </td>
                </tr>
            </tbody>
        </table>
        <table>
            <tbody class="tableau_fiche_client">
            
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Photographies</h4>
                </th>
                <tr>
                    <th class="align-text-top"> Styles de photographes :</th>
                    <td>
                        <ul class="list-unstyled">
                            <li><?php if ($photographe['numerique']=='numeriquePhoto') { echo 'photographie numerique';}
                        else{ echo '';} ?></li>
                            <li><?php if ($photographe['noir_et_blanc']=='noirEtBlanc') { echo 'Noir et blanc';}
                        else{ echo '';} ?></li>
                            <li><?php if ($photographe['argentine']=='argentphotographe') { echo 'argentique';}
                        else{ echo '';} ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Autres type de photographes </th>
                    <td> <?=$photographe['autre_style'];?> </td>
                </tr>
                <tr>
                    <th class="align-text-top"> Types de support remis :</th>
                    <td>
                        <ul class="list-unstyled">
                            <li><?php if ($photographe['photographes_dvd']=='dvd') { echo 'DVD HD';}
                        else{ echo '';} ?></li>
                            <li><?php if ($photographe['photographes_coffret']=='coffret') { echo 'Coffret';}
                        else{ echo '';} ?></li>
                            <li><?php if ($photographe['photographes_tirages']=='tirages') { echo 'Tirages';}
                        else{ echo '';} ?></li>
                            <li><?php if ($photographe['photographes_usb']=='usb') { echo 'USB';}
                        else{ echo '';} ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Autres support : </th>
                    <td> <?=$photographe['photographes_Autre'];?> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="d-flex flex-column justify-content-center w-75 m-auto">
    
        <h5 class="bg-info text-white text-center m-5">Tarifs</h5>
        
            <div class="d-flex justify-content-around">
        
                <table>
        
                    <tbody class="tableau_fiche_client">
                    
                        <tr>
                            <th class="text-center">Formules par moments</th>
                            <th class="text-center">Prix</th>
                        </tr>
                        <tr>
                            <td> De la mairie au vin d’honneur </td>
                            <td> <?=$photographe['photographe_tarif1'];?> </td>
                        </tr>
                        <tr>
                            <td> De la mairie à la soirée </td>
                            <td> <?=$photographe['photographe_tarif2'];?> </td>
                        </tr>
                        <tr>
                            <td> Des préparatifs au vin d’honneur </td>
                            <td> <?=$photographe['photographe_tarif3'];?> </td>
                        </tr>
                        <tr>
                            <td> Des préparatifs à la soirée </td>
                            <td> <?=$photographe['photographe_tarif4'];?> </td>
                        </tr>
                        <tr>
                            <td> <?=$photographe['photographe_formule5'];?> </td>
                            <td> <?=$photographe['photographe_tarif5'];?> </td>
                        </tr>
                        <tr>
                            <td> <?=$photographe['photographe_formule6'];?> </td>
                            <td> <?=$photographe['photographe_tarif6'];?> </td>
                        </tr>
                        <tr>
                            <td> Autres </td>
                            <td> <?=$photographe['photographe_formule_moment'];?> </td>
                        </tr>
                    </tbody>
                </table>
        
                <table>
        
                    <tbody class="tableau_fiche_client">
                    
                        <tr>
                            <th class="text-center"> Formules par horaires </th>
                            <th class="text-center"> Prix </th>
                        </tr>
                        <tr>
                            <td> 6h de présence </td>
                            <td> <?=$photographe['photographe_tarif7'];?> </td>
                        </tr>
                        <tr>
                            <td> 8h de présence </td>
                            <td> <?=$photographe['photographe_tarif8'];?> </td>
                        </tr>
                        <tr>
                            <td> 12h de présence </td>
                            <td> <?=$photographe['photographe_tarif9'];?> </td>
                        </tr>
                        <tr>
                            <td> <?=$photographe['photographe_formule10'];?> </td>
                            <td> <?=$photographe['photographe_tarif10'];?> </td>
                        </tr>
                        <tr>
                            <td> <?=$photographe['photographe_formule11'];?> </td>
                            <td> <?=$photographe['photographe_tarif11'];?> </td>
                        </tr>
                        <tr>
                            <td> Autres </td>
                            <td> <?=$photographe['photographe_formule_horaire'];?> </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            
        </div>

        <div class="d-flex justify-content-center mt-4">
            <a href="../edit/editPhotographes.php?numPhotographe=<?=$_GET['numPhotographe']?>"><button class="btn btn-primary"> Modification </button></a>
            <a class="ml-3" href="../../PHP/suppression/fichierSupPhotographes.php?numPhotographe=<?=$_GET['numPhotographe']?>"><button class="btn btn-primary"> Suppression </button></a>
        </div>

</body>
</html>
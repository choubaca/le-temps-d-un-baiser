<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/ficheMaries_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche des mariés</title>
</head>

<body>

<div class="container">

    <div class="row">
    
        <fieldset>

            <table class="table-responsive-lg">

                <h1 class="text-center m-5"> Mariage de <?=$marie1['marie1_nom'];?> et <?=$marie1['marie2_nom'];?> </h1>

                    <tbody class="tableau_fiche_client">

                        <tr>
                            <th>État civil : </th>
                            <td> <?=$marie1['marie1_etat_civil'];?> </td>
                            <td></td>
                            <th>État civil : </th>
                            <td> <?=$marie1['marie2_etat_civil'];?> </td>
                        </tr>
                        <tr>
                            <th>Nom : </th>
                            <td> <?=$marie1['marie1_nom'];?> </td>
                            <td></td>
                            <th>Nom : </th>
                            <td> <?=$marie1['marie2_nom'];?> </td>
                        </tr>
                        <tr>
                            <th>Prénom : </th>
                            <td> <?=$marie1['marie1_prenom'];?> </td>
                            <td></td>
                            <th>Prénom : </th>
                            <td> <?=$marie1['marie2_prenom'];?> </td>
                        </tr>
                        <tr>
                            <th>Profession : </th>
                            <td> <?=$marie1['marie1_profession'];?> </td>
                            <td></td>
                            <th>Profession : </th>
                            <td> <?=$marie1['marie2_profession'];?> </td>
                        </tr>
                        <tr>
                            <th>Date de naissance : </th>
                            <td> <?= date('d/m/Y', strtotime($marie1['marie1_date_naissance']));?> </td>
                            <td></td>
                            <th>Date de naissance : </th>
                            <td> <?= date('d/m/Y', strtotime($marie1['marie2_date_naissance']));?> </td>
                        </tr>
                        <tr>
                            <th>Téléphone : </th>
                            <td> <?=$marie1['marie1_telephone'];?> </td>
                            <td></td>
                            <th>Téléphone : </th>
                            <td> <?=$marie1['marie2_telephone'];?> </td>
                        </tr>
                        <tr>
                            <th> E-mail : </th>
                            <td> <?=$marie1['marie1_email'];?> </td>
                            <td></td>
                            <th> E-mail : </th>
                            <td> <?=$marie1['marie2_email'];?> </td>
                        </tr>
                        <tr>
                            <th> Adresse : </th>
                            <td> <?=$marie1['marie1_adresse'];?> </td>
                            <td></td>
                            <th> Adresse : </th>
                            <td> <?=$marie1['marie2_adresse'];?> </td>
                        </tr>
                        <th></th>
                        <th class="bg-info text-white text-center p-1" colspan='3'>Temoins</th>

                        <tr>
                            <th>État civil : </th>
                            <td> <?=$marie1['temoin1_etat_civil'];?> </td>
                            <td></td>
                            <th>État civil : </th>
                            <td> <?=$marie1['temoin3_etat_civil'];?> </td>
                        </tr>
                        <tr>
                            <th>Nom : </th>
                            <td> <?=$marie1['temoin1_nom'];?> </td>
                            <td></td>
                            <th>Nom : </th>
                            <td> <?=$marie1['temoin3_nom'];?> </td>
                        </tr>
                        <tr>
                            <th>Prénom : </th>
                            <td> <?=$marie1['temoin1_prenom'];?> </td>
                            <td></td>
                            <th>Prénom : </th>
                            <td> <?=$marie1['temoin3_prenom'];?> </td>
                        </tr>
                        <tr>
                            <th>Téléphone : </th>
                            <td> <?=$marie1['temoin1_telephone'];?> </td>
                            <td></td>
                            <th>Téléphone : </th>
                            <td> <?=$marie1['temoin3_telephone'];?> </td>
                        </tr>
                        <tr>
                            <td colspan="5"></td>
                        </tr>
                        <tr <?php if (empty($marie1['temoin2_etat_civil'])){ echo 'class="d-none"'; }else {echo 'class=""';}?>>
                            <th>État civil : </th>
                            <td> <?=$marie1['temoin2_etat_civil'];?> </td>
                            <td></td>
                            <th>État civil : </th>
                            <td> <?=$marie1['temoin4_etat_civil'];?> </td>
                        </tr>
                        <tr <?php if (empty($marie1['temoin2_etat_civil'])){ echo 'class="d-none"'; }else {echo 'class=""';}?>>
                            <th>Nom : </th>
                            <td> <?=$marie1['temoin2_nom'];?> </td>
                            <td></td>
                            <th>Nom : </th>
                            <td> <?=$marie1['temoin4_nom'];?> </td>
                        </tr>
                        <tr <?php if (empty($marie1['temoin2_etat_civil'])){ echo 'class="d-none"'; }else {echo 'class=""';}?>>
                            <th>Prénom : </th>
                            <td> <?=$marie1['temoin2_prenom'];?> </td>
                            <td></td>
                            <th>Prénom : </th>
                            <td> <?=$marie1['temoin4_prenom'];?> </td>
                        </tr>
                        <tr <?php if (empty($marie1['temoin2_etat_civil'])){ echo 'class="d-none"'; }else {echo 'class=""';}?>>
                            <th>Téléphone : </th>
                            <td> <?=$marie1['temoin2_telephone'];?> </td>
                            <td></td>
                            <th>Téléphone : </th>
                            <td> <?=$marie1['temoin4_telephone'];?> </td>
                        </tr>

                    </tbody>

                </table>

        </fieldset>

    </div>

</div>
    
        

    <div class="d-flex justify-content-center mt-4">
        <a href="../edit/editMaries.php?numMarie=<?=$_GET['numMarie']?>"><button class="btn btn-primary"> Modification </button></a>
        <a class="ml-3" href="../../PHP/suppression/fichierSupMaries.php?numMarie=<?=$_GET['numMarie']?>"><button class="btn btn-primary"> Suppression </button></a>
    </div>


    <div class="d-flex justify-content-center m-4">
        <a href="ficheOrganisations.php?numMarie=<?=$_GET['numMarie']?>"><button class="btn btn-primary"> Organisation </button></a>
        <a class="ml-3" href="fichePrestataires.php?numMarie=<?=$_GET['numMarie']?>"><button class="btn btn-primary"> Prestataires </button></a>
    </div>
    

</body>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>
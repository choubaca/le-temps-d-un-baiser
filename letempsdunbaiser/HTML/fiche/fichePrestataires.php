<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/fichePrestataires_post.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche des prestataires</title>
</head>

<body>

    <h2 class="text-center m-5 "> Les prestataires </h2>

<div class="d-flex justify-content-center m-4">

    <table class='m-4'>
        <thead>
            <tr>
                <th class="text-center p-1" colspan="2"><u>DJ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class='pl-3'>Contact</th>
                <td class='pl-3'><?= $Prestataires['dj_nom']; ?> <?= $Prestataires['dj_prenom']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>Téléphone</th>
                <td class='pl-3'><?= $Prestataires['dj_telephone']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>E-mail</th>
                <td class='pl-3'><?= $Prestataires['dj_mail']; ?></td>
            </tr>
        </tbody>
    </table>

    <table class='m-4 border-left'>
        <thead>
            <tr>
                <th class="text-center p-1" colspan="2"><u>Fleuriste</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class='pl-3'>Contact</th>
                <td class='pl-3'><?= $Prestataires['fleuriste_nom']; ?> <?= $Prestataires['fleuriste_prenom']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>Téléphone</th>
                <td class='pl-3'><?= $Prestataires['fleuriste_telephone']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>E-mail</th>
                <td class='pl-3'><?= $Prestataires['fleuriste_mail']; ?></td>
            </tr>
        </tbody>
    </table>

    <table class='m-4 border-left'>
        <thead>
            <tr>
                <th class="text-center p-1" colspan="2"><u>Lieu</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class='pl-3'>Contact</th>
                <td class='pl-3'><?= $Prestataires['contact_nom']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>Téléphone</th>
                <td class='pl-3'><?= $Prestataires['contact_telephone']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>E-mail</th>
                <td class='pl-3'><?= $Prestataires['contact_mail']; ?></td>
            </tr>
        </tbody>
    </table>

    <table class='m-4 border-left'>
        <thead>
            <tr>
                <th class="text-center p-1" colspan="2"><u>Nounou</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class='pl-3'>Contact</th>
                <td class='pl-3'><?= $Prestataires['nounou_nom']; ?> <?= $Prestataires['nounou_prenom']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>Téléphone</th>
                <td class='pl-3'><?= $Prestataires['nounou_telephone']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>E-mail</th>
                <td class='pl-3'><?= $Prestataires['nounou_mail']; ?></td>
            </tr>
        </tbody>
    </table>

</div>

<div class="d-flex justify-content-center m-5">

    <table class='m-4'>
        <thead>
            <tr>
                <th class="text-center p-1" colspan="2"><u>Photographe</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class='pl-3'>Contact</th>
                <td class='pl-3'><?= $Prestataires['photographe_nom']; ?> <?= $Prestataires['photographe_prenom']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>Téléphone</th>
                <td class='pl-3'><?= $Prestataires['photographe_telephone']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>E-mail</th>
                <td class='pl-3'><?= $Prestataires['photographe_mail']; ?></td>
            </tr>
        </tbody>
    </table>

    <table class='m-4 border-left'>
        <thead>
            <tr>
                <th class="text-center p-1" colspan="2"><u>Traiteur</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th class='pl-3'>Contact</th>
                <td class='pl-3'><?= $Prestataires['traiteur_nom']; ?> <?= $Prestataires['traiteur_prenom']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>Téléphone</th>
                <td class='pl-3'><?= $Prestataires['traiteur_telephone']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>E-mail</th>
                <td class='pl-3'><?= $Prestataires['traiteur_mail']; ?></td>
            </tr>
        </tbody>
    </table>

    <table class='m-4 border-left'>
        <thead>
            <tr>
                <th class="text-center p-1" colspan="2"><u>Vidéaste</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class='pl-3'>Contact</th>
                <td class='pl-3'><?= $Prestataires['videaste_nom']; ?> <?= $Prestataires['videaste_prenom']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>Téléphone</th>
                <td class='pl-3'><?= $Prestataires['videaste_telephone']; ?></td>
            </tr>
            <tr>
                <th class='pl-3'>E-mail</th>
                <td class='pl-3'><?= $Prestataires['videaste_mail']; ?></td>
            </tr>
        </tbody>
    </table>

</div>

<div class=" d-flex justify-content-center">
    <a href="../../HTML/ajout/ajoutPrestataires.php?numMarie=<?=$_GET['numMarie']?>"><button class="btn btn-primary mr-5">Ajouter/modifier des prestataires</button></a>
    <a href="ficheMaries.php?numMarie=<?=$_GET['numMarie']?>"><button class="btn btn-primary">retour</button></a>
</div>

</body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/ficheLieux_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche du lieu</title>
</head>

<body>

    <h1 class="text-center mb-5"> <?=$Lieu['lieu_nom']; ?> </h1>

    <h4 class="bg-info text-white text-center mb-5">Contact</h4>

    <div class="d-flex justify-content-around m-5">

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <th> Le type de lieu : </th>
                    <td> <?=$Lieu['lieu_type']; ?> </td>
                </tr>
                <tr>
                    <th> Ville : </th>
                    <td> <?=$Lieu['lieu_ville']; ?> </td>
                </tr>
                <tr>
                    <th> Distance : </th>
                    <td> <?=$Lieu['lieu_distance']; ?> </td>
                </tr>
                <tr>
                    <th> Le temps de trajet : </th>
                    <td> <?=$Lieu['lieu_temps_trajet']; ?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <th> Nom : </th>
                    <td> <?=$Lieu['contact_nom']; ?> </td>
                </tr>
                <tr>
                    <th> Téléphone : </th>
                    <td> <?=$Lieu['contact_telephone']; ?> </td>
                </tr>
                <tr>
                    <th> Adresse mail : </th>
                    <td> <?=$Lieu['contact_mail']; ?> </td>
                </tr>
                <tr>
                    <th> planB : </th>
                    <td> <?=$Lieu['lieu_plan_B'];?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="d-flex justify-content-around mb-5">

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <td colspan="2">
                        <h4 class="bg-info text-white text-center"> La salle principale </h4>
                    </td>
                </tr>
                <tr>
                    <th> Longueur de la salle : </th>
                    <td> <?=$Lieu['salle1_longueur']; ?> m</td>
                </tr>
                <tr>
                    <th> Largeur de la salle : </th>
                    <td> <?=$Lieu['salle1_largeur'];?> m</td>
                </tr>
                <tr>
                    <th> Surface : </th>
                    <td> <?=$Lieu['salle1_longueur'] * $Lieu['salle1_largeur'];?> m²</td>
                </tr>
                <tr>
                    <th> Nombre maximum de personnes : </th>
                    <td> <?=$Lieu['salle1_nombre_max_personne'];?> personnes</td>
                </tr>
                <tr>
                    <th> Séparation de salle : </th>
                    <td> <?=$Lieu['salle1_separation'];?> </td>
                </tr>
                <tr>
                    <th> Salle chauffée : </th>
                    <td> <?=$Lieu['salle1_chauffage'];?> </td>
                </tr>
                <tr>
                    <th> Salle climatisée : </th>
                    <td> <?=$Lieu['salle1_climatisation'];?> </td>
                </tr>
                <tr>
                    <th> Vin d'honneur en extérieur : </th>
                    <td> <?=$Lieu['salle1_vin_en_exterieur'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <td colspan="2">
                        <h4 class="bg-info text-white text-center"> La salle numéro 2 </h4>
                    </td>
                </tr>
                <tr>
                    <th> Longueur de la salle : </th>
                    <td> <?=$Lieu['salle2_longueur'];?> m</td>
                </tr>
                <tr>
                    <th> Largeur de la salle : </th>
                    <td> <?=$Lieu['salle2_largeur'];?> m</td>
                </tr>
                <tr>
                    <th> Surface : </th>
                    <td> <?=$Lieu['salle2_longueur'] * $Lieu['salle2_largeur'];?> m² </td>
                </tr>
                <tr>
                    <th> Nombre maximum de personnes : </th>
                    <td> <?=$Lieu['salle2_nombre_max_personne'];?> personnes</td>
                </tr>
                <tr>
                    <th> Séparation de salle : </th>
                    <td> <?=$Lieu['salle2_separation'] ?> </td>
                </tr>
                <tr>
                    <th> Salle chauffée : </th>
                    <td> <?=$Lieu['salle2_chauffage'] ?> </td>
                </tr>
                <tr>
                    <th> Salle climatisée : </th>
                    <td> <?=$Lieu['salle2_climatisation'] ?> </td>
                </tr>
                <tr>
                    <th> Utilisation de la salle : </th>
                    <td> <?=$Lieu['salle2_utilisation'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <td colspan="2">
                        <h4 class="bg-info text-white text-center"> La salle numéro 3 </h4>
                    </td>
                </tr>
                <tr>
                    <th> Longueur de la salle : </th>
                    <td> <?=$Lieu['salle3_longueur']; ?> m</td>
                </tr>
                <tr>
                    <th> Largeur de la salle : </th>
                    <td> <?=$Lieu['salle3_largeur'] ?> m</td>
                </tr>
                <tr>
                    <th> Surface : </th>
                    <td> <?=$Lieu['salle3_longueur'] * $Lieu['salle3_largeur'];?> m² </td>
                </tr>
                <tr>
                    <th> Nombre maximum de personnes : </th>
                    <td> <?=$Lieu['salle3_nombre_max_personne'] ?> personnes</td>
                </tr>
                <tr>
                    <th> Séparation de salle : </th>
                    <td> <?=$Lieu['salle3_separation'] ?> </td>
                </tr>
                <tr>
                    <th> Salle chauffée : </th>
                    <td> <?=$Lieu['salle3_chauffage'] ?> </td>
                </tr>
                <tr>
                    <th> Salle climatisée : </th>
                    <td> <?=$Lieu['salle3_climatisation'] ?> </td>
                </tr>
                <tr>
                    <th> Utilisation de la salle : </th>
                    <td> <?=$Lieu['salle3_utilisation'] ?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="d-flex justify-content-around mb-5">

        <table class="tableau_Lieu">

            <tbody>
            
                <tr>
                    <td colspan="2">
                        <h4 class="bg-info text-white text-center"> Annexe </h4>
                    </td>
                </tr>
                <tr>
                    <th> Terrasse: </th>
                    <td> <?=$Lieu['terrasse'] ?> </td>
                </tr>
                <tr>
                    <th> Surface de la terrasse : </th>
                    <td> <?=$Lieu['surface_terrasse'] ?> m²</td>
                </tr>
                <tr>
                    <th> Jardin : </th>
                    <td> <?=$Lieu['jardin'] ?> </td>
                </tr>
                <tr>
                    <th> Surface du jardin : </th>
                    <td> <?=$Lieu['surface_jardin'] ?> m²</td>
                </tr>
                <tr>
                    <th> Parc : </th>
                    <td> <?=$Lieu['parc'] ?> </td>
                </tr>
                <tr>
                    <th> Surface du parc : </th>
                    <td> <?=$Lieu['surface_parc'] ?> m²</td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <td colspan="2">
                        <h4 class="bg-info text-white text-center"> Accessibilité </h4>
                    </td>
                </tr>
                <tr>
                    <th> parking : </th>
                    <td> <?=$Lieu['parking'] ?> </td>
                </tr>
                <tr>
                    <th> Nombre de places : </th>
                    <td> <?=$Lieu['place_parking'] ?> </td>
                </tr>
                <tr>
                    <th> Vestiaire : </th>
                    <td> <?=$Lieu['vestiaire'] ?> </td>
                </tr>
                <tr>
                    <th> Nombre de WC pour homme : </th>
                    <td> <?=$Lieu['toilette_homme'] ?> </td>
                </tr>
                <tr>
                    <th> Nombre de WC pour femme : </th>
                    <td> <?=$Lieu['toilette_femme'] ?> </td>
                </tr>
                <tr>
                    <th> Acces aux PMR : </th>
                    <td> <?=$Lieu['PMR'] ?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_Lieu">

            <tbody>
                <tr>
                    <td colspan="2">
                        <h4 class="bg-info text-white text-center"> Équipements </h4>
                    </td>
                </tr>
                <tr>
                    <th class="align-text-top"> Matériel disponible : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li><?php if ($Lieu['retroprojecteur']=='lieuRetropro') { echo 'Rétroprojecteur';}
                        else{ echo '';} ?></li>
                            <li><?php if ($Lieu['ecran']=='lieuEcran') { echo 'Ecran';}
                        else{ echo '';} ?></li>
                            <li><?php if ($Lieu['ampli']=='lieuAmpli') { echo 'Ampli';}
                        else{ echo '';} ?></li>
                            <li><?php if ($Lieu['enceinte']=='lieuEnceinte') { echo 'Enceinte';}
                        else{ echo '';} ?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Le mobilier est-il compris <br> dans le tarif de la location ? </th>
                    <td> <?=$Lieu['prix_mobilier_compris']; ?> </td>
                </tr>
                <tr>
                    <th> Autres : </th>
                    <td> <?=$Lieu['autre_mobilier']; ?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <h4 class="bg-info text-white text-center mb-5"> Matériel mis à disposition </h4>

    <div class="table d-flex justify-content-center mb-5">

        <table>

            <tbody>

                <tr>
                    <td></td>
                    <td class="text-center"> Dimensions </td>
                    <td class="text-center"> Nombre de personnes </td>
                    <td class="text-center"> Matière </td>
                    <td class="text-center"> Quantité </td>
                </tr>
                <tr>
                    <td> Table ronde </td>
                    <td class="text-center"> <?=$Lieu['table_ronde_dimension'] ?></td>
                    <td class="text-center"> <?=$Lieu['table_ronde_nombre_de_personnes'] ?> </td>
                    <td class="text-center"> <?=$Lieu['table_ronde_matiere'] ?></td>
                    <td class="text-center"> <?=$Lieu['table_ronde_nombre'] ?></td>
                </tr>
                <tr>
                    <td> Table rectangulaire </td>
                    <td class="text-center"> <?=$Lieu['table_rectanculaire_dimension'] ?></td>
                    <td class="text-center"> <?=$Lieu['table_rectanculaire_nombre_de_personnes'] ?> </td>
                    <td class="text-center"> <?=$Lieu['table_rectanculaire_matiere'] ?> </td>
                    <td class="text-center"> <?=$Lieu['table_rectanculaire_nombre'] ?></td>
                </tr>
                <tr>
                    <td> Buffet </td>
                    <td class="text-center"> <?=$Lieu['buffet_dimension'] ?> </td>
                    <td class="text-center"> <?=$Lieu['buffet_nombre_de_personnes'] ?></td>
                    <td class="text-center"> <?=$Lieu['buffet_matiere'] ?> </td>
                    <td class="text-center"> <?=$Lieu['buffet_nombre'] ?></td>
                </tr>
                <tr>
                    <td> Autres </td>
                    <td colspan="4"> <?=$Lieu['autre_table'] ?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="table d-flex justify-content-center mb-5">

        <table>

            <tbody>

                <tr>
                    <td></td>
                    <td class="text-center"> Type </td>
                    <td class="text-center"> Matière </td>
                    <td class="text-center"> Quantité </td>
                </tr>
                <tr>
                    <td> Chaises interieures </td>
                    <td class="text-center"> <?=$Lieu['chaise_interieur_type'] ?> </td>
                    <td class="text-center"> <?=$Lieu['chaise_interieur_matiere'] ?> </td>
                    <td class="text-center"> <?=$Lieu['chaise_interieur_nombre'] ?></td>
                </tr>
                <tr>
                    <td> Chaises extèrieures </td>
                    <td class="text-center"> <?=$Lieu['chaise_exterieur_type'] ?></td>
                    <td class="text-center"> <?=$Lieu['chaise_exterieur_matiere'] ?> </td>
                    <td class="text-center"> <?=$Lieu['chaise_exterieur_nombre'] ?></td>
                </tr>
                <tr>
                    <td> Autres </td>
                    <td colspan="3"> <?=$Lieu['autre_chaise'] ?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <h4 class="bg-info text-white text-center mb-5"> Tarifs </h4>

    <div class="d-flex justify-content-center mb-5">

        <table class="w-75">

            <tbody class="table-bordered">

                <tr>
                    <td class="text-center">
                        <h5> Jour et heure </h5>
                    </td>
                    <td class="text-center">
                        <h5> Jour et heure </h5>
                    </td>
                    <td colspan="3" class="text-center">
                        <h5> Tarif / Saison </h5>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"> </td>
                    <td class="text-center">
                        <h6> Haute </h6>
                    </td>
                    <td class="text-center">
                        <h6> Moyenne </h6>
                    </td>
                    <td class="text-center">
                        <h6> Basse </h6>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"> Du <br> <?=$Lieu['date1']; ?> <br> à partir de <br> <?=$Lieu['heure1']; ?>
                    </td>
                    <td class="text-center"> Au <br> <?=$Lieu['date2']; ?> <br> à partir de <br> <?=$Lieu['heure2']; ?>
                    </td>
                    <td class="text-center"> <?=$Lieu['tarif_saison_haute1']; ?> </td>
                    <td class="text-center"> <?=$Lieu['tarif_moyenne_saison1']; ?> </td>
                    <td class="text-center"> <?=$Lieu['tarif_basse_saison1']; ?> </td>
                </tr>
                <tr>
                    <td class="text-center"> Du <br> <?=$Lieu['date3']; ?> <br> à partir de <br> <?=$Lieu['heure3']; ?>
                    </td>
                    <td class="text-center"> Au <br> <?=$Lieu['date4']; ?> <br> à partir de <br> <?=$Lieu['heure4']; ?>
                    </td>
                    <td class="text-center"> <?=$Lieu['tarif_saison_haute2']; ?> </td>
                    <td class="text-center"> <?=$Lieu['tarif_moyenne_saison2']; ?> </td>
                    <td class="text-center"> <?=$Lieu['tarif_basse_saison2']; ?> </td>
                </tr>
                <tr>
                    <td class="text-center"> Du <br> <?=$Lieu['date5']; ?> <br> à partir de <br> <?=$Lieu['heure5']; ?>
                    </td>
                    <td class="text-center"> Au <br> <?=$Lieu['date6']; ?> <br> à partir de <br> <?=$Lieu['heure6']; ?>
                    </td>
                    <td class="text-center"> <?=$Lieu['tarif_saison_haute3']; ?> </td>
                    <td class="text-center"> <?=$Lieu['tarif_moyenne_saison3']; ?> </td>
                    <td class="text-center"> <?=$Lieu['tarif_basse_saison3']; ?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="d-flex justify-content-around mb-5">

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <td colspan="2"> <h4 class="bg-info text-white text-center"> Extras </h4>
                    </td>
                </tr>
                <tr>
                    <th> Le prix du nettoyage est inclue : </th>
                    <td> <?=$Lieu['nettoyage_inclue']; ?> </td>
                </tr>
                <tr>
                    <th> Prix du nettoyage : </th>
                    <td> <?=$Lieu['tarif_nettoyage']; ?> </td>
                </tr>
                <tr>
                    <th> La restauration est imposée : </th>
                    <td> <?=$Lieu['restauration_impose']; ?> </td>
                </tr>
                <tr>
                    <th> Prix de la restauration : </th>
                    <td> <?=$Lieu['tarif_restauration']; ?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <td colspan="2"> <h4 class="bg-info text-white text-center"> Hébergement </h4>
                    </td>
                </tr>
                <tr>
                    <th> Possibilité d'hébergement sur place : </th>
                    <td> <?=$Lieu['hebergement_sur_place']; ?> </td>
                </tr>
                <tr>
                    <th> Combien de chambre : </th>
                    <td> <?=$Lieu['nombre_de_chambre']; ?> </td>
                </tr>
                <tr>
                    <th> Capacité totale : </th>
                    <td> <?=$Lieu['capacite_total']; ?> </td>
                </tr>
                <tr>
                    <th>
                        <p> Le prix par chambre est compris entre </p>
                    </th>
                    <td>
                        <p class="d-flex"> <?=$Lieu['prix_chambre1']; ?> et <?=$Lieu['prix_chambre2']; ?> </p>
                    </td>
                </tr>
                <tr>
                    <th> Forfait de location de la totalité des hébergements : </th>
                    <td> <?=$Lieu['forfait_location']; ?> </td>
                </tr>
                <tr>
                    <th> Forfait location salle + hébergement : </th>
                    <td> <?=$Lieu['forfait_location_et_hebergement']; ?> </td>
                </tr>

            </tbody>

        </table>

        <table class="tableau_Lieu">

            <tbody>

                <tr>
                    <td colspan="2"> <h4 class="bg-info text-white text-center"> Questions Diverses </h4>
                    </td>
                </tr>
                <tr>
                    <th> Salle pour coucher les enfants : </th>
                    <td> <?=$Lieu['lieu_salle_occupation_enfant']; ?> </td>
                </tr>
                <tr>
                    <th> Salle pour occuper les enfants : </th>
                    <td> <?=$Lieu['lieu_salle_repos_enfant']; ?> </td>
                </tr>
                <tr>
                    <th> Un seul mariage/week end ? </th>
                    <td> <?=$Lieu['lieu_mariage_unique']; ?> </td>
                </tr>
                <tr>
                    <th> Accompte à verser pour réservation : </th>
                    <td> <?=$Lieu['lieu_accompte']; ?> </td>
                </tr>
                <tr>
                    <th> Avez-vous l’habitude de travailler <br> avec des Wedding Planner ? </th>
                    <td> <?=$Lieu['lieu_travailWP']; ?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="d-flex justify-content-center mb-5">

        <table>

            <tbody>

                <tr>
                    <th> Remarques : </th>
                    <td> <?=$Lieu['lieu_remarque']; ?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="d-flex justify-content-center m-5">
        <a href="../edit/editLieux.php?numLieu=<?=$_GET['numLieu']?>"><input type="submit" name="submit" class="btn btn-primary mr-5" value="Modification"/></a>
        <a href="../../PHP/suppression/fichierSupLieux.php?numLieu=<?=$_GET['numLieu']?>"><input type="submit" name="submit" class="btn btn-primary" value="Suppression"/></a>
    </div>

</body>
</html>
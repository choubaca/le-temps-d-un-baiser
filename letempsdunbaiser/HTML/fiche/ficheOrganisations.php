<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }
    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/ficheOrganisations_post.php");
    
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche de l'organisation</title>
</head>

<body>
    <div class="text-center">
        <h1> Organisation </h1>
        <h3> du mariage de <?=$organisations['marie1_prenom'];?> et <?=$organisations['marie2_prenom'];?> </h3>

<div class="d-flex justify-content-around m-5">

        <table class="tableau_fiche_client">
            <tbody>
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Informations</h4>
                </th>
                <tr>
                    <th> Date du mariage : </th>
                    <td> <?=date('d/m/Y', strtotime($organisations['date_mariage']));?> </td>
                </tr>
                <tr>
                    <th> Lieu du mariage : </th>
                    <td> <?=$organisations['lieu_ceremonie'];?> </td>
                </tr>
                <tr>
                    <th class="align-text-top"> Nombre total d'invités : </th>
                    <td> <?php if(!empty ($organisations['nombre_adulte'])) $organisations['nombre_adulte'] + $organisations['nombre_enfant'];?>
                        <ul class="list-unstyled">
                            <li>Nombre d'adultes : <?=$organisations['nombre_adulte'];?></li>
                            <li>Nombre d'enfants : <?=$organisations['nombre_enfant'];?></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Budget de la réception : </th>
                    <td> <?=$organisations['budget_reception'];?></td>
                </tr>
            </tbody>
        </table>

    <div>

        <table class="tableau_fiche_client mb-2">
            <tbody>
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Lieu des cérémonies</h4>
                </th>
                <tr>
                    <th> Cérémonie à la mairie : </th>
                    <td> <?=$organisations['lieu_mairie'];?> </td>
                </tr>
                <tr>
                    <th> Cérémonie religieuse : </th>
                    <td> <?=$organisations['lieu_ceremonie_religieuse'];?> </td>
                </tr>
                <tr>
                    <th> Cérémonie laïque : </th>
                    <td> <?=$organisations['lieu_ceremonie_laique'];?> </td>
                </tr>
            </tbody>
        </table>

        <table class="tableau_fiche_client">
            <tbody>
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Timing et réception</h4>
                </th>
                <tr>
                    <th> La cérémonie civil a lieu : </th>
                    <td>
                        <?php if ($organisations['event_ceremonie']=='Lememejour') { echo 'Le même jour';}
                        else{ echo 'À une date antérieur';}?> </td>
                </tr>
                <tr>
                    <th> La récéption a lieu : </th>
                    <td>
                        <?php if ($organisations['event_reception']=='leMidi') { echo 'Le midi';}
                        elseif($organisations['event_reception']=='leSoir'){ echo 'Le soir';}
                        else{ echo 'Sur plusieurs jours';}?> </td>
                </tr>
            </tbody>
        </table>

    </div> 

        <table class="tableau_fiche_client">
            <tbody>
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Info lieu de récéption</h4>
                </th>
                <tr>
                    <th> Type de lieu : </th>
                    <td>
                        <?php if ($organisations['type_de_lieu']=='Chateau') { echo 'Château';}
                        elseif($organisations['type_de_lieu']=='corpsDeFerme'){ echo 'Corps de ferme';}
                        else{ echo $organisations['type_de_lieu'];}?> <?=$organisations['autre_type_de_lieu'];?></td>
                </tr>
                <tr>
                    <th> Localisation Géographique : </th>
                    <td> <?=$organisations['localisation_geographique'] ?> </td>
                </tr>
                <tr>
                    <th> Critére sur place : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li> <?php if ($organisations['espace_exterieur']=='espaceExt') { echo 'Espace exterieur';}
                        else{ echo '';}?> </li>
                            <li> <?php if ($organisations['chambre_maries']=='chambreMaries') { echo 'Chambre des mariés';}
                        else{ echo '';}?> </li>
                            <li> <?php if ($organisations['piscine']=='piscine') { echo 'Piscine';}
                        else{ echo '';}?> </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Hébergements : </th>
                    <td> <?=$organisations['hebergement_sur_place'];?> <?=$organisations['nombre_hebergement'];?> <?=$organisations['autre_hebergement'];?> </td>
                </tr>
            </tbody>
        </table>

</div>

<div class="d-flex justify-content-around mb-5">

    <table class="tableau_fiche_client">
        <tbody>
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Invités</h4>
                </th>
            <tr>
                <th> Convives au vin d'honneur : </th>
                <td> <?=$organisations['vin_dhonneur'];?> </td>
            </tr>
            <tr>
                <th> Convives au diner : </th>
                <td> <?=$organisations['diner'];?> </td>
            </tr>
            <tr>
                <th> Brunch le lendemain : </th>
                <td> <?=$organisations['brunch_lendemain'];?> </td>
            </tr>
            <tr>
                <th> Convives au brunch : </th>
                <td> <?=$organisations['brunch'];?> </td>
            </tr>
        </tbody>
    </table>
    
    <table class="tableau_fiche_client">
        <tbody>
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Organisation du diner</h4>
            </th>
            <tr>
                <th class="align-text-top"> Nombre de pièces cocktail : </th>
                <td> <?=$organisations['nombre_de_piece'] ?> </td>
            </tr>
            <tr>
                <th class="align-text-top"> Dérouler du diner : </th>
                <td>
                    <ul class="list-unstyled">
                        <li><?php if ($organisations['mise_en_bouche']=='miseEnBouche') { echo 'Mise en bouche';}
                    else{ echo '';}?></li>
                        <li><?=$organisations['viande'];?></li>
                        <li><?php if ($organisations['entree']=='entree') { echo 'Entrée';}
                    else{ echo '';}?></li>
                        <li><?=$organisations['fromage'];?></li>
                        <li><?=$organisations['poisson'];?></li>
                        <li><?=$organisations['dessert'];?></li>
                    </ul>
                </td>
            </tr>
             <tr>
                <th> Type de diner : </th>
                <td> <?php if ($organisations['type_de_diner']=='dinerAssis') { echo 'Dîner assis';}
                elseif($organisations['type_de_diner']=='cocktail'){ echo 'Cocktail dinatoire';}
                else{ echo $organisations['type_de_diner'];}?> </td>
            </tr>
            <tr>
                <th> Type de dessert : </th>
                <td>
                    <ul class="list-unstyled">
                        <li><?php if ($organisations['organisation_dessert_piece_montee_choux']=='pieceMonteeChoux') { echo 'Pièce montée de choux';}
                    else{ echo '';}?></li>
                        <li><?php if ($organisations['organisation_dessert_wedding_cake']=='weddingCake') { echo 'Wedding cake';}
                    else{ echo '';}?></li>
                        <li><?php if ($organisations['organisation_dessert_piece_montee_macarons']=='pieceMonteeMacaron') { echo 'Pièce montée de macaron';}
                    else{ echo '';}?></li>
                        <li><?=$organisations['organisation_dessert_autre_dessert'];?></li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>

</div>

<div class="d-flex justify-content-center mb-5">

    <table class="tableau_fiche_organisation m-3 table-bordered">
        <tbody>
            <th colspan="15" class="text-center"> <h4 class="bg-info text-white text-center">Boissons</h4>
            </th>
            <tr>
                <td colspan="2" rowspan="2" class="sans_"></td>
                <td colspan="5" class="text-center"> Vin d'honneur </td>
                <td colspan="4" class="text-center"> Dîner </td>
                <td colspan="4" class="text-center"> Soirée </td>
            </tr>
    
            <tr>
                <td class="p-2">Eaux <br> (plate/gazeuse)</td>
                <td class="p-2">Softs<br>(jus,sodas...)</td>
                <td class="p-2">Cocktails</td>
                <td class="p-2">Alcools</td>
                <td class="p-2">Champagne</td>
                <td class="p-2">Eaux<br>(plate/gazeuse)</td>
                <td class="p-2">Softs<br>(jus,sodas...)</td>
                <td class="p-2">Vins</td>
                <td class="p-2">Alcools</td>
                <td class="p-2">Eaux<br>(plate/gazeuse)</td>
                <td class="p-2">Softs<br>(jus,sodas...)</td>
                <td class="p-2">Alcools</td>
                <td class="p-2">Champagne</td>
            </tr>
            <tr>
                <td colspan="2">Les mariés</td>
                <td><input type="radio" name="vinDhonneurEau" class="form-control" value="vinDhonneurEauMaries" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_eau']=='vinDhonneurEauMaries') { echo 'checked="checked"';}?>></td>
                                         
                <td><input type="radio" name="vinDhonneurSofts" class="form-control" value="vinDhonneurSoftsMaries" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_soft']=='vinDhonneurSoftsMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="vinDhonneurCocktail" class="form-control" value="vinDhonneurCocktailMaries" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_cocktail']=='vinDhonneurCocktailMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="vinDhonneurAlcools" class="form-control" value="vinDhonneurAlcoolsMaries" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_alcool']=='vinDhonneurAlcoolsMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="vinDhonneurChampagne" class="form-control" value="vinDhonneurChampagneMaries" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_champagne']=='vinDhonneurChampagneMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerEau" class="form-control" value="dinerEauMaries" disabled="disabled"
                <?php if($organisations['organisation_diner_eau']=='dinerEauMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerSofts" class="form-control" value="dinerSoftsMaries" disabled="disabled"
                <?php if($organisations['organisation_diner_softs']=='dinerSoftsMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerVin" class="form-control" value="dinerVinMaries" disabled="disabled"
                <?php if($organisations['organisation_diner_vin']=='dinerVinMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerAlcools" class="form-control" value="dinerAlcoolsMaries" disabled="disabled"
                <?php if($organisations['organisation_diner_alcools']=='dinerAlcoolsMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeEau" class="form-control" value="soireeEauMaries" disabled="disabled"
                <?php if($organisations['organisation_soiree_eau']=='soireeEauMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeSofts" class="form-control" value="soireeSoftsMaries" disabled="disabled"
                <?php if($organisations['organisation_soiree_softs']=='soireeSoftsMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeAlcools" class="form-control" value="soireeAlcoolsMaries" disabled="disabled"
                <?php if($organisations['organisation_soiree_alcool']=='soireeAlcoolsMaries') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeChampagne" class="form-control" value="soireeChampagneMaries" disabled="disabled"
                <?php if($organisations['organisation_soiree_champagne']=='soireeChampagneMaries') { echo 'checked="checked"';}?>></td>
    
            </tr>
    
            <tr>
                <td colspan="2">Le traiteur</td>
                <td><input type="radio" name="vinDhonneurEau" class="form-control" value="vinDhonneurEauTraiteur" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_eau']=='vinDhonneurEauTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="vinDhonneurSofts" class="form-control" value="vinDhonneurSoftsTraiteur" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_soft']=='vinDhonneurSoftsTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="vinDhonneurCocktail" class="form-control" value="vinDhonneurCocktailTraiteur" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_cocktail']=='vinDhonneurCocktailTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="vinDhonneurAlcools" class="form-control" value="vinDhonneurAlcoolsTraiteur" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_alcool']=='vinDhonneurAlcoolsTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="vinDhonneurChampagne" class="form-control" value="vinDhonneurChampagneTraiteur" disabled="disabled"
                <?php if($organisations['organisation_vin_dhonneur_champagne']=='vinDhonneurChampagneTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerEau" class="form-control" value="dinerEauTraiteur" disabled="disabled"
                <?php if($organisations['organisation_diner_eau']=='dinerEauTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerSofts" class="form-control" value="dinerSoftsTraiteur" disabled="disabled"
                <?php if($organisations['organisation_diner_softs']=='dinerSoftsTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerVin" class="form-control" value="dinerVinTraiteur" disabled="disabled"
                <?php if($organisations['organisation_diner_vin']=='dinerVinTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="dinerAlcools" class="form-control" value="dinerAlcoolsTraiteur" disabled="disabled"
                <?php if($organisations['organisation_diner_alcools']=='dinerAlcoolsTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeEau" class="form-control" value="soireeEauTraiteur" disabled="disabled"
                <?php if($organisations['organisation_soiree_eau']=='soireeEauTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeSofts" class="form-control" value="soireeSoftsTraiteur" disabled="disabled"
                <?php if($organisations['organisation_soiree_softs']=='soireeSoftsTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeAlcools" class="form-control" value="soireeAlcoolsTraiteur" disabled="disabled"
                <?php if($organisations['organisation_soiree_alcool']=='soireeAlcoolsTraiteur') { echo 'checked="checked"';}?>></td>
    
                <td><input type="radio" name="soireeChampagne" class="form-control" value="soireeChampagneTraiteur" disabled="disabled"
                <?php if($organisations['organisation_soiree_champagne']=='soireeChampagneTraiteur') { echo 'checked="checked"';}?>></td>
    
            </tr>    
        </tbody>
    </table>

</div>

<div class="d-flex justify-content-around mb-5">

    <table class="tableau_fiche_client">
        <tbody>
        <tr>
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Musique</h4>
            </th>
        </tr>
        <tr>
            <th> Prestataires : </th>
            <td>
                <ul class="list-unstyled">
                    <li><?php if ($organisations['dj']=='DJ') { echo 'DJ';}
                else{ echo '';}?></li>
                    <li><?php if ($organisations['gospel']=='Gospel') { echo 'Gospel';}
                else{ echo '';}?></li>
                    <li><?php if ($organisations['orchestre']=='Orchestre') { echo 'Orchestre';}
                else{ echo '';}?></li>
                    <li><?php if ($organisations['chant']=='Chant') { echo 'Chant';}
                else{ echo '';}?></li>
                </ul>
            </td>
        </tr>
        <tr>
            <th> Style : </th>
            <td><?=$organisations['style'];?></td>
        </tr>
        </tbody>
    </table>
    
    <table class="tableau_fiche_client">
        <tbody>
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Photographe</h4>
            </th>
            <tr>
                <th> Formule souhaitée : </th>
                <td> <?=$organisations['organisation_photographe_formule'];?></td>
            </tr>
            <tr>
                <th> Style de photos : </th>
                <td> <?=$organisations['organisation_photographe_style'];?> </td>
            </tr>
            <tr>
                <th> Support remis : </th>
                <td> <?=$organisations['organisation_photographe_support'];?> </td>
            </tr>
        </tbody>
    </table>
    
    <table class="tableau_fiche_client">
        <tbody>
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center"> Vidéo </h4>
            </th>
            <tr>
                <th> Formule souhaitée : </th>
                <td> <?=$organisations['organisation_video_formule'];?></td>
            </tr>
            <tr>
                <th> Style de vidéo : </th>
                <td> <?=$organisations['organisation_video_style'];?> </td>
            </tr>
            <tr>
                <th> Format vidéo souhaitée : </th>
                <td> <?=$organisations['organisation_video_format'];?> </td>
            </tr>
            <tr>
                <th> Support remis : </th>
                <td> <?=$organisations['organisation_video_support'];?> </td>
            </tr>
        </tbody>
    </table>
    
    <table class="tableau_fiche_client">
        <tbody>
            <th colspan="2" class="text-center"><h4 class="bg-info text-white text-center"> Baby-Sitter </h4>
            </th>
            <tr>
                <th> Nombre d'enfants pris en charge : </th>
                <td> <?=$organisations['nombre_d_enfant'];?></td>
            </tr>
            <tr>
                <th> Plage horaire de prise en charge : </th>
                <td> <?=$organisations['horaire'];?> </td>
            </tr>
        </tbody>
    </table>
    
    <table class="tableau_fiche_client">
        <tbody>
            <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Divers</h4>
            </th>
            <tr>
                <th class="align-text-top"> Autres prestataires : </th>
                <td>
                    <ul class="list-unstyled">
                        <li><?=$organisations['coiffure'];?></li>
                        <li><?=$organisations['maquillage'];?></li>
                        <li><?php if ($organisations['officiant']=='officiant') { echo 'Officiant de cérémonie';}
                    else{ echo '';}?></li>
                        <li><?php if ($organisations['location_voiture']=='locationVoiture') { echo 'Location de voiture';}
                    else{ echo '';}?></li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th> Demandes particulières : </th>
                <td> <?=$organisations['autre'];?> </td>
            </tr>
        </tbody>
    </table>
    
</div>

    <div class="d-flex justify-content-center mb-5">
        <a href="ficheMaries.php?numMarie=<?=$organisations['maries_1_id']?>"><button class="btn btn-primary"> retour </button></a>
    </div>

</body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/ficheFleuristes_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche du fleuriste</title>
</head>

<body>

    <h1 class="text-center m-5"> <?=$Fleur['fleuriste_nom'];?> <?=$Fleur['fleuriste_prenom'];?> </h1>

    <div class="d-flex justify-content-around">

        <table class="d-flex justify-content-center">

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Coordonnées et déplacement</h4>
                </th>
                <tr>
                    <th> Adresse : </th>
                    <td> <?=$Fleur['fleuriste_adresse'];?> </td>
                </tr>
                <tr>
                    <th> Téléphone : </th>
                    <td> <?=$Fleur['fleuriste_telephone'];?> </td>
                </tr>
                <tr>
                    <th> E-mail : </th>
                    <td> <?=$Fleur['fleuriste_mail'];?> </td>
                </tr>
                <tr>
                    <th> Livraison : </th>
                    <td> <?=$Fleur['livraison_validation'];?> </td>
                </tr>
                <tr>
                    <th> Supplément de la livraison : </th>
                    <td> <?=$Fleur['livraison_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Prix au KM de la livraison : </th>
                    <td> <?=$Fleur['prix_au_km'];?> </td>
                </tr>
                <tr>
                    <th> Zone de livraison : </th>
                    <td> <?=$Fleur['livraison_zone'];?> </td>
                </tr>


            </tbody>

        </table>

        <table>

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Prestation</h4>
                </th>
                <tr>
                    <th> Installation : </th>
                    <td> <?=$Fleur['installation_validation'];?> </td>
                </tr>
                <tr>
                    <th> Prix de l'installation : </th>
                    <td> <?=$Fleur['installation_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Déinstallation : </th>
                    <td> <?=$Fleur['desinstallation_validation'];?> </td>
                </tr>
                <tr>
                    <th> Prix de la déinstallation : </th>
                    <td> <?=$Fleur['desinstallation_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Commande : </th>
                    <td> <?=$Fleur['fleuriste_commande'];?> </td>
                </tr>
                <tr>
                    <th> RDV pour les fleurs : </th>
                    <td> <?=$Fleur['fleuriste_rdv'];?> </td>
                </tr>
            </tbody>

        </table>

        <table>

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Décoration</h4>
                </th>
                <tr>
                    <th> Fleur de saison : </th>
                    <td> <?=$Fleur['fleuriste_saison'];?> </td>
                </tr>
                <tr>
                    <th> Choix des fleurs : </th>
                    <td> <?=$Fleur['fleuriste_choix'];?> </td>
                </tr>
                <tr>
                    <th> Test floral : </th>
                    <td> <?=$Fleur['fleuriste_test_floral'];?> </td>
                </tr>
                <tr>
                    <th> Location des fleurs : </th>
                    <td> <?=$Fleur['fleuriste_location'];?> </td>
                </tr>

                <tr>
                    <th> Décoration du plafond : </th>
                    <td> <?=$Fleur['decoration_plafond'];?> </td>
                </tr>
                <tr>
                    <th> Décoration de la voiture : </th>
                    <td> <?=$Fleur['decoration_voiture'];?> </td>
                </tr>
                <tr>
                    <th class="align-text-top"> Matériel fourni avec les fleurs : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li> <?php if ($Fleur['carterie']=='carterie') { echo 'Carterie';}
                        else{ echo '';} ?> </li>
                            <li> <?php if ($Fleur['nappage']=='nappage') { echo 'Nappage de couleur';}
                        else{ echo '';} ?> </li>
                            <li> <?php if ($Fleur['chemin']=='chemin') { echo 'Chemins de table';}
                        else{ echo '';} ?> </li>
                            <li> <?php if ($Fleur['bougies']=='bougies') { echo 'Bougies et photophores';}
                        else{ echo '';} ?> </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Autres decoration de fleur : </th>
                    <td> <?=$Fleur['fleuriste_autre_elements_decoration'];?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="d-flex justify-content-center mt-4">
        <a href="../edit/editFleuristes.php?numFleuriste=<?=$_GET['numFleuriste']?>"><input type="submit" name="submit" class="btn btn-primary mr-5" value="Modification"/></a>
        <a href="../../PHP/suppression/fichierSupFleuristes.php?numFleuriste=<?=$_GET['numFleuriste']?>"><input type="submit" name="submit" class="btn btn-primary" value="Suppression"/></a>
    </div>

</body>
</html>
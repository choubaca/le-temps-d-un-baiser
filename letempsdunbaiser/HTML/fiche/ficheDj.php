<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/ficheDJ_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche du DJ</title>
</head>

<body>


    <h1 class="text-center m-5"> <?=$DJ['dj_nom'];?> <?=$DJ['dj_prenom'];?> </h1>


    <div class="d-flex justify-content-around">

        <table>

            <tbody class="tableau_fiche_client">
            
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Coordonnées et déplacement</h4>
                </th>
                <tr>
                    <th> Téléphone : </th>
                    <td> <?=$DJ['dj_telephone'];?> </td>
                </tr>
                <tr>
                    <th> Mail : </th>
                    <td> <?=$DJ['dj_mail'];?> </td>
                </tr>
                <th> Déplacement : </th>
                <td> <?=$DJ['deplacement_validation'];?> </td>
                </tr>
                <tr>
                    <th> Supplément : </th>
                    <td> <?=$DJ['deplacement_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Prix au KM : </th>
                    <td> <?=$DJ['prix_au_km'];?> </td>
                </tr>
                <tr>
                    <th> Zone : </th>
                    <td> <?=$DJ['deplacement_zone'];?> </td>
                </tr>

            </tbody>

        </table>

        <table>

            <tbody class="tableau_fiche_client">
            
                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Prestation</h4>
                </th>    
                <tr>
                    <th> Animation du DJ : </th>
                    <td> <?=$DJ['animations_divers'];?> </td>
                </tr>
                <tr>
                    <th> Lesquelles : </th>
                    <td> <?=$DJ['quelle_animations'];?> </td>
                </tr>
                <tr>
                    <th> Heure limite : </th>
                    <td> <?=$DJ['dj_heure_limite'];?> </td>
                </tr>
                <tr>
                    <th> laquelle ? : </th>
                    <td> <?=$DJ['dj_laquelle'];?> </td>
                </tr>
                <tr>
                    <th> Style musical : </th>
                    <td> <?=$DJ['dj_style_musical'];?> </td>
                <tr>
                <tr>
                    <th> Met la musique demandé : </th>
                    <td> <?=$DJ['dj_musique_demande'];?> </td>
                </tr>
            </tbody>

        </table>

        <table>

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Matériels et infos divers</h4>
                </th>
                <tr>
                    <th class="align-text-top"> Matériels possédés : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li> <?php if ($DJ['dj_sono']=='sono') { echo 'Sono';}
                                else{ echo '';} ?> </li>
                            <li> <?php if ($DJ['dj_videoprojecteur']=='videoProjecteur') { echo 'Vidéo-projecteur';}
                                else{ echo '';} ?> </li>
                            <li> <?php if ($DJ['dj_eclairage']=='eclairage') { echo 'Éclairages';}
                                else{ echo '';} ?> </li>
                            <li> <?php if ($DJ['dj_machine_a_fumee']=='machineAFumee') { echo 'Machine à fumée';}
                                else{ echo '';} ?> </li>
                            <li> <?php if ($DJ['dj_micro']=='micro') { echo 'Micro';}
                                else{ echo '';} ?> </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Matériel de rechange : </th>
                    <td> <?=$DJ['dj_rechange'];?> </td>
                </tr>
                    <th> Travail seul : </th>
                    <td> <?=$DJ['dj_travail_seul'];?> </td>
                </tr>
                </tr>
                <tr>
                    <th> Style vestimentaire : </th>
                    <td> <?=$DJ['dj_habit'];?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div class="m-5">

        <table class="d-flex justify-content-center">

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Tarifs</h4>
                </th>
                <tr>
                    <th class="text-center"> Formules </th>
                    <th class="text-center"> Prix </th>
                </tr>
                <tr>
                    <td> Formule vin d’honneur + soirée </td>
                    <td> <?=$DJ['dj_tarif1'];?> </td>
                </tr>
                <tr>
                    <td> Option cérémonie laïque </td>
                    <td> <?=$DJ['dj_tarif2'];?> </td>
                </tr>
                <tr>
                    <td>Option photobooth ? </td>
                    <td> <?=$DJ['dj_tarif3'];?> </td>
                </tr>
                <tr>
                    <td> <?=$DJ['dj_option1'];?> </td>
                    <td> <?=$DJ['dj_tarif4'];?> </td>
                </tr>
                <tr>
                    <td> <?=$DJ['dj_option2'];?> </td>
                    <td> <?=$DJ['dj_tarif5'];?> </td>
                </tr>

            </tbody>

        </table>

    </div>

        <div class="d-flex justify-content-center mt-4">
            <a href="../edit/editDJ.php?numDJ=<?=$_GET['numDJ']?>""><input type="submit" name="submit" class="btn btn-primary mr-5" value="Modification"/></a>
            <a href="../../PHP/suppression/fichierSupDJ.php?numDJ=<?=$_GET['numDJ']?>"><input type="submit" name="submit" class="btn btn-primary" value="Suppression"/></a>
        </div>
</body>
</html>
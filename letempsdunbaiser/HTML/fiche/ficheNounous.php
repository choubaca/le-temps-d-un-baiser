<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/ficheNounous_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>fiche de la nounou</title>
</head>

<body>

    <h1 class="text-center m-5"> <?=$Nounou['nounou_nom'];?> <?=$Nounou['nounou_prenom'];?> </h1>

    <div class="d-flex justify-content-around">

        <table>

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Coordonnées et déplacement</h4>
                </th>
                <tr>
                    <th> Téléphone : </th>
                    <td> <?=$Nounou['nounou_telephone'];?> </td>
                </tr>
                <tr>
                    <th> E-mail : </th>
                    <td> <?=$Nounou['nounou_mail'];?> </td>
                </tr>
                <tr>
                    <th> Déplacement : </th>
                    <td> <?=$Nounou['deplacement_validation'];?> </td>
                </tr>
                <tr>
                    <th> Supplément : </th>
                    <td> <?=$Nounou['deplacement_supplement'];?> </td>
                </tr>
                <tr>
                    <th> Zone de deplacement : </th>
                    <td> <?=$Nounou['deplacement_zone'];?> </td>
                </tr>
                <tr>
                    <th> Prix au KM : </th>
                    <td> <?=$Nounou['prix_au_km'];?> </td>
                </tr>

            </tbody>

        </table>

        <table>

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Les enfants</h4>
                </th>
                <tr>
                    <th> Âge minimum de prise en charge : </th>
                    <td> <?=$Nounou['age_minimum'];?> </td>
                </tr>
                <tr>
                    <th> Âge maximum de prise en charge : </th>
                    <td> <?=$Nounou['age_maximum'];?> </td>
                </tr>
                <tr>
                    <th> Nombre d'enfants minimum : </th>
                    <td> <?=$Nounou['nombre_minimum'];?> </td>
                </tr>
                <tr>
                    <th> Nombre d'enfants maximum : </th>
                    <td> <?=$Nounou['nombre_maximum'];?> </td>
                </tr>
                <tr>
                    <th> Gére t-elle les enfants pendant le repas ? </th>
                    <td> <?=$Nounou['nounou_gere_enfant'];?> </td>
                </tr>

                <tr>
                    <th> À t-elle une heure limite ? </th>
                    <td> <?=$Nounou['nounou_heure_limite'];?> </td>
                </tr>
                <tr>
                    <th> Laquelle : </th>
                    <td> <?=$Nounou['nounou_laquelle'];?> </td>
                </tr>

            </tbody>

        </table>

        <table>

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Animateurs et animations</h4>
                </th>
                <tr>
                    <th> Nombre d'animateurs : </th>
                    <td> <?=$Nounou['nounou_nombre_animateur'];?> </td>
                </tr>
                <tr>
                    <th> Ont-ils des qualifications ? </th>
                    <td> <?=$Nounou['validation_qualification'];?> </td>
                </tr>
                <tr>
                    <th> Lesquelles : </th>
                    <td> <?=$Nounou['quelles_qualification'];?> </td>
                </tr>
                <tr>
                    <th> Fait-elle des animations ? </th>
                    <td> <?=$Nounou['animations_divers'];?> </td>
                </tr>
                <tr>
                    <th> Lesquelles : </th>
                    <td> <?=$Nounou['quelle_animations'];?> </td>
                </tr>
                <tr>
                    <th class="align-text-top"> Matériel dont elle aurais besoin : </th>
                    <td>
                        <ul class="list-unstyled">
                            <li> <?php if ($Nounou['tables']=='tables') { echo 'Tables';}
                        else{ echo '';} ?> </li>
                            <li> <?php if ($Nounou['chaise']=='chaise') { echo 'Chaises';}
                        else{ echo '';} ?> </li>
                            <li> <?php if ($Nounou['sono']=='sono') { echo 'Sonorisation';}
                        else{ echo '';} ?> </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th> Autres matériels : </th>
                    <td> <?=$Nounou['autre_besoin'];?> </td>
                </tr>

            </tbody>

        </table>

    </div>

    <div>

        <table class="d-flex justify-content-center mt-5">

            <tbody class="tableau_fiche_client">

                <th colspan="2" class="text-center"> <h4 class="bg-info text-white text-center">Tarifs</h4>
                </th>
                <tr>
                    <th class="text-center">Formules</th>
                    <th class="text-center">Prix</th>
                </tr>
                <tr>
                    <td> <?=$Nounou['nounou_formule1'];?> </td>
                    <td> <?=$Nounou['nounou_tarif1'];?> </td>
                </tr>
                <tr>
                    <td> <?=$Nounou['nounou_formule2'];?> </td>
                    <td> <?=$Nounou['nounou_tarif2'];?> </td>
                </tr>
                <tr>
                    <td> <?=$Nounou['nounou_formule3'];?> </td>
                    <td> <?=$Nounou['nounou_tarif3'];?> </td>
                </tr>
                <tr>
                    <td> <?=$Nounou['nounou_formule4'];?> </td>
                    <td> <?=$Nounou['nounou_tarif4'];?> </td>
                </tr>

            </tbody>

        </table>

        <table class="d-flex justify-content-center">

            <tbody class="tableau_fiche_client">

                <tr>
                    <th> Remarques divers : </th>
                    <td> <?=$Nounou['nounou_remarque'];?> </td>
                </tr>

            </tbody>
        </table>

    </div>

        <div class="d-flex justify-content-center mt-4">
            <a href="../edit/editNounous.php?numNounou=<?=$_GET['numNounou']?>"><button class="btn btn-primary"> Modification </button></a>
            <a class="ml-3" href="../../PHP/suppression/fichierSupNounous.php?numNounou=<?=$_GET['numNounou']?>"><button class="btn btn-primary"> Suppression </button></a>
        </div>

</body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter un vidéaste</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Ajout d'un videaste </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/sauvegarde/sauvegardeVideastes.php" method="POST" enctype="multipart/form-data" class="d-flex flex-column">
    
<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6">
                
            <h4 class="bg-info text-white text-center"> Coordonnées </h4>
                
                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomVideo" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomVideo" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telVideo" class="form-control" required >
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="mail" name="mailVideo" class="form-control" required>
                </div>
                    
        </fieldset>

        <fieldset class="mb-5 col-lg-4 col-md-6">
                
            <h4 class="bg-info text-white text-center"> Options </h4>
                    
                <h6 class="mt-3"> Un best of / bêtisier ? </h6>    
                    
                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="betisier" value="oui">
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="betisier" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                    
                <h6 class="mt-3"> Possibilité d’insérer des photos ? </h6>
                    
                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="inserePhoto" value="oui">
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="inserePhoto" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                    
                <h6 class="mt-3"> Générique de fin avec le nom des invités ? </h6>
                    
                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="generique" value="oui">
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="generique" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                    
                <h6 class="mt-3"> Interview des invités ? </h6>
                    
                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="interview" value="oui">
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="interview" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                    
        </fieldset>

        <fieldset class="mb-5 col-lg-4 col-md-6">
                
            <h4 class="bg-info text-white text-center"> Déplacement </h4>

                <h5> Vous déplacez-vous ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="deplacementVideo" value="oui">
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="zone" class="form-control ml-3" placeholder="Zone" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="deplacementVideo" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Avez-vous des frais de déplacement ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="supplementVideo" value="oui">
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="prixKMVideo" class="form-control ml-3" placeholder="Prix au KM" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="supplementVideo" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                    
        </fieldset>
            
    </div>

</div>
                
<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">
                
            <h4 class="bg-info text-white text-center"> Vidéos </h4>
            
                <fieldset class="mt-5 col-lg-4 col-md-6">

                    <h5>Formats remis</h5>
                    
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="supportUSB" value="USB">
                            <label class="form-check-label" for=""> USB </label>
                            <input class="form-check-input ml-2" type="checkbox" name="supportDVD" value="DVD">
                            <label class="form-check-label" for=""> DVD </label>
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label"> Combien de films sont remis ? </label>
                            <input type="text" name="combienFilm" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label"> Quelle sera leurs durées ? </label>
                            <input type="text" name="dureeFilm" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label"> Délai de livraison moyen : </label>
                            <input type="text" name="livraisonFilm" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </div>
                    
                </fieldset>

                <fieldset class="mt-5 col-lg-4 col-md-6">
                    
                        <div class="form-group">
                            <label for="" class="control-label"> Jusqu’à quelle heure filmez-vous la réception ? </label>
                            <input type="text" name="heureFilm" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label"> Combien y a-t-il de cameramen ? </label>
                            <input type="text" name="cameramen" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </div>
    
                    <h5>Possibilité de visionner le film avant <br> la fin du montage pour valider ?</h5>
    
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="visioFilm" value="oui">
                            <label class="form-check-label" for=""> Oui </label>
                            <input class="form-check-input ml-2" type="radio" name="visioFilm" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
    
                    <h5> Effets au montage ? </h5>
    
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="effetMontage" value="oui">
                            <label class="form-check-label" for=""> Oui </label>
                            <input class="form-check-input ml-2" type="radio" name="effetMontage" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label"> Remarques </label>
                            <input type="text" name="remarque" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </div>

                </fieldset>

        </fieldset>
            
    </div>

</div>
                
<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">
                
            <h4 class="bg-info text-white text-center"> Tarifs </h4>
                        
                <table>
                    <tr>
                        <td class="text-center"> Formules par moment </td>
                        <td class="text-center">Prix</td>
                    </tr>
                    <tr>
                        <td>De la mairie au vin d’honneur</td>
                        <td> <input type="text" name="tarif1" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                    </tr>
                    <tr>
                        <td>De la mairie à la soirée</td>
                        <td> <input type="text" name="tarif2" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                    </tr>
                    <tr>
                        <td>Des préparatifs au vin d’honneur</td>
                        <td> <input type="text" name="tarif3" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                    </tr>
                    <tr>
                        <td>Des préparatifs à la soirée </td>
                        <td> <input type="text" name="tarif4" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                    </tr>
                </table>
                
                <div class="form-group">
                    <label for="" class="control-label"> Autre formule </label>
                    <input type="text" name="autreFormuleMoment" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>
                            
        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">
                
            <table>
                <tr>
                    <td class="text-center">Formules par horaires</td>
                    <td class="text-center">Prix</td>
                </tr>
                <tr>
                    <td>6h de présence</td>
                    <td> <input type="text" name="tarif5" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                </tr>
                <tr>
                    <td>8h de présence</td>
                    <td> <input type="text" name="tarif6" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                </tr>
                <tr>
                    <td>12h de présence </td>
                    <td> <input type="text" name="tarif7" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                </tr>
            </table>
            
            <div class="form-group">
                <label for="" class="control-label"> Autre formule </label>
                <input type="text" name="autreFormuleHoraire" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
            </div>

        </fieldset>
            
    </div>
            
</div>   
        
            <div class="d-flex justify-content-center">
                <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
            </div>
            
</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
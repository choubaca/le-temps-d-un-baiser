<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/sauvegarde/ajoutPrestataires_post.php")

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter des prestataires</title>
</head>

<body>

<h1 class="text-center m-5"> Ajouter des prestataires </h1>

<form action="../../PHP/edit/updatePrestataires.php" method="POST" enctype="multipart/form-data">

    <input type="hidden" name="numMarie" value="<?=$_GET['numMarie']?>">
    
<div class="container">

    <div class="row justify-content-around">
    
            <div>
            
                <label for="djs">Djs</label>
            
                    <select name="djs" id="djs"> 
                            <?php foreach ($djs as $dj):?>
                            <option value="<?php echo $dj['id']?>"> <?php echo $dj['dj_nom'] ?></option>
                            <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="fleuristes">Fleuristes</label>
            
                    <select name="fleuristes" id="fleuristes"> 
                        <?php foreach ($fleuristes as $fleuriste):?>
                        <option value="<?php echo $fleuriste['id']?>"> <?php echo $fleuriste['fleuriste_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="lieux">Lieux</label>
            
                    <select name="lieux" id="lieux">
                        <?php foreach ($lieux as $lieu):?>
                        <option value="<?php echo $lieu['id']?>"> <?php echo $lieu['contact_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="nounous">Nounous</label>
            
                    <select name="nounous" id="nounous">
                        <?php foreach ($nounous as $nounou):?>
                        <option value="<?php echo $nounou['id']?>"> <?php echo $nounou['nounou_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
        
        
            <div>
            
                <label for="photographes">Photographes</label>
            
                    <select name="photographes" id="photographes">
                        <?php foreach ($photographes as $photographe):?>
                        <option value="<?php echo $photographe['id']?>"> <?php echo $photographe['photographe_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="traiteurs">Traiteurs</label>
            
                    <select name="traiteurs" id="traiteurs">
                        <?php foreach ($traiteurs as $traiteur):?>
                        <option value="<?php echo $traiteur['id']?>"> <?php echo $traiteur['traiteur_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="videastes">Videastes</label>
            
                    <select name="videastes" id="videastes">
                        <?php foreach ($videastes as $videaste):?>
                        <option value="<?php echo $videaste['id']?>"> <?php echo $videaste['videaste_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
    </div>

</div>
        <div class="d-flex justify-content-center m-5">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>
    </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>


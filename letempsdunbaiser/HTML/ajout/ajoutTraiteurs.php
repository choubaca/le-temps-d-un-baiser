<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter un traiteur</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Ajout d'un traiteur </h1>
    </div>

<form action="../../PHP/sauvegarde/sauvegardeTraiteurs.php" method="POST" enctype="multipart/form-data" class="d-flex flex-column">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6"> 

            <h4 class="bg-info text-white text-center"> Coordonnées </h4>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomTraiteur" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomTraiteur" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Adresse </label>
                    <input type="mail" name="adresseTraiteur" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telTraiteur" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="mail" name="mailTraiteur" class="form-control" required>
                </div>
            
        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Info divers </h4>

                <h5> Nombre d'invités </h5>

                    <div class="form-group d-flex">
                        <div>
                            <label for="" class="control-label"> Minimum (sans supplément) </label>
                            <input type="text" name="nbrMinInvite" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,25}">
                        </div>
                        <div class="ml-2">
                            <label for="" class="control-label"> Maximum </label>
                            <input type="mail" name="nbrMaxInvite" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,25}">
                        </div>
                    </div>

                <h5> Nombre de serveurs</h5>

                    <div class="form-group">
                        <input type="text" name="nbrServeurs" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,25}">
                    </div>

                <h5> Heure limite</h5>

                    <div class="form-group">
                        <input type="text" name="heureLimite" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                    </div>

                <h5> Tarif des heures supplémentaires</h5>

                    <div class="form-group">
                        <input type="text" name="tarifHeureSup" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                <h5> Déduction une fois le contrat signé : </h5>

                    <div class="form-group">
                        <input type="text" name="deduction" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                <h5> Combien de temps avant le mariage faut-il <br> fixer le choix du menu et le nombre d’invités :</h5>

                    <div class="form-group">
                        <input type="text" name="tempsAvantMariage" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}">
                    </div>
            
                <h5> Combien de temps arrivez-vous <br> avant le vin d'honneur ?</h5>

                    <div class="form-group">
                        <input type="text" name="delaiVinDhonnuer" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}">
                    </div>

                <h5> Combien de temps entre les plats ?</h5>

                    <div class="form-group">
                        <input type="text" name="delaiPlat" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

        </fieldset>
            
    </div>

</div>
                
<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h3 class="bg-info text-white text-center"> Cuisine </h3>
            
                <h5> Qualité des produits </h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="produitFrais" value="frais" required>
                        <label class="form-check-label" for=""> Frais </label>
                        <input class="form-check-input ml-2" type="radio" name="produitFrais" value="surgeles">
                        <label class="form-check-label" for=""> Surgelés </label>
                    </div>

                <h5> Produits principalement de saison </h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="produitSaison" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="produitSaison" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5>Tout est-il fait maison ? </h5>

                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="faitMaison" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                    </div>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="faitMaison" value="non">
                        <label class="form-check-label" for=""> Non </label>
                        <input type="text" name="autreFaitMaison" class="form-control ml-3" placeholder="Autres methodes" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                <h5> Style de cuisine</h5>

                    <div class="form-group">
                        <input type="text" name="styleCuisine" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                <h5> Les contraintes alimentaires </h5>

                    <div class="mb-2">
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="halal" value="halal">
                                <label for="" class="form-check-label"> Halal </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cacher" value="cacher">
                                <label for="" class="form-check-label"> Cacher </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="vegetarien" value="vegetarien">
                                <label for="" class="form-check-label"> Végétarien </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="sansgluten" value="sansgluten">
                                <label for="" class="form-check-label"> Sans gluten </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="vegan" value="vegan">
                                <label for="" class="form-check-label"> Végan </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="sanslactose" value="sanslactose">
                                <label for="" class="form-check-label"> Sans lactose </label>
                            </div>
                        </div>
                    </div>

                <h5> Comment sont fait les desserts ? </h5>

                    <div class="form-check-inline mb-2">
                        <input class="form-check-input" type="radio" name="dessertFait" value="vous" required>
                        <label class="form-check-label" for=""> Par vous </label>
                        <input class="form-check-input ml-2" type="radio" name="dessertFait" value="patissier">
                        <label class="form-check-label" for=""> Un pâtissier extérieur </label>
                    </div>

                <h5> Matière des bouteilles : </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="matiereBouteille" value="verre" required>
                        <label class="form-check-label" for=""> Verre </label>
                        <input class="form-check-input ml-2" type="radio" name="matiereBouteille" value="plastique">
                        <label class="form-check-label" for=""> Plastique </label>
                    </div>

                <h5> Y a-t-il un supplement pour avoir <br> des bouteilles en verre ? </h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="supplementBouteille" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="combienSup" class="form-control ml-3" placeholder="Combien?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="supplementBouteille" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Type de prestations </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="buffet" value="buffet">
                                <label for="" class="form-check-label"> Buffet </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="animCuli" value="animCuli">
                                <label for="" class="form-check-label"> Animations culinaires </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="serviceTable" value="service">
                                <label for="" class="form-check-label"> Service à table </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="brunch" value="brunch">
                                <label for="" class="form-check-label"> Brunch </label>
                            </div>
                        </div>
                    </div>

                <h5> Type de dessert </h5>

                    <div>
                        <div>
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" name="pieceMonteeChoux" value="pieceChoux">
                                <label for="" class="form-check-label"> Pièce montée de choux </label>
                            </div>
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" name="pieceMonteeMacarons" value="pieceMacaron">
                                <label for="" class="form-check-label"> Pièce montée de macarons </label>
                            </div>
                        </div>
                        <div>
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" name="weddingCake" value="wedding">
                                <label for="" class="form-check-label"> Wedding Cakes </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="autreDessert" class="form-control" placeholder="Autres types de pièces montées" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                <h5> Cocktail : </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="eauxCocktail" value="eauxCocktail">
                                <label for="" class="form-check-label"> Eaux </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="softsCocktail" value="softsCocktail">
                                <label for="" class="form-check-label"> Softs </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="alcoolsCocktail" value="alcoolsCocktail">
                                <label for="" class="form-check-label"> Alcools forts </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cocktails" value="cocktails">
                                <label for="" class="form-check-label"> Cocktails </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="champagneCocktail" value="champagneCocktail">
                                <label for="" class="form-check-label"> Champagne </label>
                            </div>
                        </div>
                    </div>

                <h5> Dîner : </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="eauxDiner" value="eauxDiner">
                                <label for="" class="form-check-label"> Eaux </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="softsDiner" value="softsDiner">
                                <label for="" class="form-check-label"> Softs </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="vinDiner" value="vinDiner">
                                <label for="" class="form-check-label"> Vins </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="ChampagneDiner" value="ChampagneDiner">
                                <label for="" class="form-check-label"> Champagne </label>
                            </div>
                        </div>
                    </div>

                <h5> Soirée : </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="eauxSoiree" value="eauxSoiree">
                                <label for="" class="form-check-label"> Eaux </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="softsSoiree" value="softsSoiree">
                                <label for="" class="form-check-label"> Softs </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="alcoolSoiree" value="alcoolSoiree">
                                <label for="" class="form-check-label"> Alcools forts </label>
                            </div>
                        </div>
                    </div>

        </fieldset>
            
    </div>
</div>
                
<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">      

            <h3 class="bg-info text-white text-center"> Organisation </h3>

                <h5> Comment est fait le service ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="service" value="verre" required>
                        <label class="form-check-label" for=""> Au verre </label>
                        <input class="form-check-input ml-2" type="radio" name="service" value="aTable">
                        <label class="form-check-label" for=""> Bouteille à table </label>
                    </div>

                <h5> Décoration du buffet incluse </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="decoBuffet" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="decoBuffet" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Choix de couleurs du chemin <br> de table du buffet ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="couleurChemin" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="couleurChemin" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
            
                <h5> Le nappage et les serviettes sont : </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="nappage" value="vous" required>
                        <label class="form-check-label" for=""> À vous </label>
                        <input class="form-check-input ml-2" type="radio" name="nappage" value="louee">
                        <label class="form-check-label" for=""> Louée </label>
                    </div>

                <h5> Y a-t-il possibilité de récupérer les nappages <br> et les serviettes à l’avance ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="recupnappe" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="recupnappe" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> La vaisselle est : </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="vaisselle" value="vous" required>
                        <label class="form-check-label" for=""> À vous </label>
                        <input class="form-check-input ml-2" type="radio" name="vaisselle" value="louee">
                        <label class="form-check-label" for=""> Louée </label>
                    </div>


                <h5> La vaisselle est-elle laissée sur place à votre départ ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="vaisselleSurPlace" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="vaisselleSurPlace" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Modalités de retour de la vaisselle : </h5>

                    <div class="form-group">
                        <input type="text" name="retourVaisselle" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                <h5> Apportez-vous le matériel nécessaire ?</h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="apportMateriel" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="apportMateriel" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
            
                <h5> Faites-vous une visite technique <br> avant le jour J ? </h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="visiteTech" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="visiteTech" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Faites-vous des dégustations ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="faireDegustation" value="oui" required>
                        <label class="form-check-label " for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="faireDegustation" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> La dégustation est : </h5>

                    <div class="mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="degustation" value="locaux" required>
                            <label for="" class="form-check-label"> Dans les locaux du prestataire </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="degustation" value="emporter">
                            <label for="" class="form-check-label"> À emporter </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="degustation" value="domicile">
                            <label for="" class="form-check-label"> Le prestataire se déplace à domicile </label>
                        </div>
                    </div>

        </fieldset>
            
    </div>

</div>
                
<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">
            
                <h5> Nombres de personnes max au repas test : </h5>

                    <div class="form-group">
                        <input type="text" name="persMaxTest" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,60}">
                    </div>

                <h5> Prix par personne : </h5>

                    <div class="form-group">
                        <input type="text" name="prixParPers" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,60}">
                    </div>

            <h5> Composition du repas test : </h5>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="cocktailTest" value="cocktail">
                        <label class="form-check-label" for=""> Cocktails </label>
                        <input type="text" name="combienCocktailTest" class="form-control" placeholder="Nombres de cocktails choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="entreeTest" value="entree">
                        <label class="form-check-label" for=""> Entrées </label>
                        <input type="text" name="combienEntreeTest" class="form-control" placeholder="Nombres d’entrées choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="platTest" value="plat">
                        <label class="form-check-label" for=""> Plats </label>
                        <input type="text" name="combienPlatTest" class="form-control" placeholder="Nombres de plats choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="dessertTest" value="dessert">
                        <label class="form-check-label" for=""> Desserts </label>
                        <input type="text" name="combienDessertTest" class="form-control" placeholder="Nombres de desserts choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}">
                    </div>

        </fieldset>
            
    </div>
</div>

    <div class="d-flex justify-content-center">
        <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
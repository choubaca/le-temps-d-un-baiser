<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      } 

    include("../affichage/menu.php");
    
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter un DJ</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Ajout d'un DJ </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/sauvegarde/sauvegardeDJ.php" method="POST" enctype="multipart/form-data">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Coordonnées </h4>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomDJ" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomDJ" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telDJ" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="email" name="mailDJ" class="form-control" required>
                </div>

        </fieldset>

        <fieldset class="mb-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Déplacement </h4>

                <h5> Vous déplacez-vous ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="deplacementDJ" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="zone" class="form-control ml-3" placeholder="Zone" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,70}">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="deplacementDJ" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                    <h5> Avez-vous des frais de déplacement ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="supplementDJ" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="prixKMDJ" class="form-control ml-3" placeholder="Prix au KM" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,70}">

                    </div>
                    <div class="form-check mb-2">
                    <input class="form-check-input" type="radio" name="supplementDJ" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Matériels </h4>

                <h5> Quels matériels avez-vous ? </h5>

                    <div class="d-flex justify-content-md-around">
                        <div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="sonoDJ" value="sono">
                                <label for="" class="form-check-label"> Sono </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="videoprojecteurDJ"
                                    value="videoProjecteur">
                                <label for="" class="form-check-label"> Vidéo-projecteur </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="eclairageDJ" value="eclairage">
                                <label for="" class="form-check-label"> Éclairages </label>
                            </div>
                        </div>
                        <div>
                            <div class="form-check ml-2">
                                <input class="form-check-input" type="checkbox" name="machineAFumeeDJ"
                                    value="machineAFumee">
                                <label for="" class="form-check-label"> Machine à fumée </label>
                            </div>
                            <div class="form-check ml-2">
                                <input class="form-check-input" type="checkbox" name="microDJ" value="micro">
                                <label for="" class="form-check-label"> Micro </label>
                            </div>
                        </div>
                    </div>

                <h5> Avez-vous du matériel de rechange ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="rechangeDJ" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="rechangeDJ" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>
            
    </div>

</div>
            
<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mt-5 col-lg-4 col-md-6">
            
            <h4 class="bg-info text-white text-center"> Musique </h4>
            
            <h5>Quel est votre style musical ?</h5>

                <div class="form-group">
                    <input type="text" name="styleMusical" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,120}">
                </div>


                <h5> Mettez-vous la musique demandée ? </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="musiqueDemande" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="musiqueDemande" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Faite-vous des animations ? </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="animationDJ" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input type="text" name="lesquels" class="form-control ml-3" placeholder="Lesquelles ?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,70}">
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="animationDJ" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Info divers </h4>

                <h5>Comment vous habillez-vous ?</h5>

                <div class="form-group">
                    <input type="text" name="habitDJ" class="form-control" required  pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}">
                </div>

                <h5> Travaillez-vous seul ? </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="seulDJ" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="seulDJ" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Avez-vous une heure limite ?</h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="heureDJ" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input type="text" name="heureLimiteDJ" class="form-control ml-3" placeholder="Heure limite ?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="heureDJ" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

            </fieldset>

            <fieldset class="mt-5 col-lg-4 col-md-6">
            
            <div class="container">
            
                <div class="row">
                
                    <h4 class="bg-info text-white text-center col-12"> Tarifs </h4>
                
                        <div class="col-12 m-1">
                            <div class="container">
                                <div class="row">
                                    <label class="col-lg-8">Formule vin d’honneur + soirée</label>
                                    <input class="form-control col-lg-4" type="text" name="tarif1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 m-1">
                            <div class="container">
                                <div class="row">
                                    <label class="col-lg-8">Option cérémonie laïque</label>
                                    <input class="form-control col-lg-4" type="text" name="tarif2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 m-1">
                            <div class="container">
                                <div class="row">
                                    <label class="col-lg-8">Option photobooth ? </label>
                                    <input class="form-control col-lg-4" type="text" name="tarif3" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 m-1">
                            <label class="pl-3">Option 1</label>
                            <div class="container">
                                <div class="row">
                                    <input class="form-control col-lg-6 mt-1" type="text" name="option1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                                    <input class="form-control col-lg-6 mt-1" type="text" name="tarif4" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                                </div>
                            </div>  
                        </div>
                        <div class="col-12 m-1">
                            <label class="pl-3">Option 2</label>
                            <div class="container">
                                <div class="row">
                                    <input class="form-control col-lg-6 mt-1" type="text" name="option2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                                    <input class="form-control col-lg-6 mt-1" type="text" name="tarif5" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}">
                                </div>
                            </div>
                        </div>

                </div>
                
            </div>

        </fieldset>

    </div>

</div>

    <div class="button-ajust d-flex justify-content-center mt-5">
        <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>


</form>
 
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
 
</body>
</html>

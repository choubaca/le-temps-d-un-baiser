<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter le premier marié</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Ajout du 1er marié.e </h1>
    </div>
    
<form class="d-flex flex-column" action="../../PHP/sauvegarde/sauvegardeMaries.php" method="POST" enctype="multipart/form-data">

    <div class="container">
    
        <div class="row justify-content-around">

            <fieldset>

                <h3 class="text-center"> 1er marié.e </h3>

                <div class="form-group">
                    <label for=""> Etat civil</label>
                    <input type="radio" name="sexe" value="madame" required> Madame
                    <input type="radio" name="sexe" value="monsieur"> Monsieur
                    <input type="radio" name="sexe" value="indefini"> Indéfini
                </div>

                <div class="form-group">
                    <label for="" class="control-label"required> Nom </label>
                    <input type="text" name="nom" class="form-control" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenom" class="form-control" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Profession </label>
                    <input type="text" name="profession" class="form-control" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Date de naissance </label>
                    <input type="date" name="naissance" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="tel" name="tel" class="form-control" required  pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="email" name="email" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Adresse </label>
                    <input type="text" name="adresse" class="form-control" required pattern="[a-zA-Zà-ÿèÀ-Û- ]0-9]{1,120}">
                </div>

            </fieldset>

            <div class="d-flex flex-column justify-content-around">

                <fieldset>

                    <h3 class="text-center"> 1er témoin </h3>

                    <div class="form-group">
                        <label for=""> État civil</label>
                        <input type="radio" name="sexeTemoin1" value="madame" required> Madame
                        <input type="radio" name="sexeTemoin1" value="monsieur"> Monsieur
                        <input type="radio" name="sexeTemoin1" value="indefini"> Indéfini
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Nom </label>
                        <input type="text" name="nomTemoin1" class="form-control" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Prénom </label>
                        <input type="text" name="prenomTemoin1" class="form-control" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Téléphone </label>
                        <input type="text" name="telTemoin1" class="form-control" required  pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                    </div>

                </fieldset>

                    <input type="button" id='bouton' class="btn btn-info" value="Ajouter un second temoins" onclick="afficher();">

                <fieldset class="d-none" id='temoin2'>

                    <h3 class="text-center"> 2nd témoin </h3>

                    <div class="form-group">
                        <label for=""> Etat civil</label>
                        <input id="madame" class="sexeTemoin2 requis" type="radio" name="sexeTemoin2" value="madame"> Madame
                        <input id="monsieur" class="sexeTemoin2 requis" type="radio" name="sexeTemoin2" value="monsieur"> Monsieur
                        <input id="indefini" class="sexeTemoin2 requis" type="radio" name="sexeTemoin2" value="indefini"> Indéfini
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Nom </label>
                        <input id="nomTemoin2" type="text" name="nomTemoin2" class="form-control requis" pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Prénom </label>
                        <input id="prenomTemoin2" type="text" name="prenomTemoin2" class="form-control requis" pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Téléphone </label>
                        <input id="telTemoin2" type="text" name="telTemoin2" class="form-control requis"  pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                    </div>

                </fieldset>
                
                <input type="button" id='boutoncacher' class="d-none" value="Enlever" onclick="cacher();">

            </div>
            
        </div>
    
    </div>            
    
        <div class="d-flex justify-content-center m-5">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>

</form>
        

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="../../JS/main.js"></script>

</body>
</html>
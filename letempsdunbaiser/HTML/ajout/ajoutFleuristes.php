<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter un fleuriste</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Ajout d'un fleuriste </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/sauvegarde/sauvegardeFleuristes.php" method="POST" enctype="multipart/form-data">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Coordonnées </h4>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomFleur" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomFleur" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telFleur" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="text" name="mailFleur" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Adresse </label>
                    <input type="text" name="adresseFleur" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,120}">
                </div>

        </fieldset>

        <fieldset class="mb-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Livraison </h4>

                <h5> Livrez-vous ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="livraison" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="zoneLivrai" class="form-control ml-3" placeholder="Zone de livraison" pattern="[0-9a-zA-Zà-ÿÀ-Û- ',]{1,50}">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="livraison" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                    <h5> Avez-vous des frais de déplacement ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="supplementLivrai" value="oui" required>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="PrixAuxKM" class="form-control ml-3" placeholder="Prix au KM" pattern="[0-9a-zA-Zà-ÿÀ-Û- ',]{1,50}">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="supplementLivrai" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Installation </h4>

                <h5> Faite-vous l'installation ? </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="instal" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input type="text" name="prixInstal" class="form-control ml-3" placeholder="Prix de l'installation" pattern="[0-9a-zA-Zà-ÿÀ-Û- ',]{1,50}">
                </div>
                <div class="form-check mb-2">
                    <input class="form-check-input" type="radio" name="instal" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Faite-vous la désinstallation ? </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="desinstal" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input type="text" name="prixDesinstal" class="form-control ml-3" placeholder="Prix de la désintallation" pattern="[0-9a-zA-Zà-ÿÀ-Û- ',]{1,50}">
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="desinstal" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

        </fieldset>
            
    </div>
</div>
                
<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Fleurs </h4>

                <h5> Utilisez-vous des fleurs de saison </h5>

                <div class="form-check form-check-inline mb-2">
                    <input class="form-check-input" type="radio" name="fleurSaison" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="fleurSaison" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Choix des fleurs utilisées </h5>

                <div class="form-check form-check-inline mb-2">
                    <input class="form-check-input" type="radio" name="choixFleur" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="choixFleur" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Faite-vous des tests floral ? </h5>

                <div class="form-check form-check-inline mb-2">
                    <input class="form-check-input" type="radio" name="testFloral" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="testFloral" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Matériels </h4>

                <h5> Proposez-vous de la location de matériel ? </h5>

                <div class="form-check form-check-inline mb-2">
                    <input class="form-check-input" type="radio" name="locationFleur" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="locationFleur" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Quels matériels pouvez-vous fournir ? </h5>

                <div class="d-flex justify-content-md-around">
                    <div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="carterieFleur" value="carterie">
                            <label for="" class="form-check-label"> Carterie </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="nappageFleur" value="nappage">
                            <label for="" class="form-check-label"> Nappage de couleur </label>
                        </div>
                    </div>
                    <div>
                        <div class="form-check ml-2">
                            <input class="form-check-input" type="checkbox" name="cheminFleur" value="chemin">
                            <label for="" class="form-check-label"> Chemins de table </label>
                        </div>
                        <div class="form-check ml-2">
                            <input class="form-check-input" type="checkbox" name="bougiesFleur" value="bougies">
                            <label for="" class="form-check-label"> Bougies et photophores </label>
                        </div>
                    </div>
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Décoration </h4>

                <h5> Faite-vous la décoration du plafond ? </h5>

                <div class="form-check form-check-inline mb-2">
                    <input class="form-check-input" type="radio" name="decoPlafondFleur" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="decoPlafondFleur" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Faite-vous la décoration de la voiture ? </h5>

                <div class="form-check form-check-inline mb-2">
                    <input class="form-check-input" type="radio" name="decoVoitureFleur" value="oui" required>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="decoVoitureFleur" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Info divers </h4>

                <h5> À quelle date fixer la commande et les quantités ? </h5>

                <div class="form-group">
                    <input type="text" name="commande" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ',]{1,120}">
                </div>

                <h5> Combien de RDV vous faut-il ? </h5>

                <div class="form-group">
                    <input type="text" name="rdvFleur" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ',]{1,50}">
                </div>

                <h5> Autres </h5>

                <div class="form-group">
                    <input type="text" name="autreLocationFleur" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ',]{1,120}">
                </div>

        </fieldset>

    </div>

</div>

        <div class="d-flex justify-content-center">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
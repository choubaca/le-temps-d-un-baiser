<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }
    
    require("../../PHP/sauvegarde/sauvegardePhotos.php");
    if ($validation == true) header('refresh: 2 ; url=../../HTML/affichage/galerie.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter des photos</title>
</head>
<body>

<form enctype="multipart/form-data" action="ajoutPhotos.php" method="post">

    <div class="d-flex flex-column">

        <h4 class="font-weight-bold text-center m-3">Ajout photo de galerie</h4>
        
        <h6 class="font-weight-bold text-center m-3"> 22 photos maximum</h6>

            <div class="d-flex justify-content-center m-3">
            
                    <input type="hidden" name="numGalerie" value="<?=$_GET['numGalerie']?>">

                    <input type="file" name="photogalerie[]" multiple required>

            </div>
            
        <div class="d-flex justify-content-center m-5">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>

    </div>
    
    <div id="erreur" class="text-center">

        <?php
        
            if (!empty($erreurExtention)){
            
                    echo '<p class="text-danger">'.$erreurExtention.'</p>';
                }
            if (!empty($erreurUpload)){
            
                    echo '<p class="text-danger">'.$erreurUpload.'</p>';
                }
            if (!empty($valeurInserer)){
            
                    echo '<p class="text-success">'.$message.'</p>';
                }
        ?>
    </div>

</form>

<script src="../../JS/main.js"></script>

</body>
</html>
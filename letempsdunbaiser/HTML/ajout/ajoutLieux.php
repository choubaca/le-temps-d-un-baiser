<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter un lieu</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Ajout d'un lieux </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/sauvegarde/sauvegardeLieux.php" method="POST" enctype="multipart/form-data" class="d-flex flex-column">
    
<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-4 taille col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center mb-4"> Coordonnées </h4>
        
                <div class="form-group">
                    <input type="text" name="nomLieu" class="form-control" placeholder="Nom du lieu" required pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="form-group">
                    <input type="text" name="typeLieu" class="form-control" placeholder="Type de lieu" required pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="form-group">
                    <input type="text" name="ville" class="form-control" placeholder="Ville" required pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="distance" class="form-control" placeholder="Distance depuis la ville" required pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="tempsTrajet" class="form-control" placeholder="Temps de trajet" required pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>
        
        </fieldset>

        <fieldset class="col-lg-4 col-md-6">
        
            <div class="d-flex align-items-center form-group">
                <input type="text" name="nomContact" class="form-control" placeholder="Nom du contact" required pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
            </div>

            <div class="form-group">
                <input type="text" name="telContact" class="form-control" placeholder="Telephone du contact" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
            </div>

            <div class="form-group">
                <input type="text" name="mailContact" class="form-control" required placeholder="Mail du contact">
            </div>
        
        </fieldset>
            
    </div>

</div>
            
<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mt-5 col-lg-4 col-md-6">
    
            <h4 class="mb-4 bg-info text-white text-center"> La salle principale </h4>
    
                <div class="form-group">
                    <input type="text" name="longueurSalleP" class="form-control" placeholder="Longueur de la salle" required pattern="[0-9]{1,3}">
                </div>

                <div class="form-group">
                    <input type="text" name="largeurSalleP" class="form-control" placeholder="Largeur de la salle" required pattern="[0-9]{1,3}">
                </div>

                <div class="form-group">
                    <input type="text" name="nbMaxSalleP" class="form-control" placeholder="Nombre de personne max" required pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="d-flex justify-content-between">
                    <div class="d-flex flex-column mb-2">
                        <label class="form-check-label" for=""> Y a-t-il une séparation ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sepSalleP" value="oui" required>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="sepSalleP" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>

                    <div class="d-flex flex-column ml-2">
                        <label class="form-check-label" for=""> Est-elle chauffée ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sallePChauffe" value="oui" required>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="sallePChauffe" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <div class="d-flex flex-column">
                        <label class="form-check-label" for=""> Est-elle cimatisée ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sallePClimati" value="oui" required>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="sallePClimati" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>

                    <div class="d-flex flex-column">
                        <label class="form-check-label" for=""> Le vin d'honneur <br> est-il à l'exterieur ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="vinExter" value="oui" required>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="vinExter" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>
                </div>
    
                <div class="d-flex flex-column text-center mt-4">
                    <label class="form-check-label" for="">Avez-vous un Plan B si mauvais temps ?</label>
                    <div class="form-check form-check-center">
                        <input class="form-check-input" type="radio" name="planB" value="oui" required>
                        <label class="form-check-label mr-5" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="planB" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

        </fieldset>
            
    </div>
        
</div>
                    
<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="mt-5 col-lg-4 col-md-6">
    
            <h4 class="bg-info text-white text-center"> La salle numéro 2 </h4>
    
                <div class="form-group">
                    <input type="text" name="longueurSalle2" class="form-control" placeholder="Longeur de la salle" pattern="[0-9 *]{1,3}">
                </div>

                <div class="form-group">
                    <input type="text" name="largeurSalle2" class="form-control" placeholder="Largeur de la salle" pattern="[0-9 *]{1,3}">
                </div>

                <div class="form-group">
                    <input type="text" name="nbMaxSalle2" class="form-control" placeholder="Nombre de personne max" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="form-group">
                    <input type="text" name="utilSalle2" class="form-control" placeholder="Utilisation de la salle" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="d-flex justify-content-between">
                    <div class="d-flex flex-column mb-2">
                        <label class="form-check-label" for=""> Y a-t-il une séparation ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sepSalle2" value="oui">
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="sepSalle2" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>

                    <div class="d-flex flex-column ml-2">
                        <label class="form-check-label" for=""> Est-elle chauffée ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="salle2Chauffe" value="oui">
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="salle2Chauffe" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-column">
                    <label class="form-check-label" for=""> Est-elle cimatisée ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="salle2Climati" value="oui">
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="salle2Climati" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>
    
        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">
    
            <h4 class="mb-4 bg-info text-white text-center"> La salle numéro 3 </h4>
    
                <div class="form-group">
                    <input type="text" name="longueurSalle3" class="form-control" placeholder="Longeur de la salle" pattern="[0-9 *]{1,3}">
                </div>

                <div class="form-group">
                    <input type="text" name="largeurSalle3" class="form-control" placeholder="Largeur de la salle" pattern="[0-9 *]{1,3}">
                </div>

                <div class="form-group">
                    <input type="text" name="nbMaxSalle3" class="form-control" placeholder="Nombre de personne max" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="form-group">
                    <input type="text" name="utilSalle3" class="form-control" placeholder="Utilisation de la salle" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="d-flex justify-content-between">
                    <div class="d-flex flex-column mb-2">
                        <label class="form-check-label" for="">Y a-t-il une séparation ?</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sepSalle3" value="oui">
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="sepSalle3" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>

                    <div class="d-flex flex-column">
                        <label class="form-check-label ml-2" for="">Est-elle chauffée ?</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="salle3Chauffe" value="oui">
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="salle3Chauffe" value="non">
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-column">
                    <label class="form-check-label" for="">Est-elle cimatisée ?</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="salle3Climati" value="oui">
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="salle3Climati" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">
    
            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Terrasse</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="terrasse" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="terrasseSurf" class="form-control" placeholder="Surface" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ²]{1,10}">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="terrasse" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Jardin</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="jardin" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="jardinSurf" class="form-control" placeholder="Surface" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ²]{1,10}">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="jardin" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>
            
            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Parc</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="parc" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="parcSurf" class="form-control" placeholder="Surface" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ²]{1,10}">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="parc" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>
    
        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Parking</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="parking" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="placeParking" class="form-control" placeholder="Nombre de places" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="parking" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Vestiaire</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="vestiaire" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="vestiaire" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="mb-2">
                <input type="text" name="WcHomme" class="form-control mb-2" placeholder="Nombre WC homme" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                <input type="text" name="WcFemme" class="form-control" placeholder="Nombre WC femme" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label" for=""> Y a-t-il un acces aux PMR ?</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="PMR" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input class="form-check-input" type="radio" name="PMR" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>
    
        </fieldset>
            
    </div>
        
</div>
                    
<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="mt-5 col-lg-4 col-md-6">
    
            <h4 class="mb-4 bg-info text-white text-center"> Matériel mis à disposition </h4>
    
                <div class="d-flex justify-content-around mb-5">
                    <div class="form-check">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuRetropro" value="lieuRetropro">
                            <label for="" class="form-check-label">Rétroprojecteur</label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuEcran" value="lieuEcran">
                            <label for="" class="form-check-label"> Écran </label>
                        </div>
                    </div>
                    <div class="form-check">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuAmpli" value="lieuAmpli">
                            <label for="" class="form-check-label"> Ampli </label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuEnceinte" value="lieuEnceinte">
                            <label for="" class="form-check-label"> Enceinte </label>
                        </div>
                    </div>
                </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

                <table class="table table-responsive-md table-hover mb-5">

                    <tbody class="table-bordered">

                        <tr>
                            <td></td>
                            <td class="text-center"> Dimensions </td>
                            <td class="text-center"> Nombres de personnes </td>
                            <td class="text-center"> Matière </td>
                            <td class="text-center"> Quantité </td>
                        </tr>
                        <tr>
                            <td> Table ronde </td>
                            <td> <input type="text" name="dimTableRonde" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="nbrPersTableRonde" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="matiereTableRonde" class="form-control" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="nbrTableRonde" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                        </tr>
                        <tr>
                            <td> Table rectangulaire </td>
                            <td> <input type="text" name="dimTableRect" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="nbrPersTableRect" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="matiereTableRect" class="form-control" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="nbrTableRect" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                        </tr>
                        <tr>
                            <td> Buffet </td>
                            <td> <input type="text" name="dimBuffet" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="nbrPersBuffet" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="matiereBuffet" class="form-control" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="nbrBuffet" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                        </tr>
                        <tr>
                            <td colspan="5"> <input type="text" name="autreTable" class="form-control" placeholder="Autres" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                            </td>
                        </tr>

                    </tbody>

                </table>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

                <table class="table table-responsive-md table-hover mb-3">

                    <tbody class="table-bordered">

                        <tr>
                            <td></td>
                            <td class="text-center"> Type </td>
                            <td class="text-center"> Matière </td>
                            <td class="text-center"> Quantité </td>
                        </tr>
                        <tr>
                            <td> Chaises interieures </td>
                            <td> <input type="text" name="TypeChaiseInt" class="form-control" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="matiereChaiseInt" class="form-control" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="qtsChaiseInt" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                        </tr>
                        <tr>
                            <td> Chaises extérieures </td>
                            <td> <input type="text" name="TypeChaiseExt" class="form-control" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="matiereChaiseExt" class="form-control" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                            <td> <input type="text" name="qtsChaiseExt" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"></td>
                        </tr>
                        <tr>
                            <td colspan="4"> <input type="text" name="autreChaise" class="form-control" placeholder="Autres" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                            </td>
                        </tr>

                    </tbody>

                </table>

                <div class="d-flex justify-content-center">

                    <div class="d-flex flex-column mb-2">
                        <label class="form-check-label text-center" for="">Le mobilier est-il compris dans le tarif de la location ?</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="mobilierCompris" value="oui" required>
                            <label class="form-check-label" for=""> Oui </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="mobilierCompris" value="non">
                            <label class="form-check-label mr-2" for=""> Non </label>
                            <input type="text" name="autreMobilier" class="form-control" placeholder="Prix de la location" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </div>
                    </div>

                </div>
    
        </fieldset>
            
    </div>
        
</div>
                    
<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="col-lg-4 col-md-6 mt-5">
    
            <h4 class="bg-info text-white text-center"> Tarifs </h4>
    
                <table class="table table-responsive-md table-hover mb-5">

                    <tbody class="table-bordered">

                        <tr>
                            <td colspan="5" class="text-center">
                                <h4> Formules </h4>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <h5> Jour et heure </h5>
                            </td>
                            <td class="text-center">
                                <h5> Jour et heure </h5>
                            </td>
                            <td colspan="3" class="text-center">
                                <h5> Tarif / Saison </h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> </td>
                            <td class="text-center">
                                <h6> Haute </h6>
                            </td>
                            <td class="text-center">
                                <h6> Moyenne </h6>
                            </td>
                            <td class="text-center">
                                <h6> Basse </h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"> Du <br> <input type="date" class="form-control" name="date1"> <br>
                                à partir de <br> <input type="text" class="form-control" name="heure1" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td class="text-center"> Au <br> <input type="date" class="form-control" name="date2"> <br>
                                à partir de <br> <input type="text" class="form-control" name="heure2" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifHaute1" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifMoy1" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifBas1" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                        </tr>
                        <tr>
                            <td class="text-center"> Du <br> <input type="date" class="form-control" name="date3"> <br>
                                à partir de <br> <input type="text" class="form-control" name="heure3" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td class="text-center"> Au <br> <input type="date" class="form-control" name="date4"> <br>
                                à partir de <br> <input type="text" class="form-control" name="heure4" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifHaute2" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifMoy2" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifBas2" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                        </tr>
                        <tr>
                            <td class="text-center"> Du <br> <input type="date" class="form-control" name="date5"> <br>
                                à partir de <br> <input type="text" class="form-control" name="heure5" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td class="text-center"> Au <br> <input type="date" class="form-control" name="date6"> <br>
                                à partir de <br> <input type="text" class="form-control" name="heure6" pattern="[a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifHaute3" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifMoy3" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                            <td> <input type="text" name="tarifBas3" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> </td>
                        </tr>

                    </tbody>

                </table>
    
        </fieldset>
            
    </div>
        
</div>
                    
<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="col-lg-4 col-md-6">

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label" for="">Le nettoyage est-il inclue ?</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nettoyageInclue" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="tarifNettoie" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="nettoyageInclue" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label" for="">La restauration est-elle imposée ? </label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="restoImposer" value="oui" required>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="tarifResto" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="restoImposer" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Hébergements sur place : </h4>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Y a-t-il un hébergement sur place ?</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="hebergSurPlace" value="oui" required>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="hebergSurPlace" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="form-group">
                    <input type="text" name="combienChambre" class="form-control" placeholder="Combien y a-t-il de chambre ?" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div>
                    <p> La capacité total de l'hébergement est de <input type="text" name="capaTotal" class="form-control" placeholder="Capacité totale" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> personnes. </p>
                </div>

                <div>
                    <p> Le prix par chambre est compris entre </p>
                    <p class="d-flex"> 
                        <fieldset class="col-lg-4 col-md-6">
                            <input type="text" name="prixChambre1" class="form-control" placeholder="Prix bas" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                        </fieldset>
                            et
                        <fieldset class="col-lg-4 col-md-6">
                            <input type="text" name="prixChambre2" class="form-control" placeholder="Prix haut" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}"> 
                        </fieldset>
                    </p>
                </div>


                <div class="form-group">
                    <label for=""> Forfait de location de la totalité des hébergements : </label>
                    <input type="text" name="forfaitLocation" class="form-control" placeholder="Forfait location" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="form-group">
                    <label for=""> Forfait location salle + hébergement : </label>
                    <input type="text" name="forfaitLocHerb" class="form-control" placeholder="Forfait location et hébergement" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Questions Diverses : </h4>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Y a-t-il une salle pour coucher les enfants ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="salleOccupEnfant" value="oui" required>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="salleOccupEnfant" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Y a-t-il une salle pour occuper les enfants ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="SalleDormirEnfant" value="oui" required>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="SalleDormirEnfant" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Un seul mariage/week end ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="mariageSolo" value="oui" required>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="mariageSolo" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for=""> Accompte à verser pour réservation : </label>
                    <input type="text" name="accompte" class="form-control" placeholder="accompte" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
                </div>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Avez-vous l’habitude de travailler <br> avec des Wedding
                        Planners ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="travailWP" value="oui" required>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="travailWP" value="non">
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

        </fieldset>
            
    </div>
        
</div>
                    
<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="mt-4 col-lg-4 col-md-6">

            <div class="form-group">
                <input type="text" name="remarque" class="form-control" placeholder="Remarques" pattern="[0-9a-zA-Zà-ÿÀ-Û-' ]{1,35}">
            </div>

        </fieldset>

    </div>
</div>

        <div class="d-flex justify-content-center">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
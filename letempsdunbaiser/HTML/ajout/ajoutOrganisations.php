<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Ajouter une organisation</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Ajout d'une organisation </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/sauvegarde/sauvegardeOrganisations.php?numMarie=<?= $_GET['numMarie']?>" method="POST" enctype="multipart/form-data">

    <h3 class="bg-info text-white text-center m-5"> Informations générales</h3>
        
        
<div class="container">

    <div class="row justify-content-around">
    
        <fieldset class="col-md-3">
    
            <h5>Informations divers</h5>
    
                <div class="form-group">
                    <input type="date" name="dateDuMariage" class="form-control" required>
                </div>
    
                <div class="form-group">
                    <input type="text" name="lieuCeremonie" class="form-control" placeholder="Lieu du mariage" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>
    
                <div class="form-group">
                    <input type="text" name="nbrAdulte" class="form-control" placeholder="Nombre d'adulte" pattern="[0-9]{1,3}">
                </div>
    
                <div class="form-group">
                    <input type="text" name="nbrEnfant" class="form-control" placeholder="Nombre d'enfant" pattern="[0-9]{1,3}">
                </div>
                
        </fieldset>
        
        <fieldset class="col-md-3">
        
            <h5>Budget</h5>
        
                <div class="form-group">
                    <input type="text" name="budgetReception" class="form-control" placeholder="Budget du mariage" pattern="[0-9a-zA-Z- ]{1,15}">
                </div>
    
            <h5>Lieu des cérémonies</h5>
    
                <div class="d-flex align-items-center form-group">
                    <input type="text" name="mairie" class="form-control" placeholder="Mairie" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>
    
                <div class="d-flex align-items-center form-group">
                    <input type="text" name="Ceremoniereligieuse" class="form-control" placeholder="Cérémonie religieuse" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>
    
                <div class="d-flex align-items-center form-group">
                    <input type="text" name="Ceremonielaïque" class="form-control" placeholder="Cérémonie laïque" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>
                
        </fieldset>
        
        <fieldset class="col-md-3">
    
            <h6> Timing </h6>
    
                <h5>La cérémonie civile a lieu :</h5>
    
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eventCeremonie" value="Lememejour">
                    <label class="form-check-label" for=""> Le même jour </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eventCeremonie" value="Aunedateanterieur">
                    <label class="form-check-label" for=""> À une date antérieur </label>
                </div>
        
            <h6>La réception</h6>
    
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eventReception" value="leMidi">
                    <label class="form-check-label" for=""> Le midi </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eventReception" value="leSoir">
                    <label class="form-check-label" for=""> Le soir </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="eventReception" value="surPlusieursJours">
                    <label class="form-check-label" for=""> Sur plusieurs jours </label>
                </div>
                
        </fieldset>
        
    </div>
    
</div>

<div class="container">

    <div class="row justify-content-around">
    
        <fieldset class="col-md-3">
    
            <h5> Types de lieu</h5>

                <div class="d-flex justify-content-around">
                
                    <div>
                    
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Chateau">
                            <label class="form-check-label" for=""> Château </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Ferme">
                            <label class="form-check-label" for=""> Ferme </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="corpsDeFerme">
                            <label class="form-check-label" for=""> Corps de ferme </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Domaine">
                            <label class="form-check-label" for=""> Domaine </label>
                        </div>

                    </div>

                    <div>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Bastide">
                            <label class="form-check-label" for=""> Bastide </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Villa">
                            <label class="form-check-label" for=""> Villa </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Orangerie">
                            <label class="form-check-label" for=""> Orangerie </label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="autreTypeDeLieu" class="form-control" placeholder="Autre type de lieu" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                        </div>

                    </div>
                    
                </div>

                <div class="form-group">
                    <input type="text" name="localisationGeo" class="form-control" placeholder="localisation géographique" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>
                        
        </fieldset>
        
        <fieldset class="col-md-3">

            <h5> Critéres </h5>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="espaceExt" value="espaceExt">
                    <label for="" class="form-check-label"> Espace extérieur </label>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="piscine" value="piscine">
                    <label for="" class="form-check-label"> Piscine </label>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="chambreMaries" value="chambreMaries">
                    <label for="" class="form-check-label"> Chambre des mariés </label>
                </div>
                        
        </fieldset>
        
        <fieldset class="col-md-3">
        
            <h5> Hébergement sur place </h5>
    
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="hebergementSurPlace" value="oui">
                    <label class="form-check-label" for=""> Oui </label>
                </div>
                
                <div class="form-group form-check-inline">
                    <input type="text" name="combienHebergement" class="form-control" placeholder="Combien d'hébergement ?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}">
                </div>
    
                <div class="form-check">
                    <input type="radio" name="hebergementSurPlace" class="form-check-input" value="non">
                    <label for="" class="form-check-label"> Non </label>
                </div>
    
                <div class="form-group">
                    <input type="text" name="autreHebergement" class="form-control" placeholder="Autres hébergements" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>
            
        </fieldset>
        
    </div>
    
</div>

    <h3 class="bg-info text-white text-center m-5"> La restauration </h3>
            
<div class="container">

    <div class="row justify-content-around">
    
        <fieldset class="col-md-3">

            <h5>Nombre de convives</h5>

                <div class="form-group">
                    <input type="text" name="convivesVinDhonneur" class="form-control" placeholder="Au vin d'honneur" pattern="[0-9]{1,3}">
                    <input type="text" name="convivesAuDiner" class="form-control" placeholder="Au dîner" pattern="[0-9]{1,3}">
                    <input type="text" name="convivesAuBrunch" class="form-control" placeholder="Au brunch" pattern="[0-9]{1,3}">
                </div>
                    
        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5>Type de dîner</h5>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="TypeDeDiner" value="dinerAssis">
                    <label class="form-check-label" for=""> Dîner assis </label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="TypeDeDiner" value="Buffet">
                    <label class="form-check-label" for=""> Buffet </label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="TypeDeDiner" value="cocktail">
                    <label class="form-check-label" for=""> Cocktail dinatoire </label>
                </div>
                    
        </fieldset>
                         
        <fieldset class="col-md-3">
    
            <h5> Type de dessert souhaité : </h5>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="pieceMonteeChoux" value="pieceMonteeChoux">
                    <label for="" class="form-check-label"> Pièce montée de choux </label>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="weddingCake" value="weddingCake">
                    <label for="" class="form-check-label"> Wedding cake </label>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="pieceMonteeMacaron"
                        value="pieceMonteeMacaron">
                    <label for="" class="form-check-label"> Pièce montée de macaron </label>
                </div>
                
                <div class="form-group">
                    <input type="text" name="autreDessert" class="form-control" placeholder="Autre dessert" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>
                    
        </fieldset>
        
    </div>
    
</div>

<div class="container">

    <div class="row justify-content-around">
    
        <fieldset class="col-md-3">
        
            <h5>Animations culinaires : </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="animationCulinaire" value="oui">
                    <label class="form-check-label" for=""> Oui </label>
                </div>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="animationCulinaire" value="non">
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="nbrPieceCocktail" class="form-control" placeholder="Nombres de piéces pour le cocktail" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>
                    
        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5> Déroulé du dîner : </h5>

                <div class="d-flex justify-content-md-around">
                    <div>
                        <div class="form-check mr-3">
                            <input class="form-check-input" type="checkbox" name="miseEnBouche" value="miseEnBouche">
                            <label for="" class="form-check-label"> Mise en bouche </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="viande" value="viande">
                            <label for="" class="form-check-label"> Viande </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="entree" value="entree">
                            <label for="" class="form-check-label"> Entrée </label>
                        </div>
                    </div>
                    
                    <div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="fromage" value="fromage">
                            <label for="" class="form-check-label"> Fromage </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="poisson" value="poisson">
                            <label for="" class="form-check-label"> Poisson </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="dessert" value="dessert">
                            <label for="" class="form-check-label"> Dessert </label>
                        </div>
                    </div>
                </div>

        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5> Brunch le lendemain </h5>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="brunchLendemain" value="oui">
                    <label for="" class="form-check-label"> Oui </label>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="brunchLendemain" value="non">
                    <label for="" class="form-check-label"> Non </label>
                </div>
                
        </fieldset>
        
    </div>
    
</div>

<div class="container">

    <div class="row justify-content-around">
    
        <fieldset>
    
                <table class="table-responsive-lg tableau_organisation table-bordered">
                
                    <tbody>
                        <tr>
                            <td colspan="2" rowspan="2"></td>
                            <td colspan="5" class="text-center"> Vin d'honneur </td>
                            <td colspan="4" class="text-center"> Dîner </td>
                            <td colspan="4" class="text-center"> Soirée </td>
                        </tr>
    
                        <tr>
                            <td>Eaux <br> (plate/gazeuse)</td>
                            <td>Softs<br>(jus,sodas...)</td>
                            <td>Cocktails</td>
                            <td>Alcools</td>
                            <td>Champagne</td>
                            <td>Eaux<br>(plate/gazeuse)</td>
                            <td>Softs<br>(jus,sodas...)</td>
                            <td>Vins</td>
                            <td>Alcools</td>
                            <td>Eaux<br>(plate/gazeuse)</td>
                            <td>Softs<br>(jus,sodas...)</td>
                            <td>Alcools</td>
                            <td>Champagne</td>
                        </tr>
    
                        <tr>
                            <td colspan="2" class="p-2">Les mariés</td>
                            <td><input type="radio" name="vinDhonneurEau" value="vinDhonneurEauMaries"></td>
                            <td><input type="radio" name="vinDhonneurSofts" value="vinDhonneurSoftsMaries"></td>
                            <td><input type="radio" name="vinDhonneurCocktail" value="vinDhonneurCocktailMaries"></td>
                            <td><input type="radio" name="vinDhonneurAlcools" value="vinDhonneurAlcoolsMaries"></td>
                            <td><input type="radio" name="vinDhonneurChampagne" value="vinDhonneurChampagneMaries"></td>
                            <td><input type="radio" name="dinerEau" value="dinerEauMaries"></td>
                            <td><input type="radio" name="dinerSofts" value="dinerSoftsMaries"></td>
                            <td><input type="radio" name="dinerVin" value="dinerVinMaries"></td>
                            <td><input type="radio" name="dinerAlcools" value="dinerAlcoolsMaries"></td>
                            <td><input type="radio" name="soireeEau" value="soireeEauMaries"></td>
                            <td><input type="radio" name="soireeSofts" value="soireeSoftsMaries"></td>
                            <td><input type="radio" name="soireeAlcools" value="soireeAlcoolsMaries"></td>
                            <td><input type="radio" name="soireeChampagne" value="soireeChampagneMaries"></td>
                        </tr>
    
                        <tr>
                            <td colspan="2" class="p-2">Le traiteur</td>
                            <td><input type="radio" name="vinDhonneurEau" value="vinDhonneurEauTraiteur"></td>
                            <td><input type="radio" name="vinDhonneurSofts" value="vinDhonneurSoftsTraiteur"></td>
                            <td><input type="radio" name="vinDhonneurCocktail" value="vinDhonneurCocktailTraiteur"></td>
                            <td><input type="radio" name="vinDhonneurAlcools" value="vinDhonneurAlcoolsTraiteur"></td>
                            <td><input type="radio" name="vinDhonneurChampagne" value="vinDhonneurChampagneTraiteur"></td>
                            <td><input type="radio" name="dinerEau" value="dinerEauTraiteur"></td>
                            <td><input type="radio" name="dinerSofts" value="dinerSoftsTraiteur"></td>
                            <td><input type="radio" name="dinerVin" value="dinerVinTraiteur"></td>
                            <td><input type="radio" name="dinerAlcools" value="dinerAlcoolsTraiteur"></td>
                            <td><input type="radio" name="soireeEau" value="soireeEauTraiteur"></td>
                            <td><input type="radio" name="soireeSofts" value="soireeSoftsTraiteur"></td>
                            <td><input type="radio" name="soireeAlcools" value="soireeAlcoolsTraiteur"></td>
                            <td><input type="radio" name="soireeChampagne" value="soireeChampagneTraiteur"></td>
                        </tr>
                    </tbody>
                    
                </table>
                
        </fieldset>
        
    </div>
    
</div>

        <h3 class="bg-info text-white text-center m-5"> Animations </h3>
            
<div class="container">

    <div class="row justify-content-around">
                         
        <fieldset class="col-md-3">

            <h5>Photographe</h5>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="formulePhotographe" class="form-control" placeholder="Formule souhaitée :" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="stylePhotographe" class="form-control" placeholder="Style de photos :" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">

                    <input type="text" name="supportPhotographe" class="form-control" placeholder="Support remis :" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>
                    
        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5>Vidéo</h5>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="formuleVideo" class="form-control" placeholder="Formule souhaitée :" pattern="[[0-9a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="styleVideo" class="form-control" placeholder="Style de vidéo :" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="formatVideo" class="form-control" placeholder="Format vidéo souhaitée :" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="supportVideo" class="form-control" placeholder="Support remis :" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>
                
        </fieldset>
        
        <fieldset class="col-md-3">
        
            <h5>Baby sitters</h5>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="nbrEnfantsBS" class="form-control" placeholder="Nombre d'enfants pris en charge" pattern="[0-9]{0,3}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="horaireBS" class="form-control" placeholder="Plage horaire de prise en charge" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{0,15}">
                </div>
                    
        </fieldset>
        
    </div>
    
</div>

<div class="container">

    <div class="row justify-content-around">
                         
        <fieldset class="mb-5 col-md-6">

                <h5>Musique</h5>

                    <div class="container">

                        <div class="row">
                        
                            <div class="col-lg-6">
                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="DJ" value="DJ">
                                    <label for="" class="form-check-label"> DJ </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="Gospel" value="Gospel">
                                    <label for="" class="form-check-label"> Gospel </label>
                                </div>
                                
                            </div>
                            
                            <div class="col-lg-6">
                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="Orchestre" value="Orchestre">
                                    <label for="" class="form-check-label"> Orchestre </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="Chant" value="Chant">
                                    <label for="" class="form-check-label"> Chant </label>
                                </div>
                                
                            </div>
                            
                            <div>
                                <input type="text" name="styleAnimationMusical" class="form-control mt-2" placeholder="Style de musique souhaité" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,55}">
                            </div>

                        </div>
                        
                    </div>
        </fieldset>
                
        <fieldset class="mb-5 col-md-6">

                <h5>Autres Prestataires : </h5>
                
                    <div class="container">

                        <div class="row">
                        
                            <div class="col-lg-6">
                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="coiffure" value="Coiffure">
                                    <label for="" class="form-check-label"> Coiffure </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="maquillage" value="Maquillage">
                                    <label for="" class="form-check-label"> Maquillage </label>
                                </div>
                                
                            </div>
                            
                            <div class="col-lg-6">
                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="officiant" value="officiant">
                                    <label for="" class="form-check-label"> Officiant de cérémonie </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="locationVoiture" value="locationVoiture">
                                    <label for="" class="form-check-label"> Location de voiture </label>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                        
        </fieldset>
        
        <fieldset class="mb-5">
                    <div class="d-flex align-items-center form-group">
                        <input type="text" name="autre" class="form-control" placeholder="Demandes particulières" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{0,120}">
                    </div>
                    
        </fieldset>

    </div>
    
</div>
        <div class="d-flex justify-content-center">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>            
        </div>

</form>
        

    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="../../JS/main.js"></script>
</body>

</html>
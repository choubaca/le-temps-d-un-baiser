<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/fiche/ficheNounous_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche de la nounou</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Editer </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/edit/updateNounous.php" method="POST" enctype="multipart/form-data">

    <input type="hidden" name="numNounou" value="<?=$Nounou['id'] ?>">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Coordonnées </h4>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomNounou" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$Nounou['nounou_nom']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomNounou" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,40}" value="<?=$Nounou['nounou_prenom']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telNounou" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?=$Nounou['nounou_telephone']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="text" name="mailNounou" class="form-control" required value="<?=$Nounou['nounou_mail']?>">
                </div>

        </fieldset>

        <fieldset class="mb-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Déplacement </h4>

                <h5> Vous déplacez-vous ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="deplacementNounou" value="oui" required
                        <?php if ($Nounou['deplacement_validation']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                    </div>
                    <div class="form-group">
                        <input type="text" name="zone" class="form-control" placeholder="Zone" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$Nounou['deplacement_zone']?>">
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="deplacementNounou" value="non"
                        <?php if ($Nounou['deplacement_validation']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Y a-t'il un supplément </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="supplementNounou" value="oui" required
                        <?php if ($Nounou['deplacement_supplement']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="prixKMNounou" class="form-control ml-3" placeholder="Prix aux KM" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}" value="<?=$Nounou['prix_au_km']?>">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="supplementNounou" value="non"
                        <?php if ($Nounou['deplacement_supplement']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Animateurs </h4>

            <h5> Nombre d'animateurs </h5>

                <div class="form-group">
                    <input type="text" name="nbAnimateurs" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$Nounou['nounou_nombre_animateur']?>">
                </div>

            <h5> Les animateurs ont-ils une qualification ? </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="qualifAnim" value="oui" required
                    <?php if ($Nounou['validation_qualification']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Oui </label>
                    <input type="text" name="quellesQualif" class="form-control ml-3" placeholder="Laquelles ?" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$Nounou['quelles_qualification']?>">
                </div>
                <div class="form-check mb-2">
                    <input class="form-check-input" type="radio" name="qualifAnim" value="non"
                    <?php if ($Nounou['validation_qualification']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>

            <h5> Quels matériels avez-vous besoin ? </h5>

                <div class="d-flex justify-content-around">
                    <div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="tables" value="tables"
                            <?php if ($Nounou['tables']=='tables') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Tables </label>
                        </div>
                        <div class="form-check mb-3">
                            <input class="form-check-input" type="checkbox" name="chaise" value="chaise"
                            <?php if ($Nounou['chaise']=='chaise') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Chaises </label>
                        </div>
                    </div>
                    <div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="sono" value="sono"
                            <?php if ($Nounou['sono']=='sono') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Sonorisation </label>
                        </div>
                    </div>
                </div>
                <div>
                    <input type="text" name="autres" class="form-control" placeholder="Autres" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$Nounou['autre_besoin']?>">
                </div>

        </fieldset>
            
    </div>
            
</div>
                        
<div class="container">
            
    <div class="row justify-content-around">
            
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Enfants </h4>

                <h5> Nombres d'enfants </h5>

                    <div class="container">
    
                        <div class="row">

                            <fieldset class="col-6">

                                <div class="d-flex">
                                    <input type="text" name="ageMinEnfant" class="form-control" placeholder="Age min pris en charge" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}" value="<?=$Nounou['age_minimum']?>">
                                    <input type="text" name="ageMaxEnfant" class="form-control" placeholder="Age max pris en charge" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}" value="<?=$Nounou['age_maximum']?>">
                                </div>
                                <div class="d-flex">
                                    <input type="text" name="nbrEnfantsMin" class="form-control" placeholder="Nombres d'enfants min" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}" value="<?=$Nounou['nombre_minimum']?>">
                                    <input type="text" name="nbrEnfantstMax" class="form-control" placeholder="Nombres d'enfants max" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}" value="<?=$Nounou['nombre_maximum']?>">
                                </div>

                            </fieldset>
            
                        </div>

                    </div>

                <h5> Gérez les enfants au diner ? </h5>

                    <div class="form-check form-check-inline mb-2">
                        <input class="form-check-input" type="radio" name="gereEnfantDiner" value="oui" required
                        <?php if ($Nounou['nounou_gere_enfant']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="gereEnfantDiner" value="non"
                        <?php if ($Nounou['nounou_gere_enfant']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Info divers </h4>

                <h5> Faite-vous des animations ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="animationNounou" value="oui" required
                        <?php if ($Nounou['animations_divers']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="lesquelles" class="form-control ml-3" placeholder="Lesquelles ?" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$Nounou['quelle_animations']?>">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="animationNounou" value="non"
                        <?php if ($Nounou['animations_divers']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Avez-vous une heure limite ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="heureLimiteNounou" value="oui" required
                        <?php if ($Nounou['nounou_heure_limite']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="quelHeure" class="form-control ml-3" placeholder="Laquelles" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$Nounou['nounou_laquelle']?>">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="heureLimiteNounou" value="non"
                        <?php if ($Nounou['nounou_heure_limite']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Tarifs </h4>

                <table>

                    <tr>
                        <td class="text-center"> Formules </td>
                        <td class="text-center"> Prix </td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="formule1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}"
                        value="<?=$Nounou['nounou_formule1']?>"></td>
                        <td> <input type="text" name="tarif1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}"
                        value="<?=$Nounou['nounou_tarif1']?>"></td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="formule2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}"
                        value="<?=$Nounou['nounou_formule2']?>"></td>
                        <td> <input type="text" name="tarif2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}"
                        value="<?=$Nounou['nounou_tarif2']?>"></td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="formule3" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}"
                        value="<?=$Nounou['nounou_formule3']?>"></td>
                        <td> <input type="text" name="tarif3" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}"
                        value="<?=$Nounou['nounou_tarif3']?>"></td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="formule4" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,50}"
                        value="<?=$Nounou['nounou_formule4']?>"></td>
                        <td> <input type="text" name="tarif4" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}"
                        value="<?=$Nounou['nounou_tarif4']?>"></td>
                    </tr>
                </table>

                <div class="form-group">
                    <label for="" class="control-label"> Autres </label>
                    <input type="text" name="remarques" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$Nounou['nounou_remarque']?>">
                </div>

        </fieldset>
            
    </div>

</div>

    <div class="d-flex justify-content-center">
        <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
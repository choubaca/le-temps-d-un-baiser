<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }
    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/edit/editMaries2_post.php"); 

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche du second marié</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Éditer </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/edit/updateMaries2.php" method="POST" enctype="multipart/form-data">
        
    <div class="container">
    
        <div class="row justify-content-around">

        <fieldset>

            <input type="hidden" name="numMarie" value="<?=$_GET['numMarie'];?>">

                <h3 class="text-center"> 2nd marié.e </h3>

                <div class="form-group">
                    <label for=""> État civil</label>
                    <input type="radio" name="sexe" value="madame"
                        <?php if ($marie2['marie2_etat_civil']=='madame') { echo 'checked="checked"';}?>> Madame
                    <input type="radio" name="sexe" value="monsieur"
                        <?php if ($marie2['marie2_etat_civil']=='monsieur') { echo 'checked="checked"';}?>> Monsieur
                    <input type="radio" name="sexe" value="indefini"
                        <?php if ($marie2['marie2_etat_civil']=='indefini') { echo 'checked="checked"';}?>> Indéfini
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nom" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie2['marie2_nom'];?>">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenom" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie2['marie2_prenom'];?>">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Profession </label>
                    <input type="text" name="profession" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie2['marie2_profession'];?>">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Date de naissance </label>
                    <input type="date" name="naissance" class="form-control" required value="<?=$marie2['marie2_date_naissance'];?>">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="tel" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?=$marie2['marie2_telephone'];?>">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="email" name="email" class="form-control" required value="<?=$marie2['marie2_email'];?>">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Adresse </label>
                    <input type="text" name="adresse" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]0-9]{1,120}" value="<?=$marie2['marie2_adresse'];?>">
                </div>

            </fieldset>

            <div class="d-flex flex-column justify-content-around">

                <fieldset>

                    <h3 class="text-center"> 3éme temoin </h3>

                    <div class="form-group">
                        <label for=""> État civil</label>
                        <input type="radio" name="sexeTemoin3" value="madame"
                            <?php if ($marie2['temoin3_etat_civil']=='madame') { echo 'checked="checked"';}?>> Madame
                        <input type="radio" name="sexeTemoin3" value="monsieur"
                            <?php if ($marie2['temoin3_etat_civil']=='monsieur') { echo 'checked="checked"';}?>> Monsieur
                        <input type="radio" name="sexeTemoin3" value="indefini"
                            <?php if ($marie2['temoin3_etat_civil']=='indefini') { echo 'checked="checked"';}?>> Indéfini
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Nom </label>
                        <input type="text" name="nomTemoin3" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie2['temoin3_nom'];?>">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Prénom </label>
                        <input type="text" name="prenomTemoin3" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie2['temoin3_prenom'];?>">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Téléphone </label>
                        <input type="text" name="telTemoin3" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?=$marie2['temoin3_telephone'];?>">
                    </div>

                </fieldset>

                <input type="button" id='bouton' <?php if (empty($marie2['temoin4_etat_civil'])){ echo 'class="btn btn-info"'; }else {echo 'class="d-none"';}?> value="Ajouter" onclick="afficher();">

                <fieldset <?php if (empty($marie2['temoin4_etat_civil'])){ echo 'class="d-none"'; }else {echo 'class="d-block"';}?> id='temoin2'>

                    <h3 class="text-center"> 4éme temoin </h3>

                    <div class="form-group">
                        <label for=""> État civil</label>
                        <input type="radio" name="sexeTemoin4" value="madame"
                            <?php if ($marie2['temoin4_etat_civil']=='madame') { echo 'checked="checked"';}?>> Madame
                        <input type="radio" name="sexeTemoin4" value="monsieur"
                            <?php if ($marie2['temoin4_etat_civil']=='monsieur') { echo 'checked="checked"';}?>> Monsieur
                        <input type="radio" name="sexeTemoin4" value="indefini"
                            <?php if ($marie2['temoin4_etat_civil']=='indefini') { echo 'checked="checked"';}?>> Indéfini
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Nom </label>
                        <input type="text" name="nomTemoin4" class="form-control requis" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie2['temoin4_nom'];?>">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Prénom </label>
                        <input type="text" name="prenomTemoin4" class="form-control requis" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie2['temoin4_prenom'];?>">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Téléphone </label>
                        <input type="text" name="telTemoin4" class="form-control requis" pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?=$marie2['temoin4_telephone'];?>">
                    </div>

                </fieldset>

                <input type="button" id='boutoncacher' class="d-none" value="Enlever" onclick="cacher();">

                </div>
            
            </div>
        
        </div>     

            <div class="d-flex justify-content-center m-5">
                <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
            </div>

</form>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="../../JS/main.js"></script>

</body>
</html>
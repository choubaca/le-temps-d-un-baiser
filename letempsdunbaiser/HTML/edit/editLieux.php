<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/ficheLieux_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche du lieu</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Éditer </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/edit/updateLieux.php" method="POST" enctype="multipart/form-data" class="d-flex flex-column">

<input type="hidden" name="numLieu" value="<?=$Lieu['id']?>">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-4 taille col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center mb-4"> Coordonnées </h4>
            
                <div class="form-group">
                    <input type="text" name="nomLieu" class="form-control" placeholder="Nom du lieu" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['lieu_nom']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="typeLieu" class="form-control" placeholder="Type de lieu" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['lieu_type']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="ville" class="form-control" placeholder="Ville" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['lieu_ville']?>">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="distance" class="form-control" placeholder="Distance depuis la ville" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['lieu_distance']?>">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="tempsTrajet" class="form-control" placeholder="Temps de trajet" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['lieu_temps_trajet']?>">
                </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <div class="d-flex align-items-center form-group">
                <input type="text" name="nomContact" class="form-control" placeholder="Nom du contact" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                value="<?=$Lieu['contact_nom']?>">
            </div>

            <div class="form-group">
                <input type="text" name="telContact" class="form-control" placeholder="Telephone du contact" pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$"
                value="<?=$Lieu['contact_telephone']?>">
            </div>

            <div class="form-group">
                <input type="text" name="mailContact" class="form-control" placeholder="Mail du contact"
                value="<?=$Lieu['contact_mail']?>">
            </div>

        </fieldset>
            
    </div>
        
</div>

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mt-5 col-lg-4 col-md-6">

                <h4 class="mb-4 bg-info text-white text-center"> La salle principale </h4>

                    <div class="form-group">
                        <input type="text" name="longueurSalleP" class="form-control" placeholder="Longueur de la salle" pattern="[0-9]{1,10}"
                        value="<?=$Lieu['salle1_longueur']?>">
                    </div>

                    <div class="form-group">
                        <input type="text" name="largeurSalleP" class="form-control" placeholder="Largeur de la salle" pattern="[0-9]{1,10}"
                        value="<?=$Lieu['salle1_largeur']?>">
                    </div>

                    <div class="form-group">
                        <input type="text" name="nbMaxSalleP" class="form-control" placeholder="Nombre de personne max" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['salle1_nombre_max_personne']?>">
                    </div>

                    <div class="d-flex justify-content-between">
                        <div class="d-flex flex-column mb-2">
                            <label class="form-check-label" for=""> Y a-t-il une séparation ? </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sepSalleP" value="oui"
                                <?php if ($Lieu['salle1_separation']=='oui') { echo 'checked="checked"';}?>>
                                <label class="form-check-label mr-2" for=""> Oui </label>
                                <input class="form-check-input" type="radio" name="sepSalleP" value="non"
                                <?php if ($Lieu['salle1_separation']=='non') { echo 'checked="checked"';}?>>
                                <label class="form-check-label" for=""> Non </label>
                            </div>
                        </div>

                        <div class="d-flex flex-column ml-2">
                            <label class="form-check-label" for=""> Est-elle chauffée ? </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sallePChauffe" value="oui"
                                <?php if ($Lieu['salle1_chauffage']=='oui') { echo 'checked="checked"';}?>>
                                <label class="form-check-label mr-2" for=""> Oui </label>
                                <input class="form-check-input" type="radio" name="sallePChauffe" value="non"
                                <?php if ($Lieu['salle1_chauffage']=='non') { echo 'checked="checked"';}?>>
                                <label class="form-check-label" for=""> Non </label>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class="d-flex flex-column">
                            <label class="form-check-label" for=""> Est-elle cimatisée ? </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sallePClimati" value="oui"
                                <?php if ($Lieu['salle1_climatisation']=='oui') { echo 'checked="checked"';}?>>
                                <label class="form-check-label mr-2" for=""> Oui </label>
                                <input class="form-check-input" type="radio" name="sallePClimati" value="non"
                                <?php if ($Lieu['salle1_climatisation']=='non') { echo 'checked="checked"';}?>>
                                <label class="form-check-label" for=""> Non </label>
                            </div>
                        </div>

                        <div class="d-flex flex-column">
                            <label class="form-check-label" for=""> Le vin d'honneur <br> est-il à l'exterieur ? </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="vinExter" value="oui"
                                <?php if ($Lieu['salle1_vin_en_exterieur']=='oui') { echo 'checked="checked"';}?>>
                                <label class="form-check-label mr-2" for=""> Oui </label>
                                <input class="form-check-input" type="radio" name="vinExter" value="non"
                                <?php if ($Lieu['salle1_vin_en_exterieur']=='non') { echo 'checked="checked"';}?>>
                                <label class="form-check-label" for=""> Non </label>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex flex-column text-center mb-5">
                    <label class="form-check-label" for="">Avez-vous un Plan B si mauvais temps ?</label>
                    <div class="form-check form-check-center">
                        <input class="form-check-input" type="radio" name="planB" value="oui"
                        <?php if ($Lieu['lieu_plan_B']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-5" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="planB" value="non"
                        <?php if ($Lieu['lieu_plan_B']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

        </fieldset>
            
    </div>
                
</div>
                            
<div class="container">
                
    <div class="row justify-content-around">
                
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> La salle numéro 2 </h4>

                <div class="form-group">
                    <input type="text" name="longueurSalle2" class="form-control" placeholder="Longeur de la salle" pattern="[0-9]{1,10}" value="<?=$Lieu['salle2_longueur']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="largeurSalle2" class="form-control" placeholder="Largeur de la salle" pattern="[0-9]{1,10}" value="<?=$Lieu['salle2_largeur']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="nbMaxSalle2" class="form-control" placeholder="Nombre de personne max" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['salle2_nombre_max_personne']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="utilSalle2" class="form-control" placeholder="Utilisation de la salle" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['salle2_utilisation']?>">
                </div>

                <div class="d-flex justify-content-between">
                    <div class="d-flex flex-column mb-2">
                        <label class="form-check-label" for=""> Y a-t-il une séparation ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sepSalle2" value="oui"
                            <?php if ($Lieu['salle2_separation']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="sepSalle2" value="non"
                            <?php if ($Lieu['salle2_separation']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>

                    <div class="d-flex flex-column ml-2">
                        <label class="form-check-label" for=""> Est-elle chauffée ? </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="salle2Chauffe" value="oui"
                            <?php if ($Lieu['salle2_chauffage']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="salle2Chauffe" value="non"
                            <?php if ($Lieu['salle2_chauffage']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-column">
                    <label class="form-check-label" for=""> Est-elle cimatisée ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="salle2Climati" value="oui"
                        <?php if ($Lieu['salle2_climatisation']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="salle2Climati" value="non"
                        <?php if ($Lieu['salle2_climatisation']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="mb-4 bg-info text-white text-center"> La salle numéro 3 </h4>

                <div class="form-group">
                    <input type="text" name="longueurSalle3" class="form-control" placeholder="Longeur de la salle" pattern="[0-9]{1,10}" value="<?=$Lieu['salle3_longueur']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="largeurSalle3" class="form-control" placeholder="Largeur de la salle" pattern="[0-9]{1,10}" value="<?=$Lieu['salle3_largeur']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="nbMaxSalle3" class="form-control" placeholder="Nombre de personne max" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['salle3_nombre_max_personne']?>">
                </div>

                <div class="form-group">
                    <input type="text" name="utilSalle3" class="form-control" placeholder="Utilisation de la salle" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['salle3_utilisation']?>">
                </div>

                <div class="d-flex justify-content-between">
                    <div class="d-flex flex-column mb-2">
                        <label class="form-check-label" for="">Y a-t-il une séparation ?</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sepSalle3" value="oui"
                            <?php if ($Lieu['salle3_separation']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="sepSalle3" value="non"
                            <?php if ($Lieu['salle3_separation']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>

                    <div class="d-flex flex-column">
                        <label class="form-check-label ml-2" for="">Est-elle chauffée ?</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="salle3Chauffe" value="oui"
                            <?php if ($Lieu['salle3_chauffage']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label mr-2" for=""> Oui </label>
                            <input class="form-check-input" type="radio" name="salle3Chauffe" value="non"
                            <?php if ($Lieu['salle3_chauffage']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Non </label>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-column">
                    <label class="form-check-label" for="">Est-elle cimatisée ?</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="salle3Climati" value="oui"
                        <?php if ($Lieu['salle3_climatisation']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="salle3Climati" value="non">
                        <?php if ($Lieu['salle3_climatisation']=='non') { echo 'checked="checked"';}?>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Terrasse</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="terrasse" value="oui"
                    <?php if ($Lieu['terrasse']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="terrasseSurf" class="form-control" placeholder="Surface" pattern="[0-9 m²]{1,10}"
                    value="<?=$Lieu['surface_terrasse']?>">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="terrasse" value="non"
                    <?php if ($Lieu['terrasse']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Jardin</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="jardin" value="oui"
                    <?php if ($Lieu['jardin']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="jardinSurf" class="form-control" placeholder="Surface" pattern="[0-9 m²]{1,10}"
                    value="<?=$Lieu['surface_jardin']?>">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="jardin" value="non"
                    <?php if ($Lieu['jardin']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>
            
            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Parc</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="parc" value="oui"
                    <?php if ($Lieu['parc']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="parcSurf" class="form-control" placeholder="Surface" pattern="[0-9 m²]{1,10}"
                    value="<?=$Lieu['surface_parc']?>">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="parc" value="non"
                    <?php if ($Lieu['parc']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Parking</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="parking" value="oui"
                    <?php if ($Lieu['parking']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="placeParking" class="form-control" placeholder="Nombre de places" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['place_parking']?>">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="parking" value="non"
                    <?php if ($Lieu['parking']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label text-center" for="">Vestiaire</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="vestiaire" value="oui"
                    <?php if ($Lieu['vestiaire']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="vestiaire" value="non"
                    <?php if ($Lieu['vestiaire']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="mb-2">
                <input type="text" name="WcHomme" class="form-control mb-2" placeholder="Nombre WC homme" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                value="<?=$Lieu['toilette_homme']?>">
                <input type="text" name="WcFemme" class="form-control" placeholder="Nombre WC femme" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                value="<?=$Lieu['toilette_femme']?>">
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label" for=""> Y a-t-il un acces aux PMR ?</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="PMR" value="oui"
                    <?php if ($Lieu['PMR']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input class="form-check-input" type="radio" name="PMR" value="non"
                    <?php if ($Lieu['PMR']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

        </fieldset>

    </div>
</div>

<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Matériel mis à disposition </h4>

                <div class="d-flex justify-content-around mb-5">
                    <div class="form-check">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuRetropro" value="lieuRetropro"
                            <?php if ($Lieu['retroprojecteur']=='lieuRetropro') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label">Rétroprojecteur</label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuEcran" value="lieuEcran"
                            <?php if ($Lieu['ecran']=='lieuEcran') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Écran </label>
                        </div>
                    </div>
                    <div class="form-check">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuAmpli" value="lieuAmpli"
                            <?php if ($Lieu['ampli']=='lieuAmpli') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Ampli </label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="lieuEnceinte" value="lieuEnceinte"
                            <?php if ($Lieu['enceinte']=='lieuEnceinte') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Enceinte </label>
                        </div>
                    </div>
                </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <table class="table table-responsive-md table-hover mb-5">

                <tbody class="table-bordered">

                    <tr>
                        <td></td>
                        <td class="text-center"> Dimensions </td>
                        <td class="text-center"> Nombres de personnes </td>
                        <td class="text-center"> Matière </td>
                        <td class="text-center"> Quantité </td>
                    </tr>
                    <tr>
                        <td> Table ronde </td>
                        <td> <input type="text" name="dimTableRonde" class="form-control" placeholder="Dimension" pattern="[0-9a-zA-Zà-ÿÀ-Û- '*]{1,35}"
                        value="<?=$Lieu['table_ronde_dimension']?>"></td>
                        <td> <input type="text" name="nbrPersTableRonde" class="form-control" placeholder="Nombres de place" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['table_ronde_nombre_de_personnes']?>"></td>
                        <td> <input type="text" name="matiereTableRonde" class="form-control" placeholder="Matière" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['table_ronde_matiere']?>"></td>
                        <td> <input type="text" name="nbrTableRonde" class="form-control" placeholder="Nombre" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['table_ronde_nombre']?>"></td>
                    </tr>
                    <tr>
                        <td> Table rectangulaire </td>
                        <td> <input type="text" name="dimTableRect" class="form-control" placeholder="Dimension" pattern="[0-9a-zA-Zà-ÿÀ-Û- '*]{1,35}"
                        value="<?=$Lieu['table_rectanculaire_dimension']?>"></td>
                        <td> <input type="text" name="nbrPersTableRect" class="form-control" placeholder="Nombres de place" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['table_rectanculaire_nombre_de_personnes']?>"></td>
                        <td> <input type="text" name="matiereTableRect" class="form-control" placeholder="Matière" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['table_rectanculaire_matiere']?>"></td>
                        <td> <input type="text" name="nbrTableRect" class="form-control" placeholder="Nombre" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['table_rectanculaire_nombre']?>"></td>
                    </tr>
                    <tr>
                        <td> Buffet </td>
                        <td> <input type="text" name="dimBuffet" class="form-control" placeholder="Dimension" pattern="[0-9a-zA-Zà-ÿÀ-Û- '*]{1,35}"
                        value="<?=$Lieu['buffet_dimension']?>"></td>
                        <td> <input type="text" name="nbrPersBuffet" class="form-control" placeholder="Nombres de place" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['buffet_nombre_de_personnes']?>"></td>
                        <td> <input type="text" name="matiereBuffet" class="form-control" placeholder="Matière" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['buffet_matiere']?>"></td>
                        <td> <input type="text" name="nbrBuffet" class="form-control" placeholder="Nombre" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['buffet_nombre']?>"></td>
                    </tr>
                    <tr>
                        <td colspan="5"> <input type="text" name="autreTable" class="form-control" placeholder="Autres" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['autre_table']?>"></td>
                    </tr>

                </tbody>

            </table>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <table class="table table-responsive-md table-hover mb-3">

                <tbody class="table-bordered">

                    <tr>
                        <td></td>
                        <td class="text-center"> Type </td>
                        <td class="text-center"> Matière </td>
                        <td class="text-center"> Quantité </td>
                    </tr>
                    <tr>
                        <td> Chaises interieures </td>
                        <td> <input type="text" name="TypeChaiseInt" class="form-control" placeholder="Type" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['chaise_interieur_type']?>"></td>
                        <td> <input type="text" name="matiereChaiseInt" class="form-control" placeholder="Matière" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['chaise_interieur_matiere']?>"></td>
                        <td> <input type="text" name="qtsChaiseInt" class="form-control" placeholder="Nombre" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['chaise_interieur_nombre']?>"></td>
                    </tr>
                    <tr>
                        <td> Chaises extèrieures </td>
                        <td> <input type="text" name="TypeChaiseExt" class="form-control" placeholder="Type" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['chaise_exterieur_type']?>"></td>
                        <td> <input type="text" name="matiereChaiseExt" class="form-control" placeholder="Matière" pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['chaise_exterieur_matiere']?>"></td>
                        <td> <input type="text" name="qtsChaiseExt" class="form-control" placeholder="Nombre" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['chaise_exterieur_nombre']?>"></td>
                    </tr>
                    <tr>
                        <td colspan="4"> <input type="text" name="autreChaise" class="form-control" placeholder="Autres" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                        value="<?=$Lieu['autre_chaise']?>"></td>
                    </tr>

                </tbody>

            </table>

                <div class="d-flex justify-content-center">

                    <div class="d-flex flex-column mb-2 w-50">
                        <label class="form-check-label text-center" for="">Le mobilier est-il compris dans le tarif de
                            la location ?</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="mobilierCompris" value="oui"
                            <?php if ($Lieu['prix_mobilier_compris']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Oui </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="mobilierCompris" value="non"
                            <?php if ($Lieu['prix_mobilier_compris']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label mr-2" for=""> Non </label>
                            <input type="text" name="autreMobilier" class="form-control" placeholder="Prix de la location" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['autre_mobilier']?>">
                        </div>
                    </div>

                </div>

        </fieldset>

    </div>

</div>

<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="col-lg-4 col-md-6 mt-5">

                <h4 class="bg-info text-white text-center"> Tarifs </h4>

                <table class="table table-responsive-md table-hover mb-5">

                    <tbody class="table-bordered">

                        <tr>
                            <td colspan="5" class="text-center">
                                <h4> Formules </h4>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <h5> Jour et heure </h5>
                            </td>
                            <td class="text-center">
                                <h5> Jour et heure </h5>
                            </td>
                            <td colspan="3" class="text-center">
                                <h5> Tarif / Saison </h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> </td>
                            <td class="text-center">
                                <h6> Haute </h6>
                            </td>
                            <td class="text-center">
                                <h6> Moyenne </h6>
                            </td>
                            <td class="text-center">
                                <h6> Basse </h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"> Du <br> <input type="date" class="form-control" name="date1"
                            value="<?=$Lieu['date1']?>"><br>
                                à partir de <br> <input type="text" class="form-control" name="heure1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['heure1']?>"></td>
                            <td class="text-center"> Au <br> <input type="date" class="form-control" name="date2"
                            value="<?=$Lieu['date2']?>"><br>
                                à partir de <br> <input type="text" class="form-control" name="heure2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['heure2']?>"></td>
                            <td> <input type="text" name="tarifHaute1" class="form-control" placeholder="Tarif saison haute" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_saison_haute1']?>"></td>
                            <td> <input type="text" name="tarifMoy1" class="form-control" placeholder="Tarif saison moyenne" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_moyenne_saison1']?>"></td>
                            <td> <input type="text" name="tarifBas1" class="form-control" placeholder="Tarif saison basse" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_basse_saison1']?>"></td>
                        </tr>
                        <tr>
                            <td class="text-center"> Du <br> <input type="date" class="form-control" name="date3"
                            value="<?=$Lieu['date3']?>"><br>
                                à partir de <br> <input type="text" class="form-control" name="heure3" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['heure3']?>"></td>
                            <td class="text-center"> Au <br> <input type="date" class="form-control" name="date4"
                            value="<?=$Lieu['date4']?>"><br>
                                à partir de <br> <input type="text" class="form-control" name="heure4" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['heure4']?>"></td>
                            <td> <input type="text" name="tarifHaute2" class="form-control" placeholder="Tarif saison haute" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_saison_haute2']?>"></td>
                            <td> <input type="text" name="tarifMoy2" class="form-control" placeholder="Tarif saison moyenne" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_moyenne_saison2']?>"></td>
                            <td> <input type="text" name="tarifBas2" class="form-control" placeholder="Tarif saison basse" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_basse_saison2']?>"></td>
                        </tr>
                        <tr>
                            <td class="text-center"> Du <br> <input type="date" class="form-control" name="date5"
                            value="<?=$Lieu['date5']?>"><br>
                                à partir de <br> <input type="text" class="form-control" name="heure5" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['heure5']?>"></td>
                            <td class="text-center"> Au <br> <input type="date" class="form-control" name="date6"
                            value="<?=$Lieu['date6']?>"><br>
                                à partir de <br> <input type="text" class="form-control" name="heure6" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['heure6']?>"></td>
                            <td> <input type="text" name="tarifHaute3" class="form-control" placeholder="Tarif saison haute" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_saison_haute3']?>"></td>
                            <td> <input type="text" name="tarifMoy3" class="form-control" placeholder="Tarif saison moyenne" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_moyenne_saison3']?>"></td>
                            <td> <input type="text" name="tarifBas3" class="form-control" placeholder="Tarif saison basse" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                            value="<?=$Lieu['tarif_basse_saison3']?>"></td>
                        </tr>

                    </tbody>

                </table>

        </fieldset>

    </div>

</div>

<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="col-lg-4 col-md-6">

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label" for="">Le nettoyage est-il inclue ?</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nettoyageInclue" value="oui"
                    <?php if ($Lieu['nettoyage_inclue']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="tarifNettoie" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['tarif_nettoyage']?>">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="nettoyageInclue" value="non"
                    <?php if ($Lieu['nettoyage_inclue']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

            <div class="d-flex flex-column mb-2">
                <label class="form-check-label" for="">La restoration est-elle imposée ? </label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="restoImposer" value="oui"
                    <?php if ($Lieu['restauration_impose']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label mr-2" for=""> Oui </label>
                    <input type="text" name="tarifResto" class="form-control" placeholder="Tarif" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['tarif_restauration']?>">
                </div>
                <div class="form-check form-check">
                    <input class="form-check-input" type="radio" name="restoImposer" value="non"
                    <?php if ($Lieu['restauration_impose']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>
            </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

                <h4 class="bg-info text-white text-center"> Hébergements sur place : </h4>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Y a-t-il un hébergement sur place ?</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="hebergSurPlace" value="oui"
                        <?php if ($Lieu['hebergement_sur_place']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                        <input class="form-check-input" type="radio" name="hebergSurPlace" value="non"
                        <?php if ($Lieu['hebergement_sur_place']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="form-group">
                    <input type="text" name="combienChambre" class="form-control" placeholder="Combien y a-t-il de chambre ?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}"
                    value="<?=$Lieu['nombre_de_chambre']?>">
                </div>

                <div>
                    <p> La capacité total de l'hébergement est de <input type="text" name="capaTotal" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" placeholder="Capacité totale" value="<?=$Lieu['capacite_total']?>"> personnes. </p>
                </div>

                <div>
                    <p> Le prix par chambre est compris entre </p>
                    <p class="d-flex">
                        <fieldset class="col-lg-4 col-md-6">
                            <input type="text" name="prixChambre1" class="form-control mr-2" placeholder="Prix bas" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['prix_chambre1']?>">
                        </fieldset>
                            et
                        <fieldset class="col-lg-4 col-md-6">
                            <input type="text" name="prixChambre2" class="form-control ml-2" placeholder="Prix haut" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['prix_chambre2']?>"> </p>
                        </fieldset>
                </div>

                <div class="form-group">
                    <label for=""> Forfait de location de la totalité des hébergements : </label>
                    <input type="text" name="forfaitLocation" class="form-control" placeholder="Forfait location" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['forfait_location']?>">
                </div>

                <div class="form-group">
                    <label for=""> Forfait location salle + hébergement : </label>
                    <input type="text" name="forfaitLocHerb" class="form-control" placeholder="Forfait location et hébergement" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['forfait_location_et_hebergement']?>">
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Questions Diverses : </h4>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Y a-t-il une salle pour coucher les enfants ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="salleOccupEnfant" value="oui"
                        <?php if ($Lieu['lieu_salle_occupation_enfant']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="salleOccupEnfant" value="non"
                        <?php if ($Lieu['lieu_salle_occupation_enfant']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Y a-t-il une salle pour occuper les enfants ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="SalleDormirEnfant" value="oui"
                        <?php if ($Lieu['lieu_salle_repos_enfant']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="SalleDormirEnfant" value="non"
                        <?php if ($Lieu['lieu_salle_repos_enfant']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Un seul mariage/week end ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="mariageSolo" value="oui"
                        <?php if ($Lieu['lieu_mariage_unique']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="mariageSolo" value="non"
                        <?php if ($Lieu['lieu_mariage_unique']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for=""> Accompte à verser pour réservation : </label>
                    <input type="text" name="accompte" class="form-control" placeholder="accompte" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['lieu_accompte']?>">
                </div>

                <div class="d-flex flex-column mb-2">
                    <label class="form-check-label" for="">Avez-vous l’habitude de travailler <br> avec des Wedding Planners ? </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="travailWP" value="oui"
                        <?php if ($Lieu['lieu_travailWP']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label mr-2" for=""> Oui </label>
                    </div>
                    <div class="form-check form-check">
                        <input class="form-check-input" type="radio" name="travailWP" value="non"
                        <?php if ($Lieu['lieu_travailWP']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                </div>

        </fieldset>
            
    </div>
                
</div>
                            
<div class="container">
                
    <div class="row justify-content-around">
                
        <fieldset class="mt-4 col-lg-4 col-md-6">

            <div class="form-group">
                <input type="text" name="remarque" class="form-control" placeholder="Remarques" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?=$Lieu['lieu_remarque']?>">
            </div>

         </fieldset>

    </div>

</div>

        <div class="d-flex justify-content-center m-5">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/ficheDJ_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche du DJ</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Éditer </h1>
    </div>

    <form class="d-flex flex-column" action="../../PHP/edit/updateDJ.php" method="POST" enctype="multipart/form-data">
    
    <input type="hidden" name="numDJ" value="<?=$_GET['numDJ']?>">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Coordonnées </h4>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomDJ" class="form-control" value="<?=$DJ['dj_nom']?>" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomDJ" class="form-control" value="<?=$DJ['dj_prenom']?>" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telDJ" class="form-control" value="<?=$DJ['dj_telephone']?>" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="email" name="mailDJ" class="form-control" value="<?=$DJ['dj_mail']?>" required>
                </div>

            </fieldset>

            <fieldset class="mb-5 col-lg-4 col-md-6">

                <h4 class="bg-info text-white text-center"> Déplacement </h4>

                    <h5> Vous déplacez-vous ? </h5>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deplacementDJ" value="oui" required
                            <?php if ($DJ['deplacement_validation']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Oui </label>
                            <input type="text" name="zone" class="form-control ml-3" placeholder="Zone" value="<?=$DJ['deplacement_zone']?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,70}">
                        </div>
                        <div class="form-check mb-2">
                            <input class="form-check-input" type="radio" name="deplacementDJ" value="non"
                            <?php if ($DJ['deplacement_validation']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Non </label>
                        </div>

                    <h5> Avez-vous des frais de déplacement ? </h5>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="supplementDJ" value="oui" required
                            <?php if ($DJ['deplacement_supplement']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Oui </label>
                            <input type="text" name="prixKMDJ" class="form-control ml-3" placeholder="Prix au KM" value="<?=$DJ['prix_au_km']?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,70}">

                        </div>
                        <div class="form-check mb-2">
                            <input class="form-check-input" type="radio" name="supplementDJ" value="non"
                            <?php if ($DJ['deplacement_supplement']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Non </label>
                        </div>

            </fieldset>

            <fieldset class="col-lg-4 col-md-6">

                <h4 class="bg-info text-white text-center"> Matériels </h4>

                    <h5> Quels matériels avez-vous ? </h5>

                        <div class="d-flex justify-content-md-around">
                            <div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="sonoDJ" value="sono"
                                    <?php if ($DJ['dj_sono']=='sono') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Sono </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="videoprojecteurDJ" value="videoProjecteur"
                                    <?php if ($DJ['dj_videoprojecteur']=='videoProjecteur') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Vidéo-projecteur </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="eclairageDJ" value="eclairage"
                                    <?php if ($DJ['dj_eclairage']=='eclairage') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Éclairages </label>
                                </div>
                            </div>
                            <div>
                                <div class="form-check ml-2">
                                    <input class="form-check-input" type="checkbox" name="machineAFumeeDJ" value="machineAFumee"
                                    <?php if ($DJ['dj_machine_a_fumee']=='machineAFumee') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Machine à fumée </label>
                                </div>
                                <div class="form-check ml-2">
                                    <input class="form-check-input" type="checkbox" name="microDJ" value="micro"
                                    <?php if ($DJ['dj_micro']=='micro') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Micro </label>
                                </div>
                            </div>
                        </div>

                    <h5> Avez-vous du matériel de rechange ? </h5>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="rechangeDJ" value="oui" required
                            <?php if ($DJ['dj_rechange']=='oui') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Oui </label>
                            <input class="form-check-input ml-2" type="radio" name="rechangeDJ" value="non"
                            <?php if ($DJ['dj_rechange']=='non') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Non </label>
                        </div>

        </fieldset>
            
    </div>
        
</div>
                    
<div class="container">
        
    <div class="row justify-content-around">
        
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Musique </h4>

                <h5>Quel est votre style musical ?</h5>

                    <div class="form-group">
                        <input type="text" name="styleMusical" class="form-control" value="<?=$DJ['dj_style_musical']?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,120}">
                    </div>

                <h5> Mettez-vous la musique demandée ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="musiqueDemande" value="oui" required
                        <?php if ($DJ['dj_musique_demande']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="musiqueDemande" value="non"
                        <?php if ($DJ['dj_musique_demande']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Faite-vous des animations ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="animationDJ" value="oui" required
                        <?php if ($DJ['animations_divers']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="lesquels" class="form-control ml-3" placeholder="Lesquelles ?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,70}"
                            value="<?=$DJ['quelle_animations']?>">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="animationDJ" value="non"
                        <?php if ($DJ['animations_divers']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Info divers </h4>

                <h5>Comment vous habillez-vous ?</h5>

                <div class="form-group">
                    <input type="text" name="habitDJ" class="form-control" required  pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$DJ['dj_habit']?>">
                </div>

                <h5> Travaillez-vous seul ? </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="seulDJ" value="oui" required
                    <?php if ($DJ['dj_travail_seul']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Oui </label>
                    <input class="form-check-input ml-2" type="radio" name="seulDJ" value="non"
                    <?php if ($DJ['dj_travail_seul']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <h5> Avez-vous une heure limite ?</h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="heureDJ" value="oui" required
                    <?php if ($DJ['dj_heure_limite']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Oui </label>
                    <input type="text" name="heureLimiteDJ" class="form-control ml-3" placeholder="Heure limite ?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_laquelle']?>">
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="heureDJ" value="non"
                    <?php if ($DJ['dj_heure_limite']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>

        </fieldset>

        <fieldset class="mt-5 col-lg-4 col-md-6">

                <table>

                <h4 class="bg-info text-white text-center"> Tarifs </h4>
                    <tr>
                        <td></td>
                        <td class="text-center">Prix</td>
                    </tr>
                    <tr>
                        <td class="pr-3">Formule vin d’honneur + soirée</td>
                        <td> <input type="text" name="tarif1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_tarif1'];?>"> </td>
                    </tr>
                    <tr>
                        <td>Option cérémonie laïque</td>
                        <td> <input type="text" name="tarif2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_tarif2'];?>"> </td>
                    </tr>
                    <tr>
                        <td>Option photobooth ? </td>
                        <td> <input type="text" name="tarif3" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_tarif3'];?>"> </td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="option1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_option1'];?>"> </td>
                        <td> <input type="text" name="tarif4" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_tarif4'];?>"> </td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="option2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_option2'];?>"> </td>
                        <td> <input type="text" name="tarif5" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$DJ['dj_tarif5'];?>"> </td>
                    </tr>

                </table>

        </fieldset>

    </div>

</div>

    <div class="button-ajust d-flex justify-content-center">
        <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
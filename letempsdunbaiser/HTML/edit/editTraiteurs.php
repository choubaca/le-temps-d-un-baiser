<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/ficheTraiteurs_post.php"); 

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche du traiteur</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Éditer </h1>
    </div>

<form action="../../PHP/edit/updateTraiteurs.php" method="POST" enctype="multipart/form-data" class="d-flex flex-column">


    <input type="hidden" name="numTraiteur" value="<?=$_GET['numTraiteur']?>">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6"> 

            <h4 class="bg-info text-white text-center"> Coordonnées </h4>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomTraiteur" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?= $Traiteur['traiteur_nom']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomTraiteur" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?= $Traiteur['traiteur_prenom']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Adresse </label>
                    <input type="mail" name="adresseTraiteur" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,120}" value="<?= $Traiteur['traiteur_adresse']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telTraiteur" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?= $Traiteur['traiteur_telephone']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="mail" name="mailTraiteur" class="form-control" required value="<?= $Traiteur['traiteur_mail']?>">
                </div>
            
        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Info divers </h4>

                <h5> Nombre d'invités </h5>

                    <div class="form-group d-flex">
                        <div>
                            <label for="" class="control-label"> Minimum (sans supplément) </label>
                            <input type="text" name="nbrMinInvite" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?= $Traiteur['nombre_minimum_invite']?>">
                        </div>
                        <div class="ml-2">
                            <label for="" class="control-label"> Maximum </label>
                            <input type="mail" name="nbrMaxInvite" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?= $Traiteur['nombre_maximum_invite']?>">
                        </div>
                    </div>

                <h5> Nombre de serveurs</h5>

                    <div class="form-group">
                        <input type="text" name="nbrServeurs" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?= $Traiteur['nombre_serveur']?>">
                    </div>

                <h5> Heure limite</h5>

                    <div class="form-group">
                        <input type="text" name="heureLimite" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,35}" value="<?= $Traiteur['heure_limite_traiteur']?>">
                    </div>

                <h5> Tarif des heures supplémentaires</h5>

                    <div class="form-group">
                        <input type="text" name="tarifHeureSup" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}" value="<?= $Traiteur['tarif_heure_supplementaire']?>">
                    </div>
            
                <h5> Déduction une fois le contrat signé : </h5>

                    <div class="form-group">
                        <input type="text" name="deduction" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}" value="<?= $Traiteur['deduction']?>">
                    </div>

                <h5> Combien de temps avant le mariage faut-il <br> fixer le choix du menu et le nombre d’invités :</h5>

                    <div class="form-group">
                        <input type="text" name="tempsAvantMariage" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}" value="<?= $Traiteur['commande_avant_mariage']?>">
                    </div>
            
                <h5> Combien de temps arrivez-vous <br> avant le vin d'honneur ?</h5>

                    <div class="form-group">
                        <input type="text" name="delaiVinDhonnuer" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}" value="<?= $Traiteur['arriver_avant_vin_dhonneur']?>">
                    </div>

                <h5> Combien de temps entre les plats ?</h5>

                    <div class="form-group">
                        <input type="text" name="delaiPlat" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}" value="<?= $Traiteur['delai_entre_plat']?>">
                    </div>

        </fieldset>
            
    </div>
        
</div>
                        
<div class="container">
            
    <div class="row justify-content-around">
            
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h3 class="bg-info text-white text-center"> Cuisine </h3>
            
                <h5> Qualité des produits </h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="produitFrais" value="frais" required
                        <?php if ($Traiteur['produit_frais']=='frais') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Frais </label>
                        <input class="form-check-input ml-2" type="radio" name="produitFrais" value="surgeles"
                        <?php if ($Traiteur['produit_frais']=='surgeles') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Surgelés </label>
                    </div>

                <h5> Produits principalement de saison </h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="produitSaison" value="oui" required
                        <?php if ($Traiteur['produit_de_saison']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="produitSaison" value="non"
                        <?php if ($Traiteur['produit_de_saison']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5>Tout est-il fait maison ? </h5>

                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="faitMaison" value="oui" required
                        <?php if ($Traiteur['fait_maison']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                    </div>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="faitMaison" value="non"
                        <?php if ($Traiteur['fait_maison']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                        <input type="text" name="autreFaitMaison" class="form-control ml-3" placeholder="Autres methodes" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}"
                        value="<?= $Traiteur['autre_fait_maison']?>">
                    </div>

                <h5> Style de cuisine</h5>

                    <div class="form-group">
                        <input type="text" name="styleCuisine" class="form-control" required pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}"
                        value="<?= $Traiteur['traiteur_style_cuisine']?>">
                    </div>

                <h5> Les contraintes alimentaires </h5>

                    <div class="mb-2">
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="halal" value="halal"
                                <?php if ($Traiteur['halal']=='halal') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Halal </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cacher" value="cacher"
                                <?php if ($Traiteur['cacher']=='cacher') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Cacher </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="vegetarien" value="vegetarien"
                                <?php if ($Traiteur['vegetarien']=='vegetarien') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Végétarien </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="sansgluten" value="sansgluten"
                                <?php if ($Traiteur['sansgluten']=='sansgluten') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Sans gluten </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="vegan" value="vegan"
                                <?php if ($Traiteur['vegan']=='vegan') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Végan </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="sanslactose" value="sanslactose"
                                <?php if ($Traiteur['sanslactose']=='sanslactose') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Sans lactose </label>
                            </div>
                        </div>
                    </div>

                <h5> Comment sont fait les desserts ? </h5>

                    <div class="form-check-inline mb-2">
                        <input class="form-check-input" type="radio" name="dessertFait" value="vous" required
                        <?php if ($Traiteur['fabrication_dessert']=='vous') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Par vous </label>
                        <input class="form-check-input ml-2" type="radio" name="dessertFait" value="patissier"
                        <?php if ($Traiteur['fabrication_dessert']=='patissier') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Un pâtissier extérieur </label>
                    </div>

                <h5> Matière des bouteilles : </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="matiereBouteille" value="verre" required
                        <?php if ($Traiteur['bouteille_matiere']=='verre') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Verre </label>
                        <input class="form-check-input ml-2" type="radio" name="matiereBouteille" value="plastique"
                        <?php if ($Traiteur['bouteille_matiere']=='plastique') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Plastique </label>
                    </div>

                <h5> Y a-t-il un supplement pour avoir <br> des bouteilles en verre ? </h5>

                    <div class="form-check-inline">
                        <input class="form-check-input" type="radio" name="supplementBouteille" value="oui" required
                        <?php if ($Traiteur['bouteille_supplement']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="combienSup" class="form-control ml-3" placeholder="Combien?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}" value="<?= $Traiteur['tarif_supplement']?>">
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="supplementBouteille" value="non"
                        <?php if ($Traiteur['bouteille_supplement']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
                    
                <h5> Type de prestations </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="buffet" value="buffet"
                                <?php if ($Traiteur['buffet']=='buffet') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Buffet </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="animCuli" value="animCuli"
                                <?php if ($Traiteur['animation_culinaire']=='animCuli') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Animations culinaires </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="serviceTable" value="service"
                                <?php if ($Traiteur['service_a_table']=='service') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Service à table </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="brunch" value="brunch"
                                <?php if ($Traiteur['brunch']=='brunch') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Brunch </label>
                            </div>
                        </div>
                    </div>

                <h5> Type de dessert </h5>

                    <div>
                        <div>
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" name="pieceMonteeChoux" value="pieceChoux"
                                <?php if ($Traiteur['traiteur_piece_montee_choux']=='pieceChoux') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Pièce montée de choux </label>
                            </div>
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" name="pieceMonteeMacarons" value="pieceMacaron"
                                <?php if ($Traiteur['traiteur_piece_montee_macarons']=='pieceMacaron') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Pièce montée de macarons </label>
                            </div>
                        </div>
                        <div>
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" name="weddingCake" value="wedding"
                                <?php if ($Traiteur['traiteur_wedding_cake']=='wedding') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Wedding Cakes </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="autreDessert" class="form-control" placeholder="Autres types de pièces montées" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}"
                        value="<?= $Traiteur['traiteur_autre_dessert']?>">
                    </div>

                <h5> Cocktail : </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="eauxCocktail" value="eauxCocktail"
                                <?php if ($Traiteur['traiteur_cocktail_eaux']=='eauxCocktail') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Eaux </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="softsCocktail" value="softsCocktail"
                                <?php if ($Traiteur['traiteur_cocktail_softs']=='softsCocktail') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Softs </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="alcoolsCocktail" value="alcoolsCocktail"
                                <?php if ($Traiteur['traiteur_cocktail_alcools']=='alcoolsCocktail') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Alcools forts </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cocktails" value="cocktails"
                                <?php if ($Traiteur['traiteur_cocktail']=='cocktails') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Cocktails </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="champagneCocktail" value="champagneCocktail"
                                <?php if ($Traiteur['traiteur_cocktail_champagne']=='champagneCocktail') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Champagne </label>
                            </div>
                        </div>
                    </div>

                <h5> Dîner : </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="eauxDiner" value="eauxDiner"
                                <?php if ($Traiteur['traiteur_diner_eaux']=='eauxDiner') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Eaux </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="softsDiner" value="softsDiner"
                                <?php if ($Traiteur['traiteur_diner_softs']=='softsDiner') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Softs </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="vinDiner" value="vinDiner"
                                <?php if ($Traiteur['traiteur_diner_vins']=='vinDiner') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Vins </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="ChampagneDiner" value="ChampagneDiner"
                                <?php if ($Traiteur['traiteur_diner_champagne']=='ChampagneDiner') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Champagne </label>
                            </div>
                        </div>
                    </div>

                <h5> Soirée : </h5>

                    <div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="eauxSoiree" value="eauxSoiree"
                                <?php if ($Traiteur['traiteur_soiree_eaux']=='eauxSoiree') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Eaux </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="softsSoiree" value="softsSoiree"
                                <?php if ($Traiteur['traiteur_soiree_softs']=='softsSoiree') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Softs </label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="checkbox" name="alcoolSoiree" value="alcoolSoiree"
                                <?php if ($Traiteur['traiteur_soiree_alcools']=='alcoolSoiree') {echo 'checked="checked"';}?>>
                                <label for="" class="form-check-label"> Alcools forts </label>
                            </div>
                        </div>
                    </div>

        </fieldset>
            
    </div>

</div>
                        
<div class="container">
            
    <div class="row justify-content-around">
            
        <fieldset class="mt-5 col-lg-4 col-md-6">      
        
            <h3 class="bg-info text-white text-center"> Organisation </h3>

                <h5> Comment est fait le service ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="service" value="verre" required
                        <?php if ($Traiteur['service']=='verre') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Au verre </label>
                        <input class="form-check-input ml-2" type="radio" name="service" value="aTable"
                        <?php if ($Traiteur['service']=='aTable') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Bouteille à table </label>
                    </div>

                <h5> Décoration du buffet incluse </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="decoBuffet" value="oui" required
                        <?php if ($Traiteur['decoration_buffet']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="decoBuffet" value="non"
                        <?php if ($Traiteur['decoration_buffet']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Choix de couleurs du chemin <br> de table du buffet ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="couleurChemin" value="oui" required
                        <?php if ($Traiteur['couleur_chemin_table']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="couleurChemin" value="non"
                        <?php if ($Traiteur['couleur_chemin_table']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
            
                <h5> Le nappage et les serviettes sont : </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="nappage" value="vous" required
                        <?php if ($Traiteur['nappage']=='vous') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> À vous </label>
                        <input class="form-check-input ml-2" type="radio" name="nappage" value="louee"
                        <?php if ($Traiteur['nappage']=='louee') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Louée </label>
                    </div>

                <h5> Y a-t-il possibilité de récupérer les nappages <br> et les serviettes à l’avance ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="recupnappe" value="oui"
                        <?php if ($Traiteur['recuperation_nappage']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="recupnappe" value="non"
                        <?php if ($Traiteur['recuperation_nappage']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> La vaisselle est : </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="vaisselle" value="vous"
                        <?php if ($Traiteur['vaisselle']=='vous') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> À vous </label>
                        <input class="form-check-input ml-2" type="radio" name="vaisselle" value="louee"
                        <?php if ($Traiteur['vaisselle']=='louee') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Louée </label>
                    </div>


                <h5> La vaisselle est-elle laissée sur place à votre départ ? </h5>


                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="vaisselleSurPlace" value="oui"
                        <?php if ($Traiteur['vaisselle_laissee']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="vaisselleSurPlace" value="non"
                        <?php if ($Traiteur['vaisselle_laissee']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Modalités de retour de la vaisselle : </h5>

                    <div class="form-group">
                        <input type="text" name="retourVaisselle" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}" value="<?= $Traiteur['vaisselle_retour']?>">
                    </div>

                <h5> Apportez-vous le matériel nécessaire ?</h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="apportMateriel" value="oui"
                        <?php if ($Traiteur['apporte_materiel']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="apportMateriel" value="non"
                        <?php if ($Traiteur['apporte_materiel']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>
            
                <h5> Faites-vous une visite technique <br> avant le jour J ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="visiteTech" value="oui"
                        <?php if ($Traiteur['visite_technique']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="visiteTech" value="non"
                        <?php if ($Traiteur['visite_technique']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Faites-vous des dégustations ? </h5>

                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="faireDegustation" value="oui"
                        <?php if ($Traiteur['validation_degustation']=='oui') {echo 'checked="checked"';}?>>
                        <label class="form-check-label " for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="faireDegustation" value="non"
                        <?php if ($Traiteur['validation_degustation']=='non') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> La dégustation est : </h5>

                    <div class="mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="degustation" value="locaux"
                            <?php if ($Traiteur['lieu_degustation']=='locaux') {echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Dans les locaux du prestataire </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="degustation" value="emporter"
                            <?php if ($Traiteur['lieu_degustation']=='emporter') {echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> À emporter </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="degustation" value="domicile"
                            <?php if ($Traiteur['lieu_degustation']=='domicile') {echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Le prestataire se déplace à domicile </label>
                        </div>
                    </div>

        </fieldset>
            
    </div>
        
</div>
                        
<div class="container">
            
    <div class="row justify-content-around">
            
        <fieldset class="mt-5 col-lg-4 col-md-6">
            
                <h5> Nombres de personnes max au repas test : </h5>

                    <div class="form-group">
                        <input type="text" name="persMaxTest" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,60}" value="<?= $Traiteur['nombre_personne_max']?>">
                    </div>

                <h5> Prix par personne : </h5>

                    <div class="form-group">
                        <input type="text" name="prixParPers" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,60}" value="<?= $Traiteur['prix_par_personne']?>">
                    </div>

            <h5> Composition du repas test : </h5>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="cocktailTest" value="cocktail"
                        <?php if ($Traiteur['test_cocktail']=='cocktail') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Cocktails </label>
                        <input type="text" name="combienCocktailTest" class="form-control" placeholder="Nombres de cocktails choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}"
                        value="<?= $Traiteur['combien_cocktail_test']?>">
                    </div>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="entreeTest" value="entree"
                        <?php if ($Traiteur['test_entree']=='entree') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Entrées </label>
                        <input type="text" name="combienEntreeTest" class="form-control" placeholder="Nombres d’entrées choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}"
                        value="<?= $Traiteur['combien_entree_test']?>">
                    </div>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="platTest" value="plat"
                        <?php if ($Traiteur['test_plat']=='plat') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Plats </label>
                        <input type="text" name="combienPlatTest" class="form-control" placeholder="Nombres de plats choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}"
                        value="<?= $Traiteur['combien_plat_test']?>">
                    </div>

                    <div class="form-check mb-2">
                        <input class="form-check-input" type="checkbox" name="dessertTest" value="dessert"
                        <?php if ($Traiteur['test_dessert']=='dessert') {echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Desserts </label>
                        <input type="text" name="combienDessertTest" class="form-control" placeholder="Nombres de desserts choisies" pattern="[0-9a-zA-Zà-ÿÀ-Û- ']{1,50}"
                        value="<?= $Traiteur['combien_dessert_test']?>">
                    </div>

                </div>
        </fieldset>
            
    </div>

</div>

    <div class="d-flex justify-content-center">
        <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/ficheOrganisations_post.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche de l'organisation</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Éditer </h1>
    </div>


<form class="d-flex flex-column" action="../../PHP/edit/updateOrganisations.php?numMarie=<?= $_GET['numMarie'];?>" method="POST" enctype="multipart/form-data">

    <h3 class="bg-info text-white text-center m-5"> Informations générales</h3>

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="col-md-3">

            <h5>Informations divers</h5>

                <input type="hidden" name="numMarie" value="<?=$_GET['numMarie'];?>">

                <div class="form-group">
                    <input type="date" name="dateDuMariage" class="form-control"
                    value="<?=$organisations['date_mariage'];?>" required>
                </div>

                <div class="form-group">
                    <input type="text" name="lieuCeremonie" class="form-control" placeholder="Lieu du mariage"
                    value="<?=$organisations['lieu_ceremonie'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>

                <div class="form-group">
                    <input type="text" name="nbrAdulte" class="form-control" placeholder="Nombre d'adulte"
                    value="<?=$organisations['nombre_adulte'];?>" pattern="[0-9]{1,3}">
                </div>

                <div class="form-group">
                    <input type="text" name="nbrEnfant" class="form-control" placeholder="Nombre d'enfant"
                    value="<?=$organisations['nombre_enfant'];?>" pattern="[0-9]{1,3}">
                </div>

        </fieldset>

        <fieldset class="col-md-3">

            <h5>Budget</h5>
    
                <div class="form-group">
                    <input type="text" name="budgetReception" class="form-control" placeholder="Budget du mariage"
                    value="<?=$organisations['budget_reception'];?>" pattern="[0-9 a-zA-Z ]{1,15}">
                </div>

            <h5>Lieu des cérémonies</h5>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="mairie" class="form-control" placeholder="Mairie"
                    value="<?=$organisations['lieu_mairie'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="Ceremoniereligieuse" class="form-control" placeholder="Cérémonie religieuse"
                    value="<?=$organisations['lieu_ceremonie_religieuse'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="Ceremonielaïque" class="form-control" placeholder="Cérémonie laïque"
                    value="<?=$organisations['lieu_ceremonie_laique'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>
            
        </fieldset>
        
        <fieldset class="col-md-3">

                <h6> Timing </h6>

                <h5>La cérémonie civile a lieu :</h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="eventCeremonie" value="Lememejour"
                    <?php if($organisations['event_ceremonie']=='Lememejour') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Le même jour </label>
                </div>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="eventCeremonie" value="Aunedateanterieur"
                    <?php if($organisations['event_ceremonie']=='Aunedateanterieur') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> À une date antérieur </label>
                </div>

                <h6>La réception</h6>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="eventReception" value="leMidi"
                    <?php if($organisations['event_reception']=='leMidi') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Le midi </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="eventReception" value="leSoir"
                    <?php if($organisations['event_reception']=='leSoir') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Le soir </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="eventReception" value="surPlusieursJours"
                    <?php if($organisations['event_reception']=='surPlusieursJours') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Sur plusieurs jours </label>
                </div>

        </fieldset>
        
    </div>
        
</div>

<div class="container">

    <div class="row justify-content-around">
    
        <fieldset class="col-md-3">

            <h5> Types de lieu</h5>

                <div class="d-flex justify-content-around">

                    <div>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Chateau"
                            <?php if($organisations['type_de_lieu']=='Chateau') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Château </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Ferme"
                            <?php if($organisations['type_de_lieu']=='Ferme') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Ferme </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="corpsDeFerme"
                            <?php if($organisations['type_de_lieu']=='corpsDeFerme') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Corps de ferme </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Domaine"
                            <?php if($organisations['type_de_lieu']=='Domaine') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Domaine </label>
                        </div>

                    </div>

                    <div>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Bastide"
                            <?php if($organisations['type_de_lieu']=='Bastide') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Bastide </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Villa"
                            <?php if($organisations['type_de_lieu']=='Villa') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Villa </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="typeDeLieu" value="Orangerie"
                            <?php if($organisations['type_de_lieu']=='Orangerie') { echo 'checked="checked"';}?>>
                            <label class="form-check-label" for=""> Orangerie </label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="autreTypeDeLieu" class="form-control" placeholder="Autre type de lieu"
                            value="<?=$organisations['autre_type_de_lieu'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                        </div>

                    </div>

                </div>

                <div class="form-group">
                    <input type="text" name="localisationGeo" class="form-control" placeholder="localisation géographique"
                    value="<?=$organisations['localisation_geographique'] ?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>

        </fieldset>
            
        <fieldset class="col-md-3">

            <h5> Critéres </h5>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="espaceExt" value="espaceExt"
                    <?php if($organisations['espace_exterieur']=='espaceExt') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Espace extérieur </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="piscine" value="piscine"
                    <?php if($organisations['piscine']=='piscine') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Piscine </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="chambreMaries" value="chambreMaries"
                    <?php if($organisations['chambre_maries']=='chambreMaries') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Chambre des mariés </label>
                </div>

        </fieldset>
        
        <fieldset class="col-md-3">

                <h5> Hébergement sur place </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="hebergementSurPlace" value="oui"
                    <?php if($organisations['hebergement_sur_place']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Oui </label>
                </div>
                <div class="form-group form-check-inline">
                    <input type="text" name="combienHebergement" class="form-control" placeholder="Combien d'hébergement ?"
                    value="<?=$organisations['nombre_hebergement'] ?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,15}">
                </div>

                <div class="form-check">
                    <input type="radio" name="hebergementSurPlace" class="form-check-input" value="non"
                    <?php if($organisations['hebergement_sur_place']=='non') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Non </label>
                </div>

                <div class="form-group">
                    <input type="text" name="autreHebergement" class="form-control" placeholder="Autres hébergements"
                    value="<?=$organisations['autre_hebergement'] ?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

        </fieldset>
        
    </div>
        
</div>

    <h3 class="bg-info text-white text-center m-5"> La restauration </h3>

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="col-md-3">

            <h5>Nombre de convives</h5>

                <div class="form-group">
                    <input type="text" name="convivesVinDhonneur" class="form-control" placeholder="Au vin d'honneur" value="<?=$organisations['vin_dhonneur'];?>" pattern="[0-9]{1,3}">
                    <input type="text" name="convivesAuDiner" class="form-control" placeholder="Au dîner" value="<?=$organisations['diner'];?>" pattern="[0-9]{1,3}">
                    <input type="text" name="convivesAuBrunch" class="form-control" placeholder="Au brunch" value="<?=$organisations['brunch'];?>" pattern="[0-9]{1,3}">
                </div>

        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5>Type de dîner</h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="TypeDeDiner" value="dinerAssis"
                    <?php if($organisations['type_de_diner']=='dinerAssis') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Dîner assis </label>
                </div>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="TypeDeDiner" value="Buffet"
                    <?php if($organisations['type_de_diner']=='Buffet') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Buffet </label>
                </div>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="TypeDeDiner" value="cocktail"
                    <?php if($organisations['type_de_diner']=='cocktail') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Cocktail dinatoire </label>
                </div>

        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5> Type de dessert souhaité : </h5>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="pieceMonteeChoux" value="pieceMonteeChoux"
                    <?php if($organisations['organisation_dessert_piece_montee_choux']=='pieceMonteeChoux') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Pièce montée de choux </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="weddingCake" value="weddingCake"
                    <?php if($organisations['organisation_dessert_wedding_cake']=='weddingCake') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Wedding cake </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="pieceMonteeMacaron" value="pieceMonteeMacaron"
                    <?php if($organisations['organisation_dessert_piece_montee_macarons']=='pieceMonteeMacaron') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Pièce montée de macaron </label>
                </div>
                <div class="form-group">
                    <input type="text" name="autreDessert" class="form-control" placeholder="Autre dessert"
                    value="<?=$organisations['organisation_dessert_autre_dessert'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>
        </fieldset>
        
    </div>
        
</div>

<div class="container">

    <div class="row justify-content-around">
    
        <fieldset class="col-md-3">

            <h5>Animations culinaires : </h5>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="animationCulinaire" value="oui"
                    <?php if($organisations['animation_culinaire']=='oui') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Oui </label>
                </div>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="animationCulinaire" value="non"
                    <?php if($organisations['animation_culinaire']=='non') { echo 'checked="checked"';}?>>
                    <label class="form-check-label" for=""> Non </label>
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="nbrPieceCocktail" class="form-control" placeholder="Nombres de piéces pour le cocktail"
                    value="<?=$organisations['nombre_de_piece'] ?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,25}">
                </div>

        </fieldset>

        <fieldset class="col-md-3">

            <h5> Déroulé du dîner : </h5>

                <div class="d-flex justify-content-md-around">
                    <div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="miseEnBouche" value="miseEnBouche"
                            <?php if($organisations['mise_en_bouche']=='miseEnBouche') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Mise en bouche </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="viande" value="viande"
                            <?php if($organisations['viande']=='viande') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Viande </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="entree" value="entree"
                            <?php if($organisations['entree']=='entree') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Entrée </label>
                        </div>
                    </div>

                    <div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="fromage" value="fromage"
                            <?php if($organisations['fromage']=='fromage') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Fromage </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="poisson" value="poisson"
                            <?php if($organisations['poisson']=='poisson') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Poisson </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="dessert" value="dessert"
                            <?php if($organisations['dessert']=='dessert') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Dessert </label>
                        </div>
                    </div>
                </div>

        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5> Brunch le lendemain </h5>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="brunchLendemain" value="oui"
                    <?php if($organisations['brunch_lendemain']=='oui') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Oui </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="brunchLendemain" value="non"
                    <?php if($organisations['brunch_lendemain']=='non') { echo 'checked="checked"';}?>>
                    <label for="" class="form-check-label"> Non </label>
                </div>

        </fieldset>
        
    </div>
        
</div>

<div class="container">

    <div class="row justify-content-around">
    
        <fieldset>

            <table class="table-responsive-lg tableau_organisation table-bordered">
                <tbody>
                    <tr>
                        <td colspan="2" rowspan="2" class="sans_border"></td>
                        <td colspan="5" class="text-center"> Vin d'honneur </td>
                        <td colspan="4" class="text-center"> Dîner </td>
                        <td colspan="4" class="text-center"> Soirée </td>
                    </tr>

                    <tr>
                        <td class="p-2">Eaux <br> (plate/gazeuse)</td>
                        <td class="p-2">Softs<br>(jus,sodas...)</td>
                        <td class="p-2">Cocktails</td>
                        <td class="p-2">Alcools</td>
                        <td class="p-2">Champagne</td>
                        <td class="p-2">Eaux<br>(plate/gazeuse)</td>
                        <td class="p-2">Softs<br>(jus,sodas...)</td>
                        <td class="p-2">Vins</td>
                        <td class="p-2">Alcools</td>
                        <td class="p-2">Eaux<br>(plate/gazeuse)</td>
                        <td class="p-2">Softs<br>(jus,sodas...)</td>
                        <td class="p-2">Alcools</td>
                        <td class="p-2">Champagne</td>
                    </tr>

                    <tr>
                        <td colspan="2">Les mariés</td>
                        <td><input type="radio" name="vinDhonneurEau" class="form-control" value="vinDhonneurEauMaries"
                        <?php if($organisations['organisation_vin_dhonneur_eau']=='vinDhonneurEauMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurSofts" class="form-control" value="vinDhonneurSoftsMaries"
                        <?php if($organisations['organisation_vin_dhonneur_soft']=='vinDhonneurSoftsMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurCocktail" class="form-control" value="vinDhonneurCocktailMaries"
                        <?php if($organisations['organisation_vin_dhonneur_cocktail']=='vinDhonneurCocktailMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurAlcools" class="form-control" value="vinDhonneurAlcoolsMaries"
                        <?php if($organisations['organisation_vin_dhonneur_alcool']=='vinDhonneurAlcoolsMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurChampagne" class="form-control" value="vinDhonneurChampagneMaries"
                        <?php if($organisations['organisation_vin_dhonneur_champagne']=='vinDhonneurChampagneMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerEau" class="form-control" value="dinerEauMaries"
                        <?php if($organisations['organisation_diner_eau']=='dinerEauMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerSofts" class="form-control" value="dinerSoftsMaries"
                        <?php if($organisations['organisation_diner_softs']=='dinerSoftsMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerVin" class="form-control" value="dinerVinMaries"
                        <?php if($organisations['organisation_diner_vin']=='dinerVinMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerAlcools" class="form-control" value="dinerAlcoolsMaries"
                        <?php if($organisations['organisation_diner_alcools']=='dinerAlcoolsMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeEau" class="form-control" value="soireeEauMaries"
                        <?php if($organisations['organisation_soiree_eau']=='soireeEauMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeSofts" class="form-control" value="soireeSoftsMaries"
                        <?php if($organisations['organisation_soiree_softs']=='soireeSoftsMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeAlcools" class="form-control" value="soireeAlcoolsMaries"
                        <?php if($organisations['organisation_soiree_alcool']=='soireeAlcoolsMaries') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeChampagne" class="form-control" value="soireeChampagneMaries"
                        <?php if($organisations['organisation_soiree_champagne']=='soireeChampagneMaries') { echo 'checked="checked"';}?>></td>

                    </tr>

                    <tr>
                        <td colspan="2">Le traiteur</td>
                        <td><input type="radio" name="vinDhonneurEau" class="form-control" value="vinDhonneurEauTraiteur"
                        <?php if($organisations['organisation_vin_dhonneur_eau']=='vinDhonneurEauTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurSofts" class="form-control" value="vinDhonneurSoftsTraiteur"
                        <?php if($organisations['organisation_vin_dhonneur_soft']=='vinDhonneurSoftsTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurCocktail" class="form-control" value="vinDhonneurCocktailTraiteur"
                        <?php if($organisations['organisation_vin_dhonneur_cocktail']=='vinDhonneurCocktailTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurAlcools" class="form-control" value="vinDhonneurAlcoolsTraiteur"
                        <?php if($organisations['organisation_vin_dhonneur_alcool']=='vinDhonneurAlcoolsTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="vinDhonneurChampagne" class="form-control" value="vinDhonneurChampagneTraiteur"
                        <?php if($organisations['organisation_vin_dhonneur_champagne']=='vinDhonneurChampagneTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerEau" class="form-control" value="dinerEauTraiteur"
                        <?php if($organisations['organisation_diner_eau']=='dinerEauTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerSofts" class="form-control" value="dinerSoftsTraiteur"
                        <?php if($organisations['organisation_diner_softs']=='dinerSoftsTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerVin" class="form-control" value="dinerVinTraiteur"
                        <?php if($organisations['organisation_diner_vin']=='dinerVinTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="dinerAlcools" class="form-control" value="dinerAlcoolsTraiteur"
                        <?php if($organisations['organisation_diner_alcools']=='dinerAlcoolsTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeEau" class="form-control" value="soireeEauTraiteur"
                        <?php if($organisations['organisation_soiree_eau']=='soireeEauTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeSofts" class="form-control" value="soireeSoftsTraiteur"
                        <?php if($organisations['organisation_soiree_softs']=='soireeSoftsTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeAlcools" class="form-control" value="soireeAlcoolsTraiteur"
                        <?php if($organisations['organisation_soiree_alcool']=='soireeAlcoolsTraiteur') { echo 'checked="checked"';}?>></td>

                        <td><input type="radio" name="soireeChampagne" class="form-control" value="soireeChampagneTraiteur"
                        <?php if($organisations['organisation_soiree_champagne']=='soireeChampagneTraiteur') { echo 'checked="checked"';}?>></td>

                    </tr>
                </tbody>
            </table>

        </fieldset>
        
    </div>
        
</div>

            <h3 class="bg-info text-white text-center m-5"> Animations </h3>

<div class="container">

    <div class="row justify-content-around">
                     
        <fieldset class="col-md-3">

            <h5>Photographe</h5>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="formulePhotographe" class="form-control" placeholder="Formule souhaitée :"
                    value="<?=$organisations['organisation_photographe_formule'];?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="stylePhotographe" class="form-control" placeholder="Style de photos :"
                    value="<?=$organisations['organisation_photographe_style'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">

                    <input type="text" name="supportPhotographe" class="form-control" placeholder="Support remis :"
                    value="<?=$organisations['organisation_photographe_support'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

        </fieldset>
                         
        <fieldset class="col-md-3">

            <h5>Vidéo</h5>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="formuleVideo" class="form-control" placeholder="Formule souhaitée :"
                    value="<?=$organisations['organisation_video_formule'];?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="styleVideo" class="form-control" placeholder="Style de vidéo :"
                    value="<?=$organisations['organisation_video_style'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="formatVideo" class="form-control" placeholder="Format vidéo souhaitée :"
                    value="<?=$organisations['organisation_video_format'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="supportVideo" class="form-control" placeholder="Support remis :"
                    value="<?=$organisations['organisation_video_support'];?>" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,30}">
                </div>

        </fieldset>
        
        <fieldset class="col-md-3">


            <h5>Baby sitters</h5>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="nbrEnfantsBS" class="form-control" placeholder="Nombre d'enfants pris en charge"
                    value="<?=$organisations['nombre_d_enfant'];?>" pattern="[0-9]{0,3}">
                </div>

                <div class="d-flex align-items-center form-group">
                    <input type="text" name="horaireBS" class="form-control" placeholder="Plage horaire de prise en charge"
                    value="<?=$organisations['horaire'];?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{0,15}">
                </div>

        </fieldset>
        
    </div>
        
</div>

<div class="container">

    <div class="row justify-content-around">
                         
        <fieldset class="mb-5 col-md-6">

            <h5>Musique</h5>

            <div class="container">

                <div class="row">

                    <div class="col-lg-6">

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="DJ" value="DJ" 
                            <?php if($organisations['dj']=='DJ') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> DJ </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="Gospel" value="Gospel"
                            <?php if($organisations['gospel']=='Gospel') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Gospel </label>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="Orchestre" value="Orchestre"
                            <?php if($organisations['orchestre']=='Orchestre') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Orchestre </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="Chant" value="Chant"
                            <?php if($organisations['chant']=='Chant') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Chant </label>
                        </div>

                    </div>
                    
                    <div>
                        <input type="text" name="styleAnimationMusical" class="form-control" placeholder="Style de musique souhaité"
                        value="<?=$organisations['style'];?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,55}"> 
                    </div>

                </div>
                            
            </div>

        </fieldset>

        <fieldset class="mb-5 col-md-6">

                <h5>Autres Prestataires : </h5>

                    <div class="container">

                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="coiffure" value="Coiffure"
                                    <?php if($organisations['coiffure']=='Coiffure') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Coiffure </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="maquillage" value="Maquillage"
                                    <?php if($organisations['maquillage']=='Maquillage') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Maquillage </label>
                                </div>

                            </div>

                            <div class="col-lg-6">
                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="officiant" value="officiant"
                                    <?php if($organisations['officiant']=='officiant') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Officiant de cérémonie </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="locationVoiture" value="locationVoiture"
                                    <?php if($organisations['location_voiture']=='locationVoiture') { echo 'checked="checked"';}?>>
                                    <label for="" class="form-check-label"> Location de voiture </label>
                                </div>

                            </div>
                            
                        </div>
                            
                    </div>
                            
        </fieldset>

        <fieldset class="mb-5">


                <div class="d-flex align-items-center form-group">
                    <input type="text" name="autre" class="form-control" placeholder="Demandes particulières"
                    value="<?=$organisations['autre'];?>" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{0,120}">
                </div>

        </fieldset>

    </div>
    
</div>

            <div class="d-flex justify-content-center">
                <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>            
            </div>

    </form>

</body>
</html>
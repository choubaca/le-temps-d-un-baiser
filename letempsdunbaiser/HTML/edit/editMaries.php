<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }
    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/edit/editMaries_post.php"); 

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche du premier marié</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Éditer </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/edit/updateMaries.php" method="POST" enctype="multipart/form-data">

    <div class="container">
    
        <div class="row justify-content-around">

            <fieldset>

                <h3 class="text-center"> 1er marié.e </h3>

                <input type="hidden" name="numMarie" value="<?=$_GET['numMarie'];?>">

                <div class="form-group">
                    <label for=""> Etat civil</label>
                    <input type="radio" name="sexe" value="madame" required
                    <?php if ($marie1['marie1_etat_civil']=='madame') { echo 'checked="checked"';}?>> Madame
                    <input type="radio" name="sexe" value="monsieur"
                    <?php if ($marie1['marie1_etat_civil']=='monsieur') { echo 'checked="checked"';}?>> Monsieur
                    <input type="radio" name="sexe" value="indefini"
                    <?php if ($marie1['marie1_etat_civil']=='indefini') { echo 'checked="checked"';}?>> Indéfini
                </div>

                <div class="form-group">
                    <label for="" class="control-label"required> Nom </label>
                    <input type="text" name="nom" class="form-control" value="<?= $marie1['marie1_nom'];?>" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenom" class="form-control" value="<?= $marie1['marie1_prenom'];?>" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Profession </label>
                    <input type="text" name="profession" class="form-control" value="<?= $marie1['marie1_profession'];?>" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Date de naissance </label>
                    <input type="date" name="naissance" class="form-control" value="<?= $marie1['marie1_date_naissance'];?>" required>
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="tel" name="tel" class="form-control" value="<?= $marie1['marie1_telephone'];?>" required  pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="email" name="email" class="form-control" value="<?= $marie1['marie1_email'];?>" required>
                </div>

                <div class="form-group">
                    <label for="" class="control-label"> Adresse </label>
                    <input type="text" name="adresse" class="form-control" value="<?= $marie1['marie1_adresse'];?>" required pattern="[a-zA-Zà-ÿèÀ-Û- ]0-9]{1,120}">
                </div>

            </fieldset>

            <div class="d-flex flex-column justify-content-around">

                <fieldset>

                    <h3 class="text-center"> 1er témoin </h3>

                    <div class="form-group">
                        <label for=""> Etat civil</label>
                        <input type="radio" name="sexeTemoin1" value="madame" required
                        <?php if ($marie1['temoin1_etat_civil']=='madame') { echo 'checked="checked"';}?>> Madame
                        <input type="radio" name="sexeTemoin1" value="monsieur"
                        <?php if ($marie1['temoin1_etat_civil']=='monsieur') { echo 'checked="checked"';}?>> Monsieur
                        <input type="radio" name="sexeTemoin1" value="indefini"
                        <?php if ($marie1['temoin1_etat_civil']=='indefini') { echo 'checked="checked"';}?>> Indéfini
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Nom </label>
                        <input type="text" name="nomTemoin1" class="form-control" value="<?=$marie1['temoin1_nom'];?>" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Prénom </label>
                        <input type="text" name="prenomTemoin1" class="form-control" value="<?=$marie1['temoin1_prenom'];?>" required pattern="[a-zA-Zà-ÿèÀ-Û- ]{1,50}">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Téléphone </label>
                        <input type="text" name="telTemoin1" class="form-control" value="<?=$marie1['temoin1_telephone'];?>" required  pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$">
                    </div>

                </fieldset>

                <input type="button" id='bouton' <?php if (empty($marie1['temoin2_etat_civil'])){ echo 'class="btn btn-info"'; }else {echo 'class="d-none"';}?> value="Ajouter" onclick="afficher();">

                <fieldset <?php if (empty($marie1['temoin2_etat_civil'])){ echo 'class="d-none"'; }else {echo 'class="d-block"';}?> id='temoin2'>

                    <h3 class="text-center"> 2nd temoin </h3>

                    <div class="form-group">
                        <label for=""> État civil</label>
                        <input type="radio" name="sexeTemoin2" value="madame"
                            <?php if ($marie1['temoin2_etat_civil']=='madame') { echo 'checked="checked"';}?>> Madame
                        <input type="radio" name="sexeTemoin2" value="monsieur"
                            <?php if ($marie1['temoin2_etat_civil']=='monsieur') { echo 'checked="checked"';}?>> Monsieur
                        <input type="radio" name="sexeTemoin2" value="indefini"
                            <?php if ($marie1['temoin2_etat_civil']=='indefini') { echo 'checked="checked"';}?>> Indéfini
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Nom </label>
                        <input type="text" name="nomTemoin2" class="form-control requis" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie1['temoin2_nom'];?>">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Prénom </label>
                        <input type="text" name="prenomTemoin2" class="form-control requis" pattern="[a-zA-Zà-ÿÀ-Û- ]{1,50}" value="<?=$marie1['temoin2_prenom'];?>">
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label"> Téléphone </label>
                        <input type="text" name="telTemoin2" class="form-control requis" pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?=$marie1['temoin2_telephone'];?>">
                    </div>

                </fieldset>

                <input type="button" id='boutoncacher' class="d-none" value="Enlever" onclick="cacher();">

                </div>
            
            </div>
        
        </div>            
        
            <div class="d-flex justify-content-center m-5">
                <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
            </div>

</form>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="../../JS/main.js"></script>

</body>
</html>
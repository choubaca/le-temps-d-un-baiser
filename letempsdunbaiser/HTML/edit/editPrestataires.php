<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/sauvegarde/ajoutPrestataires_post.php")

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" href="../../CSS/galerie.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche des prestataires</title>
</head>

<body>

<h1 class="text-center m-5"> Edit des prestataires </h1>

<form class="d-flex justify-content-center" action="../../PHP/edit/updatePrestataires.php" method="POST" enctype="multipart/form-data">

    <input type="hidden" name="numMarie" value="<?=$_GET['numMarie']?>">
    
    <div class="d-flex flex-column taille_prestataires">
    
        <div class="d-flex justify-content-around mb-5">
    
            <div>
            
                <label for="djs">Djs</label>
            
                    <select name="djs" id="djs">
                        <option value="1">defaut</option>
                        <?php foreach ($djs as $dj):?>
                        <option value="<?php echo $dj['id']?>"> <?php echo $dj['dj_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="fleuristes">Fleuristes</label>
            
                    <select name="fleuristes" id="fleuristes">
                    <option value="1">defaut</option>
                        <?php foreach ($fleuristes as $fleuriste):?>
                        <option value="<?php echo $fleuriste['id']?>"> <?php echo $fleuriste['fleuriste_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="lieux">Lieux</label>
            
                    <select name="lieux" id="lieux">
                    <option value="1">defaut</option>
                        <?php foreach ($lieux as $lieu):?>
                        <option value="<?php echo $lieu['id']?>"> <?php echo $lieu['contact_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="nounous">Nounous</label>
            
                    <select name="nounous" id="nounous">
                        <option value="1">defaut</option>
                        <?php foreach ($nounous as $nounou):?>
                        <option value="<?php echo $nounou['id']?>"> <?php echo $nounou['nounou_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
        
        </div>
        
        <div class="d-flex justify-content-center justify-content-around">
        
            <div>
            
                <label for="photographes">Photographes</label>
            
                    <select name="photographes" id="photographes">
                    <option value="1">defaut</option>
                        <?php foreach ($photographes as $photographe):?>
                        <option value="<?php echo $photographe['id']?>"> <?php echo $photographe['photographe_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="traiteurs">Traiteurs</label>
            
                    <select name="traiteurs" id="traiteurs">
                    <option value="1">defaut</option>
                        <?php foreach ($traiteurs as $traiteur):?>
                        <option value="<?php echo $traiteur['id']?>"> <?php echo $traiteur['traiteur_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
            <div>
            
                <label for="videastes">Videastes</label>
            
                    <select name="videastes" id="videastes">
                    <option value="1">defaut</option>
                        <?php foreach ($videastes as $videaste):?>
                        <option value="<?php echo $videaste['id']?>"> <?php echo $videaste['videaste_nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
            
            </div>
            
        </div>
        
        <div class="d-flex justify-content-center m-5">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>
    </div>

</form>

</body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/edit/editGalerie_post.php"); 

    require("../../PHP/edit/updateGalerie.php");
    if ($validation == true) header('refresh: 2 ; url=../../HTML/affichage/galerie.php');

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la galerie</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Editer </h1>
    </div>

<form class="d-flex justify-content-center" enctype="multipart/form-data" action="editGalerie.php" method="post">

        <input type="hidden" name="codeMariage" value="<?=$galeries['id'] ?>">
        <input type="hidden" name="codePhoto" value="<?=$galeries['galerie_photo_de_profil'] ?>">

    <div class="d-flex flex-column w-25">

        <label for="marie"> Mariés </label>
            <select name="marie" id="marie"> 
                    <?php foreach ($maries as $marie):?>
                    <option value="<?php echo $marie['id']?>"> <?php echo $marie['marie1_nom'] ?></option>
                    <?php endforeach; ?>
            </select>

            <label for="">Titre</label>
                <input type="text" name="titre" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$galeries['galerie_titre']?>">
            <label for="">description</label>
                <textarea name="description" cols="30" rows="3" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,255}"><?=$galeries['galerie_paragraphe']?></textarea>
            <label for=""> Photo de profil</label>
                <input type="file" name="photoMariage" required>

        <div class="d-flex justify-content-center m-5">
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
        </div>
        
    </div>

    <div id="erreur" class="text-center">

        <?php
        
            if (!empty($erreurExtention)){
            
                    echo '<p class="text-danger">'.$erreurExtention.'</p>';
                }
            if (!empty($erreurUpload)){
            
                    echo '<p class="text-danger">'.$erreurUpload.'</p>';
                }
            if (!empty($valeurInserer)){
            
                    echo '<p class="text-success">'.$message.'</p>';
                }
        ?>
        
    </div>
    
</form>

</body>
</html>
<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      }

    include("../affichage/menu.php");
    include("../../PHP/connexion/connexion.php");
    include("../../PHP/fiche/fichePhotographes_post.php"); 

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Editer la fiche du photographe</title>
</head>

<body>

    <div class="text-center m-5">
        <h1> Éditer </h1>
    </div>

<form class="d-flex flex-column" action="../../PHP/edit/updatePhotographes.php" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="numPhotographe" value="<?=$photographe['id']?>">

<div class="container">

    <div class="row justify-content-around">

        <fieldset class="mb-5 taille col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Coordonnées </h4>

                <div class="form-group">
                    <label for="" class="control-label"> Nom </label>
                    <input type="text" name="nomPhoto" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$photographe['photographe_nom']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Prénom </label>
                    <input type="text" name="prenomPhoto" class="form-control" required pattern="[a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$photographe['photographe_prenom']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> Téléphone </label>
                    <input type="text" name="telPhoto" class="form-control" required pattern="(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$" value="<?=$photographe['photographe_telephone']?>">
                </div>
                <div class="form-group">
                    <label for="" class="control-label"> E-mail </label>
                    <input type="mail" name="mailPhoto" class="form-control" required value="<?=$photographe['photographe_mail']?>">
                </div>

        </fieldset>

        <fieldset class="mb-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Déplacement </h4>

                <h5> Vous déplacez-vous ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="deplacementPhoto" value="oui" required
                        <?php if ($photographe['deplacement_validation']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="zone" class="form-control ml-3" placeholder="Zone" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$photographe['deplacement_zone']?>">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="deplacementPhoto" value="non"
                        <?php if ($photographe['deplacement_validation']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Y a-t'il un supplement </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="supplementPhoto" value="oui" required
                        <?php if ($photographe['deplacement_supplement']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="prixKMPhoto" class="form-control ml-3" placeholder="Prix aux KM" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}" value="<?=$photographe['prix_au_km']?>">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="supplementPhoto" value="non"
                        <?php if ($photographe['deplacement_supplement']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

        </fieldset>

        <fieldset class="col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Photographies </h4>

                <h5> Style de photos </h5>

                    <div class="mb-3">
                        <div>
                            <input class="form-check-inline" type="checkbox" name="numeriquePhoto" value="numeriquePhoto"
                            <?php if ($photographe['numerique']=='numeriquePhoto') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Photos numeriques </label>
                            <input class="form-check-inline" type="checkbox" name="noirEtBlanc" value="noirEtBlanc"
                            <?php if ($photographe['noir_et_blanc']=='noirEtBlanc') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Noir et blanc </label>
                        </div>
                        <div class="form-check-inline">
                            <input class="form-check-inline" type="checkbox" name="argentPhoto" value="argentPhoto"
                            <?php if ($photographe['argentine']=='argentPhoto') { echo 'checked="checked"';}?>>
                            <label for="" class="form-check-label"> Argentique </label>
                            <input type="text" name="autrePhoto" class="form-control ml-3" placeholder="Autres" value="<?=$photographe['autre_style']?>">
                        </div>
                    </div>

                <h5> Support photos </h5>

                    <div>
                        <input class="form-check-inline" type="checkbox" name="dvd" value="dvd"
                        <?php if ($photographe['photographes_dvd']=='dvd') { echo 'checked="checked"';}?>>
                        <label for="" class="form-check-label"> DVD </label>
                        <input class="form-check-inline" type="checkbox" name="coffret" value="coffret"
                        <?php if ($photographe['photographes_coffret']=='coffret') { echo 'checked="checked"';}?>>
                        <label for="" class="form-check-label"> Coffret </label>
                        <input class="form-check-inline" type="checkbox" name="tirages" value="tirages"
                        <?php if ($photographe['photographes_tirages']=='tirages') { echo 'checked="checked"';}?>>
                        <label for="" class="form-check-label"> Tirage </label>
                        <input class="form-check-inline" type="checkbox" name="usb" value="usb"
                        <?php if ($photographe['photographes_usb']=='usb') { echo 'checked="checked"';}?>>
                        <label for="" class="form-check-label"> USB </label>
                        <input type="text" name="autreSupport" class="form-control mt-3" placeholder="Autres" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$photographe['photographes_Autre']?>">
                    </div>

        </fieldset>   

        <fieldset class="col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Options </h4>

                <h5>Les photos sont-elles toutes retouchées ?</h5>

                    <div class="form-check form-check-inline mb-2">
                        <input class="form-check-input" type="radio" name="retouchePhoto" value="oui" required
                        <?php if ($photographe['retouche_photo']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input class="form-check-input ml-2" type="radio" name="retouchePhoto" value="non"
                        <?php if ($photographe['retouche_photo']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5>Option Photo Booth ? </h5>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="photoBooth" value="oui" required
                        <?php if ($photographe['validation_photobooth']=='oui') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Oui </label>
                        <input type="text" name="optionBooth" class="form-control ml-3" placeholder="Quels options ?" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}"
                        value="<?=$photographe['option_photobooth']?>">
                    </div>
                    <div class="form-check mb-2">
                        <input class="form-check-input" type="radio" name="photoBooth" value="non"
                        <?php if ($photographe['validation_photobooth']=='non') { echo 'checked="checked"';}?>>
                        <label class="form-check-label" for=""> Non </label>
                    </div>

                <h5> Délai moyen de livraison</h5>

                    <div class="form-group">
                        <input type="text" name="delaiLivrai" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$photographe['delai_livraison']?>">
                    </div>

        </fieldset>
            
    </div>
    
</div>

<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Tarifs </h4>

                <div class="d-flex">

                    <div class="d-flex flex-column mr-5">

                        <table>

                            <tr>
                                <td class="text-center"> Formules par moments </td>
                                <td class="text-center"> Prix </td>
                            </tr>
                            <tr>
                                <td> De la mairie au vin d’honneur </td>
                                <td> <input type="text" name="tarif1" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_tarif1']?>"></td>
                            </tr>
                            <tr>
                                <td> De la mairie à la soirée </td>
                                <td> <input type="text" name="tarif2" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_tarif2']?>"></td>
                            </tr>
                            <tr>
                                <td> Des préparatifs au vin d’honneur </td>
                                <td> <input type="text" name="tarif3" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_tarif3']?>"></td>
                            </tr>
                            <tr>
                                <td> Des préparatifs à la soirée </td>
                                <td> <input type="text" name="tarif4" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_tarif4']?>"></td>
                            </tr>
                            <tr>
                                <td> <input type="text" name="formule5" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_formule5']?>"></td>
                                <td> <input type="text" name="tarif5" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_tarif5']?>"></td>
                            </tr>
                            <tr>
                                <td> <input type="text" name="formule6" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_formule6']?>"></td>
                                <td> <input type="text" name="tarif6" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                                value="<?=$photographe['photographe_tarif6']?>"></td>
                            </tr>

                        </table>

                        <div class="form-group">
                            <label for="" class="control-label"> Autres </label>
                            <input type="text" name="autreFormuleMoment" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$photographe['photographe_formule_moment']?>">
                        </div>

                    </div>
        
        </fieldset>
            
    </div>
                    
</div>
                            
<div class="container">
                
    <div class="row justify-content-around">
                
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <div class="d-flex flex-column ml-5">

                <table>

                    <tr>
                        <td class="text-center"> Formules par horaires </td>
                        <td class="text-center"> Prix </td>
                    </tr>
                    <tr>
                        <td> 6h de présence </td>
                        <td> <input type="text" name="tarif7" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                        value="<?=$photographe['photographe_tarif7']?>"></td>
                    </tr>
                    <tr>
                        <td> 8h de présence </td>
                        <td> <input type="text" name="tarif8" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                        value="<?=$photographe['photographe_tarif8']?>"></td>
                    </tr>
                    <tr>
                        <td> 12h de présence </td>
                        <td> <input type="text" name="tarif9" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                        value="<?=$photographe['photographe_tarif9']?>"></td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="formule10" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                        value="<?=$photographe['photographe_formule10']?>"></td>
                        <td> <input type="text" name="tarif10" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                        value="<?=$photographe['photographe_tarif10']?>"></td>
                    </tr>
                    <tr>
                        <td> <input type="text" name="formule11" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                        value="<?=$photographe['photographe_formule11']?>"></td>
                        <td> <input type="text" name="tarif11" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,35}"
                        value="<?=$photographe['photographe_tarif11']?>"></td>
                    </tr>

                </table>

                <div class="form-group">
                    <label for="" class="control-label"> Autres </label>
                    <input type="text" name="autreFormuleHoraire" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$photographe['photographe_formule_horaire']?>">
                </div>

            </div>

        </fieldset>
            
    </div>
        
</div>

<div class="container">
    
    <div class="row justify-content-around">
    
        <fieldset class="mt-5 col-lg-4 col-md-6">

            <h4 class="bg-info text-white text-center"> Remarques </h4>

                <div class="form-group">
                    <input type="text" name="remarquePhoto" class="form-control" pattern="[0-9a-zA-Zà-ÿÀ-Û- ]{1,120}" value="<?=$photographe['photographe_remarque']?>">
                </div>

        </fieldset>

    </div>

</div>

    <div class="d-flex justify-content-center">
        <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer"/>
    </div>

</form>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
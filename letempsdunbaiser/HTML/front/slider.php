<?php

require("../../PHP/connexion/connexion.php");
include("../../PHP/affichage/listePhotos.php");

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../CSS/slider.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <title>Document</title>
</head>
<body>

     <!-- Slideshow container -->
<div class="slideshow-container">

<!-- Full-width images with number and caption text -->

<?php if($tableauPhotos == true){
 
    $photos = substr($tableauPhotos["nom_photo"], 0, -1);
     

    foreach (explode("|", $photos) as $photo):?>
    
    
    
    <div class="m-5 mySlides">
      <img src="../../IMG/<?= $photo ?>" style="width:100%">
    </div>
    
    <?php  endforeach; } ?>

<!-- Next and previous buttons -->
<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<script type="text/javascript" src="../../JS/slider.js"></script>
</body>
</html>
<?php

    include("../../PHP/connexion/connexion.php");
    include("../affichage/menu.php");
    include("../../PHP/affichage/listeGalerie.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <title>Galerie</title>
</head>
<body>


<div id="galerie" class="d-flex flex-column align-items-center">
    <?php foreach ($mariages as $mariage): ?>
        <div class="mariage d-flex mr-3">
            <div class="d-flex flex-column"> 
                <img class="taille_photo" src="../../IMGPROFIL/<?php echo($mariage['galerie_photo_de_profil'])?>" alt="">
            </div>
            <div class="texte d-flex flex-column justify-content-between">
                <div class="ml-5">
                    <h4 class="text-center"><?= $mariage['galerie_titre'] ?></h4>
                    <p ><?= $mariage['galerie_paragraphe'] ?></p>
                </div>
                <div class="d-flex justify-content-center mb-3">
                    <a  href="slider.php?numGalerie=<?=$mariage['id']?>"><input type="button" name="submit" class="btn couleur" value="Galeries de photos"/></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<script src="main.js"></script>

</body>
</html>
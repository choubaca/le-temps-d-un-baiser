<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
      session_start();
    } 
    
    include("../../PHP/connexion/connexion.php");
    include("menu.php");
    include("../../PHP/affichage/listePhotographe.php");

?>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Liste des photographes</title>
</head>

<body>

  <table class="table table-responsive-md table-hover">
    <thead>
      <tr>
        <th scope="col">Nom</th>
          <th scope="col">Prénom</th>
          <th scope="col">Téléphone</th>
          <th scope="col">Email</th>
        <th><button> <a href="../ajout/ajoutPhotographes.php"> Ajouter </a> </button></th>
      </tr>
    </thead>
      <tbody>
      <?php foreach ($Photographes as $Photographe): ?>
              <tr onclick="window.location.href='../fiche/fichePhotographes.php?numPhotographe=<?= $Photographe['id'] ?>'">
                  <td><?= $Photographe['photographe_nom'] ?></td>
                  <td><?= $Photographe['photographe_prenom'] ?></td>
                  <td><?= $Photographe['photographe_telephone'] ?></td>
                  <td><?= $Photographe['photographe_mail'] ?></td>
                  <td> <a href="../edit/editPhotographes.php?numPhotographe=<?=$Photographe['id']?>">éditer</a> </td>
                  <td> <a href="../../PHP/suppression/fichierSupPhotographes.php?numPhotographe=<?=$Photographe['id']?>">suprimer</a> </td>
              </tr>
              <?php endforeach; ?>
      </tbody>
  </table>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
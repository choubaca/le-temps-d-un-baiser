<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      } 

    include("../../PHP/connexion/connexion.php");
    include("menu.php");
    include("../../PHP/affichage/listeGalerie.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Galerie</title>
</head>
<body>


<div id="galerie" class="d-flex flex-column align-items-center">
    <?php foreach ($mariages as $mariage): ?>
        <div class="mariage d-flex mr-3">
            <div class="d-flex flex-column"> 
                <img class="taille_photo" src="../../IMGPROFIL/<?php echo($mariage['galerie_photo_de_profil'])?>" alt="">
                <a class="d-flex justify-content-center" href="galeriesPhotos.php?numGalerie=<?=$mariage['id']?>"><input type="button" name="submit" class="btn couleur" value="Galeries de photos"/></a>
            </div>
            <div class="texte d-flex flex-column ml-5">
            
                <h4 class="text-center mb-3"><?= $mariage['galerie_titre'] ?></h4>
                
                <p class="mt-3"><?= $mariage['galerie_paragraphe'] ?></p>
                
                <a href="../ajout/ajoutPhotos.php?numGalerie=<?=$mariage['id']?>">Ajouter des photos</a>
                
                <a href="../../PHP/suppression/fichierSupPhotos.php?numGalerie=<?=$mariage['id']?>">Suprimer les photos</a>
                
                <a href="../edit/editGalerie.php?numGalerie=<?=$mariage['id']?>">Modifier</a>
                
                <a href="../../PHP/suppression/fichierSupGalerie.php?numGalerie=<?=$mariage['id']?>&numPhoto=<?=$mariage['galerie_photo_de_profil']?>">Suprimer</a>
            </div>
        </div>

    <?php endforeach; ?>

    <div>
    <a href="../ajout/ajoutgalerie.php"><button class="btn btn-primary text-white m-5"> AJOUTER </button> </a>
    </div>

</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
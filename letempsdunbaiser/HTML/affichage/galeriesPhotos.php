<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
        session_start();
      } 
    
  include("../../PHP/connexion/connexion.php");
  include("menu.php");
  include("../../PHP/affichage/listePhotos.php");
  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Galerie de photos</title>
</head>
<body>




<div class="d-flex flex-wrap"> 

<?php
 if($tableauPhotos == false){
 
 echo 'Aucune photos.';
     
 }else{
 
     $photos = substr($tableauPhotos["nom_photo"], 0, -1);

foreach (explode("|", $photos) as $photo) : ?>

<img class="galeriePhoto m-3 " src="../../IMG/<?php echo $photo ?>">

<?php  endforeach; } ?>

 
 
</div>

    <div class="d-flex justify-content-center mt-4">
        <a href="galerie.php"><button class="btn btn-primary"> retour </button></a>
    </div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>
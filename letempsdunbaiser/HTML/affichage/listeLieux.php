<?php

    include("../../PHP/connexion/securite.php");
    if(!isset($_SESSION)){
      session_start();
    } 
    
  include("../../PHP/connexion/connexion.php");
  include("menu.php");
  include("../../PHP/affichage/listeLieux.php");

?>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../CSS/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/myStyle.css">
    <link rel="stylesheet" media="screen" href="../../CSS/screen_back.css" type="text/css"/>
    <title>Liste des Lieux</title>
</head>
<body>

  <table class="table table-responsive-md table-hover">
    <thead>
      <tr>
          <th scope="col">Nom du contact</th>
          <th scope="col">Téléphone du contact</th>
          <th scope="col">Adresse mail du contact</th>
          <th scope="col">Ville</th>
        <th><button> <a href="../ajout/ajoutLieux.php"> Ajouter </a> </button></th>
      </tr>
    </thead>
      <tbody>
      <?php foreach ($Lieux as $Lieu): var_dump($Lieu["id"]);?>
              <tr onclick="window.location.href='../fiche/ficheLieux.php?numLieu=<?=$Lieu['id'] ?>'">
                  <td><?= $Lieu['contact_nom'] ?></td>
                  <td><?= $Lieu['contact_telephone'] ?></td>
                  <td><?= $Lieu['contact_mail'] ?></td>
                  <td><?= $Lieu['lieu_ville'] ?></td>
                  <td> <a href="../edit/editLieux.php?numLieu=<?=$Lieu['id']?>">éditer</a> </td>
                  <td> <a href="../../PHP/suppression/fichierSupLieux.php?numLieu=<?=$Lieu['id']?>">suprimer</a> </td>
              </tr>
        <?php endforeach; ?>
      </tbody>
  </table>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
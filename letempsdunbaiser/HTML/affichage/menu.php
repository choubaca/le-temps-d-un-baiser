<?php

    $page = $_SERVER['REQUEST_URI'];
    
    $page = str_replace("/test/HTML/affichage/", "",$page);
    
?>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="nav-item"> <a class="nav-link couleurMenu <?php if($page == "listeMaries.php"){echo active;} ?>" href="../../HTML/affichage/listeMaries.php"> Liste des mariés </a> </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle couleurMenu <?php if($page == "listeDJ.php" OR $page == "listeFleuristes.php" OR $page == "listeNounous.php" OR $page == "listePhotographes.php" OR $page == "listeTraiteurs.php" OR $page == "listeVideastes.php" OR $page == "listeLieux.php"){echo active;} ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Liste des Prestataires </a>
                    <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item couleurMenu <?php if($page == "listeDJ.php"){echo 'active';} ?>" href="../../HTML/affichage/listeDJ.php"> Liste DJ</a>
                        <a class="dropdown-item couleurMenu <?php if($page == "listeFleuristes.php"){echo 'active';} ?>" href="../../HTML/affichage/listeFleuristes.php"> Liste Fleuristes </a>
                        <a class="dropdown-item couleurMenu <?php if($page == "listeNounous.php"){echo 'active';} ?>" href="../../HTML/affichage/listeNounous.php"> Liste Nounous </a>
                        <a class="dropdown-item couleurMenu <?php if($page == "listePhotographes.php"){echo 'active';} ?>" href="../../HTML/affichage/listePhotographes.php"> Liste Photographes </a>
                        <a class="dropdown-item couleurMenu <?php if($page == "listeTraiteurs.php"){echo 'active';} ?>" href="../../HTML/affichage/listeTraiteurs.php"> Liste Traiteurs </a>
                        <a class="dropdown-item couleurMenu <?php if($page == "listeVideastes.php"){echo 'active';} ?>" href="../../HTML/affichage/listeVideastes.php"> Liste Videastes </a>
                        <a class="dropdown-item couleurMenu <?php if($page == "listeLieux.php"){echo 'active';} ?>" href="../../HTML/affichage/listeLieux.php"> Liste Lieux </a>
                    </div> 
            </li>
            <li class="nav-item"> <a class="nav-link couleurMenu <?php if($page == "galerie.php"){echo active;} ?>" href="../../HTML/affichage/galerie.php"> Gestion des galeries </a> </li>
            <li class="nav-item"> <a class="nav-link couleurMenu" href="../../PHP/connexion/deconnexion.php"> Déconnexion </a> </li>
        </ul>
    </div>
</nav>
